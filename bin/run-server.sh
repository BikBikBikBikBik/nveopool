#!/bin/bash
dir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)

case $1 in
	api)
		project="nVeoPool.ApiServer/nVeoPool.ApiServer.csproj"
		if [ -z "$2" ]
		then
			if [ -z "${NVEOPOOL_API_SETTINGS}" ]
			then
				export NVEOPOOL_API_SETTINGS="$dir/../src/nVeoPool.ApiServer/bin/Release/netcoreapp2.0/api-settings.json"
			fi
		else
			export NVEOPOOL_API_SETTINGS="$2"
		fi
		;;
	pool)
		project="nVeoPool.PoolServer/nVeoPool.PoolServer.csproj"
		if [ -z "$2" ]
		then
			if [ -z "${NVEOPOOL_POOL_SETTINGS}" ]
			then
				export NVEOPOOL_POOL_SETTINGS="$dir/../src/nVeoPool.PoolServer/bin/Release/netcoreapp2.0/pool-settings.json"
			fi
		else
			export NVEOPOOL_POOL_SETTINGS="$2"
		fi
		;;
	*)
		project=""
		;;
esac

if [ -z "$project" ]
then
	echo "USAGE: "
	echo "  $0 api [CONFIG_FILE_PATH]"
	echo "    Start the API server, optionally specifying the path to the API server settings file"
	echo "  $0 pool [CONFIG_FILE_PATH]"
	echo "    Start the pool server, optionally specifying the path to the pool server settings file"
else
	cd -- "$dir/../src"
	dotnet build --configuration Release && dotnet run --project "$project" --configuration Release
fi