----
---- Modify existing tables
----
ALTER TABLE "public"."MinerBalance"
    ADD CONSTRAINT "CHK_MinerBalance_PendingBalance" CHECK ("PendingBalance" >= 0);

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180413_000.sql', EXTRACT(epoch FROM now()));