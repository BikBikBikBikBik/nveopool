----
---- Create tables
----
--
-- UnrecordedBlockScan
--
CREATE TABLE "public"."UnrecordedBlockScan" (
    "LastBlockHeight" integer NOT NULL
);
ALTER TABLE "public"."UnrecordedBlockScan" OWNER TO "postgres";
ALTER TABLE ONLY "public"."UnrecordedBlockScan" ADD CONSTRAINT "PK_UnrecordedBlockScan" PRIMARY KEY ("LastBlockHeight");

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180426_000.sql', EXTRACT(epoch FROM now()));