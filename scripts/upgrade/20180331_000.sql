----
---- Create tables
----
--
-- InvalidShare
--
CREATE TABLE "public"."InvalidShare" (
    "IsDuplicate" boolean NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "SubmittedOn" bigint NOT NULL
);
ALTER TABLE "public"."InvalidShare" OWNER TO "postgres";

----
---- Add indexes
----
CREATE INDEX CONCURRENTLY "IDX_BlockCandidate_SubmittedOn_DESC" ON "public"."BlockCandidate" USING "btree" ("SubmittedOn" DESC NULLS LAST);

CREATE INDEX "IDX_InvalidShare_MinerPublicKey" ON "public"."InvalidShare" USING "btree" ("MinerPublicKey" "text_pattern_ops");

CREATE INDEX "IDX_InvalidShare_SubmittedOn_DESC" ON "public"."InvalidShare" USING "btree" ("SubmittedOn" DESC NULLS LAST);

CREATE INDEX CONCURRENTLY "IDX_PaymentTransaction_MinerPublicKey" ON "public"."PaymentTransaction" USING "btree" ("MinerPublicKey" "text_pattern_ops");

CREATE INDEX CONCURRENTLY "IDX_PaymentTransaction_SubmittedOn_DESC" ON "public"."PaymentTransaction" USING "btree" ("SubmittedOn" DESC NULLS LAST);

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180331_000.sql', EXTRACT(epoch FROM now()));