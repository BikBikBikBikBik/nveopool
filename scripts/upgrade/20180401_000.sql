----
---- Modify existing tables
----
ALTER TABLE "public"."InvalidShare"
    ADD COLUMN "MinerWorkerId" text NOT NULL DEFAULT '';

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180401_000.sql', EXTRACT(epoch FROM now()));