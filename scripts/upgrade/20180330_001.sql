----
---- Create tables
----
--
-- AcceptedShareArchive
--
CREATE TABLE "public"."AcceptedShareArchive" (
    "Id" bigint NOT NULL,
    "BlockDifficulty" integer NOT NULL,
    "BlockHash" "text" NOT NULL,
    "BlockHeight" integer NOT NULL,
    "HashCount" double precision NOT NULL,
    "IsBlockCandidate" boolean NOT NULL,
    "JobDifficulty" integer NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text",
    "Nonce" "text" NOT NULL,
    "SubmittedOn" bigint NOT NULL
);
ALTER TABLE "public"."AcceptedShareArchive" OWNER TO "postgres";

--
-- UpgradeHistory
--
CREATE TABLE "public"."UpgradeHistory" (
    "Id" bigint NOT NULL,
    "ScriptName" "text" NOT NULL,
    "CompletedOn" bigint NOT NULL
);
ALTER TABLE "public"."UpgradeHistory" OWNER TO "postgres";
CREATE SEQUENCE "public"."UpgradeHistory_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "public"."UpgradeHistory_Id_seq" OWNER TO "postgres";
ALTER SEQUENCE "public"."UpgradeHistory_Id_seq" OWNED BY "public"."UpgradeHistory"."Id";
ALTER TABLE ONLY "public"."UpgradeHistory" ALTER COLUMN "Id" SET DEFAULT "nextval"('"public"."UpgradeHistory_Id_seq"'::"regclass");

----
---- Add primary keys
----
ALTER TABLE ONLY "public"."AcceptedShareArchive"
    ADD CONSTRAINT "PK_AcceptedShareArchive" PRIMARY KEY ("Id");

ALTER TABLE ONLY "public"."UpgradeHistory"
    ADD CONSTRAINT "PK_UpgradeHistory" PRIMARY KEY ("Id");

----
---- Add indexes
----
CREATE INDEX CONCURRENTLY "IDX_AcceptedShare_SubmittedOn_DESC" ON "public"."AcceptedShare" USING "btree" ("SubmittedOn" DESC NULLS LAST);

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180330_001.sql', EXTRACT(epoch FROM now()));