----
---- Modify existing tables
----
ALTER TABLE "public"."PaymentTransaction"
    ADD COLUMN "SubmittedOnBlockHeight" integer;

CREATE INDEX "IDX_PaymentTransaction_SubmittedOnBlockHeight_DESC" ON "public"."PaymentTransaction" USING "btree" ("SubmittedOnBlockHeight" DESC NULLS LAST);

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180604_000.sql', EXTRACT(epoch FROM now()));