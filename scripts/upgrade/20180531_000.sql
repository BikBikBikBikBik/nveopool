----
---- Modify existing tables
----
ALTER TABLE "public"."PaymentTransaction"
    ADD COLUMN "TransactionId" "text";

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180531_000.sql', EXTRACT(epoch FROM now()));