----
---- Create tables
----
--
-- MinerWhitelist
--
CREATE TABLE "public"."MinerWhitelist" (
    "MinerPublicKey" text NOT NULL
);
ALTER TABLE "public"."MinerWhitelist" OWNER TO "postgres";
ALTER TABLE ONLY "public"."MinerWhitelist" ADD CONSTRAINT "PK_MinerWhitelist" PRIMARY KEY ("MinerPublicKey");

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180504_000.sql', EXTRACT(epoch FROM now()));