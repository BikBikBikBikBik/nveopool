----
---- Modify existing tables
----
ALTER TABLE "public"."MinerBalance"
    ADD COLUMN "MinimumPayoutThreshold" numeric;

ALTER TABLE "public"."MinerBalance"
    ADD CONSTRAINT "CHK_MinerBalance_MinimumPayoutThreshold" CHECK ("MinimumPayoutThreshold" IS NULL OR "MinimumPayoutThreshold" >= 0);

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180519_000.sql', EXTRACT(epoch FROM now()));