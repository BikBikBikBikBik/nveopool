----
---- Modify existing tables
----
ALTER TABLE "public"."BlockCandidate"
    ADD COLUMN "MinerPublicKey" "text";

UPDATE "public"."BlockCandidate" AS bc SET "MinerPublicKey" = CASE
                                                                WHEN (SELECT EXISTS(SELECT "MinerPublicKey" FROM "AcceptedShare" AS a WHERE bc."Nonce" = a."Nonce")) = true THEN (SELECT "MinerPublicKey" FROM "AcceptedShare" AS a WHERE bc."Nonce" = a."Nonce")
                                                                WHEN (SELECT EXISTS(SELECT "MinerPublicKey" FROM "AcceptedShareArchive" AS a WHERE bc."Nonce" = a."Nonce")) = true THEN (SELECT "MinerPublicKey" FROM "AcceptedShareArchive" AS a WHERE bc."Nonce" = a."Nonce")
	                                                            ELSE '[UNKNOWN]'
                                                            END;

ALTER TABLE "public"."BlockCandidate"
    ALTER COLUMN "MinerPublicKey" SET NOT NULL;

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180508_000.sql', EXTRACT(epoch FROM now()));