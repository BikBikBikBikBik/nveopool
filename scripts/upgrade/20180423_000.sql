----
---- Create tables
----
--
-- BlockCandidateStatus
--
CREATE TABLE "public"."BlockCandidateStatus" (
    "Id" smallint NOT NULL,
    "Name" "text" NOT NULL
);
ALTER TABLE "public"."BlockCandidateStatus" OWNER TO "postgres";
CREATE SEQUENCE "public"."BlockCandidateStatus_Id_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "public"."BlockCandidateStatus_Id_seq" OWNER TO "postgres";
ALTER SEQUENCE "public"."BlockCandidateStatus_Id_seq" OWNED BY "public"."BlockCandidateStatus"."Id";
ALTER TABLE ONLY "public"."BlockCandidateStatus" ALTER COLUMN "Id" SET DEFAULT "nextval"('"public"."BlockCandidateStatus_Id_seq"'::"regclass");
ALTER TABLE ONLY "public"."BlockCandidateStatus" ADD CONSTRAINT "PK_BlockCandidateStatus" PRIMARY KEY ("Id");

----
---- Add default data
----
INSERT INTO "public"."BlockCandidateStatus" ("Id", "Name") VALUES (1, 'Confirmed'), (2, 'Orphaned'), (3, 'Unconfirmed (Accepted)'), (4, 'Unconfirmed (Rejected)');

----
---- Modify existing tables
----
ALTER TABLE "public"."BlockCandidate"
    ADD COLUMN "StatusId" smallint;

UPDATE "public"."BlockCandidate" AS bc SET "StatusId" = CASE
                                                            WHEN bc."IsConfirmed" = false AND bc."IsOrphaned" = false THEN 3
	                                                        WHEN bc."IsConfirmed" = true AND bc."IsOrphaned" = false THEN 1
	                                                        WHEN bc."IsConfirmed" = false AND bc."IsOrphaned" = true THEN 2
	                                                        ELSE 3
                                                        END;

ALTER TABLE "public"."BlockCandidate"
    ALTER COLUMN "StatusId" SET NOT NULL;

ALTER TABLE "public"."BlockCandidate"
    DROP COLUMN "IsConfirmed";

ALTER TABLE "public"."BlockCandidate"
    DROP COLUMN "IsOrphaned";

ALTER TABLE "public"."BlockCandidate"
    RENAME "IsProcessedForPayment" TO "IsPaid";

CREATE INDEX "IDX_FK_BlockCandidate_BlockCandidateStatus" ON "public"."BlockCandidate" USING "btree" ("StatusId");

ALTER TABLE ONLY "public"."BlockCandidate"
    ADD CONSTRAINT "FK_BlockCandidate_BlockCandidateStatus" FOREIGN KEY ("StatusId") REFERENCES "public"."BlockCandidateStatus"("Id");

ALTER TABLE "public"."BlockCandidate"
    ADD CONSTRAINT "CHK_BlockCandidate_IsPaid" CHECK ("IsPaid" = false OR ("IsPaid" = true AND "StatusId" = 1));

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180423_000.sql', EXTRACT(epoch FROM now()));