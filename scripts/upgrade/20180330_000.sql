----
---- Drop existing database
----
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE "nVeoPool";
----
---- Create new database
----
CREATE DATABASE "nVeoPool" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
ALTER DATABASE "nVeoPool" OWNER TO "postgres";

\connect "nVeoPool"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

COMMENT ON SCHEMA "public" IS 'standard public schema';
CREATE EXTENSION IF NOT EXISTS "plpgsql" WITH SCHEMA "pg_catalog";
COMMENT ON EXTENSION "plpgsql" IS 'PL/pgSQL procedural language';
SET default_tablespace = '';
SET default_with_oids = false;

----
---- Create tables
----
--
-- AcceptedShare
--
CREATE TABLE "public"."AcceptedShare" (
    "Id" bigint NOT NULL,
    "BlockDifficulty" integer NOT NULL,
    "BlockHash" "text" NOT NULL,
    "BlockHeight" integer NOT NULL,
    "HashCount" double precision NOT NULL,
    "IsBlockCandidate" boolean NOT NULL,
    "JobDifficulty" integer NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text",
    "Nonce" "text" NOT NULL,
    "SubmittedOn" bigint NOT NULL
);
ALTER TABLE "public"."AcceptedShare" OWNER TO "postgres";
CREATE SEQUENCE "public"."AcceptedShare_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "public"."AcceptedShare_Id_seq" OWNER TO "postgres";
ALTER SEQUENCE "public"."AcceptedShare_Id_seq" OWNED BY "public"."AcceptedShare"."Id";
ALTER TABLE ONLY "public"."AcceptedShare" ALTER COLUMN "Id" SET DEFAULT "nextval"('"public"."AcceptedShare_Id_seq"'::"regclass");

--
-- BlockCandidate
--
CREATE TABLE "public"."BlockCandidate" (
    "BlockHeight" integer NOT NULL,
    "IsConfirmed" boolean NOT NULL,
    "IsOrphaned" boolean NOT NULL,
    "IsProcessedForPayment" boolean NOT NULL,
    "Nonce" "text" NOT NULL,
    "SubmittedOn" bigint NOT NULL
);
ALTER TABLE "public"."BlockCandidate" OWNER TO "postgres";

--
-- BlockReward
--
CREATE TABLE "public"."BlockReward" (
    "BlockHeight" integer NOT NULL,
    "FeePercent" numeric NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "Reward" numeric NOT NULL
);
ALTER TABLE "public"."BlockReward" OWNER TO "postgres";

--
-- MinerBalance
--
CREATE TABLE "public"."MinerBalance" (
    "MinerPublicKey" "text" NOT NULL,
    "PendingBalance" numeric NOT NULL,
    "TotalPaid" numeric NOT NULL
);
ALTER TABLE "public"."MinerBalance" OWNER TO "postgres";

--
-- PaymentTransaction
--
CREATE TABLE "public"."PaymentTransaction" (
    "Amount" numeric NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "SubmittedOn" bigint NOT NULL,
    "TransactionFee" numeric NOT NULL
);
ALTER TABLE "public"."PaymentTransaction" OWNER TO "postgres";

----
---- Add primary keys
----
ALTER TABLE ONLY "public"."AcceptedShare"
    ADD CONSTRAINT "PK_AcceptedShare" PRIMARY KEY ("Id");

ALTER TABLE ONLY "public"."BlockCandidate"
    ADD CONSTRAINT "PK_BlockCandidate" PRIMARY KEY ("BlockHeight", "Nonce");

ALTER TABLE ONLY "public"."BlockReward"
    ADD CONSTRAINT "PK_BlockReward" PRIMARY KEY ("BlockHeight", "MinerPublicKey");

ALTER TABLE ONLY "public"."MinerBalance"
    ADD CONSTRAINT "PK_MinerBalance" PRIMARY KEY ("MinerPublicKey");


----
---- Add indexes
----
CREATE INDEX "IDX_AcceptedShare_BlockHeight" ON "public"."AcceptedShare" USING "btree" ("BlockHeight");

CREATE INDEX "IDX_AcceptedShare_MinerPublicKeyMinerWorkerId" ON "public"."AcceptedShare" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops");

----
---- All done!
----
GRANT ALL ON SCHEMA "public" TO PUBLIC;