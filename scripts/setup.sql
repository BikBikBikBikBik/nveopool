----
---- Drop existing database
----
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE "nVeoPool";
----
---- Create new database
----
CREATE DATABASE "nVeoPool" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';
ALTER DATABASE "nVeoPool" OWNER TO "postgres";

\connect "nVeoPool"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

COMMENT ON SCHEMA "public" IS 'standard public schema';
CREATE EXTENSION IF NOT EXISTS "plpgsql" WITH SCHEMA "pg_catalog";
COMMENT ON EXTENSION "plpgsql" IS 'PL/pgSQL procedural language';
SET default_tablespace = '';
SET default_with_oids = false;

----
---- Create tables
----
--
-- AcceptedShare
--
CREATE TABLE "public"."AcceptedShare" (
    "Id" bigint NOT NULL,
    "BlockDifficulty" integer NOT NULL,
    "BlockHash" "text" NOT NULL,
    "BlockHeight" integer NOT NULL,
    "HashCount" double precision NOT NULL,
    "IsBlockCandidate" boolean NOT NULL,
    "JobDifficulty" integer NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text",
    "Nonce" "text" NOT NULL,
    "SubmittedOn" bigint NOT NULL
);
ALTER TABLE "public"."AcceptedShare" OWNER TO "postgres";
CREATE SEQUENCE "public"."AcceptedShare_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "public"."AcceptedShare_Id_seq" OWNER TO "postgres";
ALTER SEQUENCE "public"."AcceptedShare_Id_seq" OWNED BY "public"."AcceptedShare"."Id";
ALTER TABLE ONLY "public"."AcceptedShare" ALTER COLUMN "Id" SET DEFAULT "nextval"('"public"."AcceptedShare_Id_seq"'::"regclass");

--
-- AcceptedShareArchive
--
CREATE TABLE "public"."AcceptedShareArchive" (
    "Id" bigint NOT NULL,
    "BlockDifficulty" integer NOT NULL,
    "BlockHash" "text" NOT NULL,
    "BlockHeight" integer NOT NULL,
    "HashCount" double precision NOT NULL,
    "IsBlockCandidate" boolean NOT NULL,
    "JobDifficulty" integer NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text",
    "Nonce" "text" NOT NULL,
    "SubmittedOn" bigint NOT NULL
);
ALTER TABLE "public"."AcceptedShareArchive" OWNER TO "postgres";

--
-- BlockCandidate
--
CREATE TABLE "public"."BlockCandidate" (
    "BlockHeight" integer NOT NULL,
    "IsPaid" boolean NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "Nonce" "text" NOT NULL,
    "StatusId" smallint NOT NULL,
    "SubmittedOn" bigint NOT NULL
);
ALTER TABLE "public"."BlockCandidate" OWNER TO "postgres";

--
-- BlockCandidateStatus
--
CREATE TABLE "public"."BlockCandidateStatus" (
    "Id" smallint NOT NULL,
    "Name" "text" NOT NULL
);
ALTER TABLE "public"."BlockCandidateStatus" OWNER TO "postgres";
CREATE SEQUENCE "public"."BlockCandidateStatus_Id_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "public"."BlockCandidateStatus_Id_seq" OWNER TO "postgres";
ALTER SEQUENCE "public"."BlockCandidateStatus_Id_seq" OWNED BY "public"."BlockCandidateStatus"."Id";
ALTER TABLE ONLY "public"."BlockCandidateStatus" ALTER COLUMN "Id" SET DEFAULT "nextval"('"public"."BlockCandidateStatus_Id_seq"'::"regclass");

--
-- BlockReward
--
CREATE TABLE "public"."BlockReward" (
    "BlockHeight" integer NOT NULL,
    "FeePercent" numeric NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "Reward" numeric NOT NULL
);
ALTER TABLE "public"."BlockReward" OWNER TO "postgres";

--
-- InvalidShare
--
CREATE TABLE "public"."InvalidShare" (
    "IsDuplicate" boolean NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "MinerWorkerId" "text" NOT NULL,
    "SubmittedOn" bigint NOT NULL
);
ALTER TABLE "public"."InvalidShare" OWNER TO "postgres";

--
-- MinerBalance
--
CREATE TABLE "public"."MinerBalance" (
    "MinerPublicKey" "text" NOT NULL,
    "MinimumPayoutThreshold" numeric,
    "PendingBalance" numeric NOT NULL,
    "TotalPaid" numeric NOT NULL
);
ALTER TABLE "public"."MinerBalance" OWNER TO "postgres";

--
-- MinerWhitelist
--
CREATE TABLE "public"."MinerWhitelist" (
    "MinerPublicKey" text NOT NULL
);
ALTER TABLE "public"."MinerWhitelist" OWNER TO "postgres";

--
-- PaymentTransaction
--
CREATE TABLE "public"."PaymentTransaction" (
    "Amount" numeric NOT NULL,
    "MinerPublicKey" "text" NOT NULL,
    "SubmittedOn" bigint NOT NULL,
    "SubmittedOnBlockHeight" integer NOT NULL,
    "TransactionFee" numeric NOT NULL,
    "TransactionId" "text" NOT NULL
);
ALTER TABLE "public"."PaymentTransaction" OWNER TO "postgres";

--
-- UnrecordedBlockScan
--
CREATE TABLE "public"."UnrecordedBlockScan" (
    "LastBlockHeight" integer NOT NULL
);
ALTER TABLE "public"."UnrecordedBlockScan" OWNER TO "postgres";

--
-- UpgradeHistory
--
CREATE TABLE "public"."UpgradeHistory" (
    "Id" bigint NOT NULL,
    "ScriptName" "text" NOT NULL,
    "CompletedOn" bigint NOT NULL
);
ALTER TABLE "public"."UpgradeHistory" OWNER TO "postgres";
CREATE SEQUENCE "public"."UpgradeHistory_Id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE "public"."UpgradeHistory_Id_seq" OWNER TO "postgres";
ALTER SEQUENCE "public"."UpgradeHistory_Id_seq" OWNED BY "public"."UpgradeHistory"."Id";
ALTER TABLE ONLY "public"."UpgradeHistory" ALTER COLUMN "Id" SET DEFAULT "nextval"('"public"."UpgradeHistory_Id_seq"'::"regclass");

----
---- Add constraints
----
ALTER TABLE ONLY "public"."AcceptedShare"
    ADD CONSTRAINT "PK_AcceptedShare" PRIMARY KEY ("Id");

ALTER TABLE ONLY "public"."AcceptedShareArchive"
    ADD CONSTRAINT "PK_AcceptedShareArchive" PRIMARY KEY ("Id");

ALTER TABLE ONLY "public"."BlockCandidate"
    ADD CONSTRAINT "PK_BlockCandidate" PRIMARY KEY ("BlockHeight", "Nonce");

ALTER TABLE ONLY "public"."BlockCandidateStatus"
    ADD CONSTRAINT "PK_BlockCandidateStatus" PRIMARY KEY ("Id");

ALTER TABLE ONLY "public"."BlockCandidate"
    ADD CONSTRAINT "FK_BlockCandidate_BlockCandidateStatus" FOREIGN KEY ("StatusId") REFERENCES "public"."BlockCandidateStatus"("Id");

ALTER TABLE "public"."BlockCandidate"
    ADD CONSTRAINT "CHK_BlockCandidate_IsPaid" CHECK ("IsPaid" = false OR ("IsPaid" = true AND "StatusId" = 1));

ALTER TABLE ONLY "public"."BlockReward"
    ADD CONSTRAINT "PK_BlockReward" PRIMARY KEY ("BlockHeight", "MinerPublicKey");

ALTER TABLE ONLY "public"."MinerBalance"
    ADD CONSTRAINT "PK_MinerBalance" PRIMARY KEY ("MinerPublicKey");

ALTER TABLE "public"."MinerBalance"
    ADD CONSTRAINT "CHK_MinerBalance_MinimumPayoutThreshold" CHECK ("MinimumPayoutThreshold" IS NULL OR "MinimumPayoutThreshold" >= 0);

ALTER TABLE "public"."MinerBalance"
    ADD CONSTRAINT "CHK_MinerBalance_PendingBalance" CHECK ("PendingBalance" >= 0);

ALTER TABLE ONLY "public"."MinerWhitelist"
    ADD CONSTRAINT "PK_MinerWhitelist" PRIMARY KEY ("MinerPublicKey");

ALTER TABLE ONLY "public"."PaymentTransaction"
    ADD CONSTRAINT "PK_PaymentTransaction" PRIMARY KEY ("TransactionId");

ALTER TABLE ONLY "public"."UnrecordedBlockScan"
    ADD CONSTRAINT "PK_UnrecordedBlockScan" PRIMARY KEY ("LastBlockHeight");

ALTER TABLE ONLY "public"."UpgradeHistory"
    ADD CONSTRAINT "PK_UpgradeHistory" PRIMARY KEY ("Id");

----
---- Add indexes
----
CREATE INDEX "IDX_AcceptedShare_BlockHeight" ON "public"."AcceptedShare" USING "btree" ("BlockHeight" DESC NULLS LAST);

CREATE INDEX "IDX_AcceptedShare_MinerPublicKeyMinerWorkerId" ON "public"."AcceptedShare" USING "btree" ("MinerPublicKey" "text_pattern_ops", "MinerWorkerId" "text_pattern_ops");

CREATE INDEX "IDX_AcceptedShare_SubmittedOn_DESC" ON "public"."AcceptedShare" USING "btree" ("SubmittedOn" DESC NULLS LAST);

CREATE INDEX "IDX_BlockCandidate_SubmittedOn_DESC" ON "public"."BlockCandidate" USING "btree" ("SubmittedOn" DESC NULLS LAST);

CREATE INDEX "IDX_FK_BlockCandidate_BlockCandidateStatus" ON "public"."BlockCandidate" USING "btree" ("StatusId");

CREATE INDEX "IDX_InvalidShare_MinerPublicKey" ON "public"."InvalidShare" USING "btree" ("MinerPublicKey" "text_pattern_ops");

CREATE INDEX "IDX_InvalidShare_SubmittedOn_DESC" ON "public"."InvalidShare" USING "btree" ("SubmittedOn" DESC NULLS LAST);

CREATE INDEX "IDX_PaymentTransaction_MinerPublicKey" ON "public"."PaymentTransaction" USING "btree" ("MinerPublicKey" "text_pattern_ops");

CREATE INDEX "IDX_PaymentTransaction_SubmittedOn_DESC" ON "public"."PaymentTransaction" USING "btree" ("SubmittedOn" DESC NULLS LAST);

CREATE INDEX "IDX_PaymentTransaction_SubmittedOnBlockHeight_DESC" ON "public"."PaymentTransaction" USING "btree" ("SubmittedOnBlockHeight" DESC NULLS LAST);

----
---- Add default data
----
INSERT INTO "public"."BlockCandidateStatus" ("Id", "Name") VALUES (1, 'Confirmed'), (2, 'Orphaned'), (3, 'Unconfirmed (Accepted)'), (4, 'Unconfirmed (Rejected)');

----
---- All done!
----
GRANT ALL ON SCHEMA "public" TO PUBLIC;

----
---- Update upgrade history
----
INSERT INTO "public"."UpgradeHistory" ("ScriptName", "CompletedOn") VALUES ('20180604_000.sql', EXTRACT(epoch FROM now()));