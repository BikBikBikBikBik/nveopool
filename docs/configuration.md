# Database Configuration
The included setup.sql script (in the "scripts" directory) is meant to be run from the [psql console application](https://www.postgresql.org/docs/current/static/app-psql.html) included with PostgreSQL. This script will create a database called "nVeoPool" for the "postgres" user. For now the only way to modify these values is to edit the script before you run it. Make note of the database and user you use as these are required for the database connection string.

# Pool Server Configuration
Here is a full sample configuration file for the pool server. Each section will be explained below.
```
{
	"banning": {
		"$type":"nVeoPool.PoolServer.Configuration.Banning.BanningConfiguration, nVeoPool.PoolServer",
		"whitelistEnabled": false
	},
	"cache": {
		"$type":"nVeoPool.Common.Configuration.Caching.RedisCacheConfiguration, nVeoPool.Common",
		"connectionString": "localhost"
	},
	"clustering": {
		"$type":"nVeoPool.Common.Configuration.Clustering.ClusteringConfiguration, nVeoPool.Common",
		"id": "pool-master-00",
		"type": "Master"
	},
	"database": {
		"$type":"nVeoPool.Common.Configuration.Database.DatabaseConfiguration, nVeoPool.Common",
		"connectionString": "host=localhost;port=5432;database=nVeoPool;username=postgres;password=[PASSWORD]"
	},
	"logging": {
		"$type":"nVeoPool.Common.Configuration.Logging.LoggingConfiguration, nVeoPool.Common",
		"fileName": "log-poolServer.txt",
		"level": "Warning"
	},
	"nodeRpc": {
		"$type":"nVeoPool.Common.Configuration.Daemon.DaemonClientConfiguration, nVeoPool.Common",
		"backup": {
			"nodes": [
				{
					"ip": "192.168.1.1",
					"externalApiPort": 8080,
					"internalApiPort": 8081
				},
				{
					"ip": "192.168.1.2",
					"externalApiPort": 8080,
					"internalApiPort": 8081
				}
			],
			"strategy": {
				"breakDuration": 180,
				"failureThreshold": 70,
				"minimumRequests": 20,
				"sampleDuration": 90
			}
		},
		"local": {
			"ip": "127.0.0.1",
			"externalApiPort": 8080,
			"internalApiPort": 8081
		}
	},
	"notifications": {
		"$type":"nVeoPool.PoolServer.Configuration.Notifications.NotificationsConfiguration, nVeoPool.PoolServer",
		"discord": [
			{
				"eventIds": [ 8000, 9000, 10000, 110000 ],
				"webHookId": "WEBHOOK_ID",
				"webHookToken": "WEBHOOK_TOKEN"
			}
		]
	},
	"payment": {
		"$type":"nVeoPool.PoolServer.Configuration.Payment.PaymentConfiguration, nVeoPool.PoolServer",
		"batchSendDelay": 0,
		"batchSendSize": 0,
		"blockMaturityDepth": 10,
		"fees": {
			"BOatPo5I2lCvg8n0Jtr0yueONu+PWCj16ZKp4+2JfGhkAwCDhFKwk36kjCv348LFlQV548z5nA43fS2jn1LjeZ0=": 0.05
		},
		"individualSendDelay": 1000,
		"minimumPayoutThreshold": 0.5,
		"poolFee": 1.95,
		"poolPaysTransactionFee": true,
		"roundLength": 10,
		"transactionFee": 0.0015205
	},
	"serviceTimers": {
		"$type":"nVeoPool.PoolServer.Configuration.Services.ServicesConfiguration, nVeoPool.PoolServer",
		"archivedShareLifetime": 7,
		"banManagement": 60,
		"blockCandidateProcessor": 300,
		"cacheCleanup": 900,
		"difficultyCalculator": 30,
		"heartbeat": 15,
		"invalidShareCleanup": 604800,
		"invalidShareStorage": 30,
		"nodeFailover": 60,
		"nodeSync": 2,
		"paymentSender": 1800,
		"shareArchiver": 604800,
		"shareStorage": 3,
		"workRetriever": 150,
	},
	"varDiff": {
		"$type":"nVeoPool.PoolServer.Configuration.VarDiff.VarDiffConfiguration, nVeoPool.PoolServer",
		"initialDifficulty": 9500,
		"intervalSamples": 5,
		"intervalVariance": 25,
		"maximumDifficulty": 11000,
		"maximumDifficultyJump": 3,
		"minimumDifficulty": 9000,
		"targetShareInterval": 30
	},
	"workProtocol": {
		"$type":"nVeoPool.PoolServer.Configuration.WorkProtocol.WorkProtocolConfiguration, nVeoPool.PoolServer",
		"getWork": {
			"enableRequestLogging": false,
			"keepAliveTimeout": 120,
			"listenIpAddress": "0.0.0.0",
			"listenPort": 8085,
			"maximumConcurrentConnections": 15000,
			"maximumRequestBodySize": 384
			"maximumRequestBufferSize": 1048576,
			"maximumRequestHeaders": 10,
			"maximumRequestHeadersSize": 256,
			"maximumRequestLineSize": 128,
			"maximumResponseBufferSize": 65536,
			"maximumWaitOnClose": 30,
			"requestHeadersTimeout": 5
		},
		"maximumStaleWorkDuration": 60,
		"maximumTrailingBlocks": 2,
		"requireSubscribe": false,
		"stratum": {
			"idleTimeout": 120,
			"listenIpAddress": "0.0.0.0",
			"listenPort": 8086,
			"maximumWaitOnIdleDisconnect": 5
		}
	}
}
```

## **banning**
* ### $type (String, Required)
> Leave the default/example value for now.
* ### whitelistEnabled (Boolean, Optional)
> Whether or not to enable checking miner public keys against the whitelist before allowing mining.

## **cache**
* ### $type (String, Required)
> Leave the default/example value for now.
* ### connectionString (String, Required)
> The Redis connection string. Full configuration options can be found [here](https://stackexchange.github.io/StackExchange.Redis/Configuration#configuration-options).

## **clustering**
* ### $type (String, Required)
> Leave the default/example value for now.
* ### id (String, Optional)
> The id of the server.
* ### type (String, Optional)
> The type of the server, eithe Master or Slave.

## **database**
* ### $type (String, Required)
> Leave the default/example value for now.
* ### connectionString (String, Required)
> The PostgreSQL connection string. Full configuration options can be found [here](http://www.npgsql.org/doc/connection-string-parameters.html).

## **logging**
* ### $type (String, Required)
> Leave the default/example value for now.
* ### fileName (String, Required)
> The name of the file to log to.
* ### level (String, Required)
> The level to log at. Possible values: Verbose, Debug, Information, Warning, Error, Fatal.

## **nodeRpc**
* ### $type (String, Required)
> Leave the default/example value for now.
* ### backup (Object, Optional)
> Define this object to enable backup nodes.
```
{
  *nodes (Array<Object>, Required)
    -The collection of backup nodes each defined with the same format as the "local" key under the "nodeRpc" key. Each backup node can also contain a "strategy" key that overrides the failover strategy defined in the object below.

  *strategy (Object, Optional)
    -Define this object to override the default failover strategy.
      {
        *breakDuration (Number, Optional)
          -The number of seconds for the node to remain unreachable to allow it to recover.

        *failureThreshold (Number, Optional)
          -The minimum number of requests that must fail, expressed as percentage points.

        *minimumRequests (Number, Optional)
          -The minimum numer of requests that must be attempted during the sample duration before any potential failure.

        *sampleDuration (Number, Optional)
          -The number of seconds to watch for failing requests, operated as a moving window.
      }
}
```
* ### local (Object, Optional)
> Define this object to override the default local node configuration.
```
{
  *ip (String, Optional)
    -The IP address of the local Amoveo node used for mining.

  *externalApiPort (Number, Optional)
    -The port number for accessing the external API of the local node.

  *internalApiPort (Number, Optional)
    -The port number for accessing the internal API of the local node.
}
```

## **notifications**
* ### $type (String, Required)
> Leave the default/example value for now.
* ### discord (Array<Object>, Optional)
> Define this array to receive webhook notifications for desired events. Multiple WebHooks can be configured to received different events.
```
[
  {
    *eventIds (Array<Number>, Required)
	  -The events to forward to this Discord WebHook.

    *webHookId (String, Required)
	  -The Discord WebHook Id.

    *webHookToken (String, Required)
	  -The Discord WebHook token.
  }
]
```
> The following is a list of available event ids:
```
1000   - Block candidate accepted by node
2000   - Block candidate rejected by node
3000   - Block candidates successfully processed
4000   - Unrecorded block candidate successfully found & saved
5000   - Error retrieving height from node for work
6000   - Error retrieving mining data from node for work
7000   - Node height is behind peers
8000   - No work available
8250   - Possible backup node(s) sync issue
8500   - Possible primary node sync issue
8750   - Primary node recovered
9000   - Error saving successfully sent miner payments
10000  - Error sending individual payment
11000  - Payment service is starting payment process
12000  - Block rewards successfully saved for individual block
13000  - Payment service halted to avoid possible double-spend
```

## **payment**
* ### $type (String, Required)
> Leave the default/example value for now.
* ### batchSendDelay (Number, Optional)
> The number of seconds between sending each batch of payments.
* ### batchSendSize (Number, Optional)
> The number of individual payments that make up a single batch of payments.
* ### blockMaturityDepth (Number, Required)
> The number of blocks that are mined before a block is considered confirmed and processed for payment.
* ### fees (Dictionary<String, Number>, Optional)
> Additional fees (beyond the base pool fee) specified as public key/percent pairs. The example fee value is a developer donation of 0.05%. Removing/changing the developer donation will not negatively impact or throttle the software in any way.
* ### individualSendDelay (Number, Optional)
> The number of milliseconds between submitting each individual payment to the node.
* ### minimumPayoutThreshold (Number, Required)
> Minimum number of VEO required before a miner will be paid their pending balance.
* ### poolFee (Number, Required)
> The percent of each block reward kept for the pool, expressed as percentage points. The example value of 1.95 is a 1.95% fee.
* ### poolPaysTransactionFee (Boolean, Required)
> Whether or not the pool pays the transaction fees for miner payments. If true, make sure your coinbaseAddress has enough VEO to cover payments. If false the transaction fee will be subtracted from a miner's balance when sending the payment.
* ### roundLength (Number, Required)
> The number of blocks considered when calculating payments. This does not only count blocks mined by the pool but instead looks at the overall network.
* ### transactionFee (Number, Optional)
> The fee to pay the node for a single payment transaction.

## **serviceTimers** (Don't touch these unless you know what you're doing!)
* ### $type (String, Required)
> Leave the default/example value for now.
* ### archivedShareLifetime (Number, Optional)
> The number of days to keep archived shares before deleting them completely.
* ### blockCandidateProcessor (Number, Optional)
> The number of seconds to wait between checks for mature/orphan blocks.
* ### cacheCleanup (Number, Optional)
> The number of seconds to wait between freeing up some cache space.
* ### difficultyCalculator (Number, Optional)
> The number of seconds to wait between recalculating difficulties for active workers.
* ### heartbeat (Number, Optional)
> The number of seconds between each pool heartbeat (available via the API).
* ### invalidShareCleanup (Number, Optional)
> The number of seconds to wait between permanently deleting all invalid shares since the last run.
* ### invalidShareStorage (Number, Optional)
> The number of seconds to wait between checks for more new invalid shares to store in the database.
* ### nodeFailover (Number, Optional)
> The number of seconds to wait between checks for potentially out of sync primary/backup nodes.
* ### nodeSync (Number, Optional)
> The number of seconds to wait between checks for whether a sync should be forced.
* ### paymentSender (Number, Optional)
> The number of seconds to wait between sending payments for miners over the minimum payout threshold.
* ### shareArchiver (Number, Optional)
> The number of seconds to wait between pruning old shares.
* ### shareStorage (Number, Optional)
> The number of seconds to wait between checks for more new shares to store in the database.
* ### workRetriever (Number, Optional)
> The number of milliseconds to wait between checks for new work from the Amoveo node.

## **varDiff**
* ### $type (String, Required)
> Leave the default/example value for now.
* ### initialDifficulty (Number, Required)
> The initial job difficulty for a new miner.
* ### intervalSamples (Number, Optional)
> The number of share interval samples used to calculate difficulty.
* ### intervalVariance (Number, Optional)
> The percent by which the target share interval can vary for a miner, expressed as percentage points. The example value of 25 indicates an allowed variance of 25% up or down from the target share interval.
* ### maximumDifficulty (Number, Optional)
> The maximum job difficulty for a miner. Leave this (along with minimumDifficulty) empty to use initialDifficulty as the fixed difficulty for all jobs.
* ### maximumDifficultyJump (Number, Optional)
> The maximum percent by which the current job difficulty can be adjusted in one difficulty adjustment period, expressed as percentage points. The example value of 3 indicates 3%.
* ### minimumDifficulty (Number, Optional)
> The minimum job difficulty for a miner. Leave this (along with maximumDifficulty) empty to use initialDifficulty as the fixed difficulty for all jobs.
* ### targetShareInterval (Number, Optional)
> The ideal number of seconds between a miner's share submissions. Their difficulty will be adjusted periodically to try to get close to this value.

## **workProtocol**
* ### $type (String, Required)
> Leave the default/example value for now.
* ### getWork (Object, Optional)
> Define this object to enable the HTTP-based GetWork protocol.
```
{
  *enableRequestLogging (Boolean, Optional)
    -Whether or not to enable HTTP request logging. Requests are logged at Information level so ensure that "level" in the "logging" configuration section is set to "Information" (or lower) to fully enable request logging.

  *keepAliveTimeout (Number, Optional)
    -The number of seconds to keep-alive connections.

  *listenIpAddress (String, Required)
    -The IP address to listen on.

  *listenPort (Number, Required)
    -The port to listen on. Make sure to use a different port than Stratum and the API server.

  *maximumConcurrentConnections (Number, Optional)
    -The maximum number of open HTTP(S) connections.

  *maximumRequestBodySize (Number, Optional)
    -The maximum size of a request body in bytes.

  *maximumRequestBufferSize (Number, Optional)
    -The maximum size of the request buffer in bytes.

  *maximumRequestHeaders (Number, Optional)
    -The maximum number of allowed HTTP headers in a request.

  *maximumRequestHeadersSize (Number, Optional)
    -The maximum size of the request headers in bytes.

  *maximumRequestLineSize (Number, Optional)
    -The maximum size of the HTTP request line in bytes.

  *maximumResponseBufferSize (Number, Optional)
    -The maximum size of the response buffer in bytes.

  *maximumWaitOnClose (Number, Optional)
    -The maximum number of seconds to wait to gracefully close existing connections before forcefully closing them on server shutdown.

  *requestHeadersTimeout (Number, Optional)
    -The maximum number of seconds to spend receiving headers for a request.
}
```
* ### maximumStaleWorkDuration (Number, Optional)
> The maximum number of seconds the current mining data can remain unchanged before signalling "No Work Available".
* ### maximumTrailingBlocks (Number, Optional)
> The maximum number of blocks the current node can be behind other configured nodes before switching.
* ### requireSubscribe (Boolean, Optional)
> Whether or not a miner must call GetWork/subscribe before submitting a share.
* ### stratum (Object, Optional)
> Define this object to enable the TCP-based Stratum protocol.
```
{
  *idleTimeout (Number, Optional)
    -The maximum number of seconds that can pass between submits before a client will be automatically disconnected. Set to 0 to disable automatically disconnecting idle clients.

  *listenIpAddress (String, Required)
    -The IP address to listen on.

  *listenPort (Number, Required)
    -The port to listen on. Make sure to use a different port than GetWork and the API server.

  *maximumWaitOnIdleDisconnect (Number, Optional)
    -The maximum number of seconds to wait to gracefully disconnect an idle client before forcefully disconnecting them.
}
```

# API Server Configuration
Here is a full sample configuration file for the API server. Each section will be explained below.
```
{
	"cache": {
		"$type": "nVeoPool.Common.Configuration.Caching.RedisCacheConfiguration, nVeoPool.Common",
		"connectionString": "localhost"
	},
	"database": {
		"$type": "nVeoPool.Common.Configuration.Database.DatabaseConfiguration, nVeoPool.Common",
		"connectionString": "host=localhost;port=5432;database=nVeoPool;username=postgres;password=[PASSWORD]"
	},
	"http": {
		"$type": "nVeoPool.Common.Configuration.Http.HttpConfiguration, nVeoPool.Common",
		"enableRequestLogging": false,
		"listenIpAddress": "0.0.0.0",
		"listenPort": 8087,
		"maximumWaitOnClose": 30
	},
	"logging": {
		"$type":"nVeoPool.Common.Configuration.Logging.LoggingConfiguration, nVeoPool.Common",
		"fileName": "log-apiServer.txt",
		"level": "Warning"
	},
	"serviceTimers": {
		"$type":"nVeoPool.ApiServer.Configuration.Services.ServicesConfiguration, nVeoPool.ApiServer",
		"dataCacher": 60
	}
}
```

## **cache**
* ### $type (String, Required)
> Leave the default/example value for now.
* ### connectionString (String, Required)
> The Redis connection string. Full configuration options can be found [here](https://stackexchange.github.io/StackExchange.Redis/Configuration#configuration-options).

## **database**
* ### $type (String, Required)
> Leave the default/example value for now.
* ### connectionString (String, Required)
> The PostgreSQL connection string. Full configuration options can be found [here](http://www.npgsql.org/doc/connection-string-parameters.html).

## **http**
* ### $type (String, Required)
> Leave the default/example value for now.
* ### enableRequestLogging (Boolean, Required)
> Whether or not to enable HTTP request logging. Requests are logged at Information level so ensure that "level" in the "logging" configuration section is set to "Information" (or lower) to fully enable request logging.
* ### listenIpAddress (String, Required)
> The IP address to listen on.
* ### listenPort (Number, Required)
> The port to listen on. If the API server is run on the same server as the pool make sure to use different port numbers.
* ### maximumWaitOnClose (Number, Optional)
> The maximum number of seconds to wait to gracefully close existing connections before forcefully closing them on server shutdown.

## **logging**
* ### $type (String, Required)
> Leave the default/example value for now.
* ### fileName (String, Required)
> The name of the file to log to.
* ### level (String, Required)
> The level to log at. Possible values: Verbose, Debug, Information, Warning, Error, Fatal.

## **serviceTimers** (Don't touch these unless you know what you're doing!)
* ### $type (String, Required)
> Leave the default/example value for now.
* ### dataCacher (Number, Optional)
> The number of seconds to wait between caching data used for various API responses.