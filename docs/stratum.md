# Stratum Client-Server Flow
* Use the "subscribe" method to receive new job notifications on that TCP connection.
* Calling "subscribe" on a given TCP connection is not required before calling "submit" on that connection.
* There is no limit to the number of TCP connections that can "subscribe" or "submit" for a given PubKey.workerId combination.
* JSON expanded only for clarity. Actual communication is newline-delimited ("\n") with one message per line.

## Client fields
* ### id (Number, Optional)
> Useful for message correlation. Server will send back matching id only if the client sets this field.
* ### method (Number, Required)
> See end of document for list of method numbers.
* ### params (Object, Required)
> The parameters required for the method. See the examples below for the method-specific parameters.

## Subscribe
* Client sends:
```
{
	"id": 1,
	"method": 0,
	"params": {
		"id": "PubKey.worker00"
	}
}
```
* Server responds:
```
{
	"id": 1,
	"error": error ? {code: ErrorCodeNumber, message: "Error message"} : null,
	"result": {
		"bHash": "NewBlockHash00000000=",
		"jDiff": JobDifficultyNumber
	}
}
```

## Submit share
* Client sends:
```
{
	"id": 2,
	"method": 1,
	"params": {
		"id": "PubKey.worker00",
		"nonce": "Nonce00000000="
	}
}
```
* Server responds:
```
{
	"id": 2,
	"error": error ? {code: ErrorCodeNumber, message: "Error message"} : null,
	"result": !error ? {"acc": 1} : null
}
```

## Server "New Job" notification
* Either new difficulty or new block hash, server sends:
```
{
	"method": 2,
	"params": {
		"bHash": "NewBlockHash00000000=",
		"jDiff": NewJobDifficultyNumber
	}
}
```

## Server "No Work Available" notification
* When the pool is unable to retrieve new work for mining, server sends:
```
{
	"method": 2,
	"error": {code: 12}
}
```

## Methods
```
#  | Name
--------------
0  | Subscribe
1  | Submit
2  | Job
```

## Error codes/messages
* No "message" field will be present in the error object except for the case of error code = -1 ("generic" error). Otherwise only the "code" field will be present in the error object.
```
Code | Message                       | More Info
-------------------------------------------------
 10  | Banned                        |
 11  | Invalid address               |
 12  | No work available             |
 20  | Block/nonce expired           |
 21  | Invalid job id                |
 22  | Duplicate share               |
 23  | Low difficulty share          |
 24  | Unauthenticated               |
 -1  | Other/unknown error           |
```