# API Endpoints
* ## /api/miners?address={PublicKey}&includeWorkers={true|false}
> Returns aggregate stats for a miner. Hashrate measurements are in GH/s.

> String {PublicKey} - Required; The Base64-encoded public key string. Make sure this is URL-encoded.

> Boolean {includeWorkers} - Optional, default false; Whether or not to include all individual worker stats in the response
```
Request: http://localhost:8087/api/miners?address=BOatPo5I2lCvg8n0Jtr0yueONu%2BPWCj16ZKp4%2B2JfGhkAwCDhFKwk36kjCv348LFlQV548z5nA43fS2jn1LjeZ0%3D&includeWorkers=true

Reply:
{
	"individualWorkerStats": [
		{
			"workerId": "rig00",
			"acceptedShareCount1Hour": 1204,
			"acceptedShareCount6Hours": 1417,
			"acceptedShareCount24Hours": 1517,
			"averageJobDifficulty1Hour": 9623,
			"averageJobDifficulty6Hours": 9721,
			"averageJobDifficulty24Hours": 9685,
			"hashrate1Hour": 13.286958548764444,
			"hashrate6Hours": 2.6062597264118517,
			"hashrate24Hours": 0.6515649316029629,
			"invalidShareCount1Hour": 105,
			"invalidShareCount6Hours": 155,
			"invalidShareCount24Hours": 185,
			"lastShareAcceptedOn": 1525768785
		},
		{
			"workerId": "rig01",
			"acceptedShareCount1Hour": 1124,
			"acceptedShareCount6Hours": 1393,
			"acceptedShareCount24Hours": 1617,
			"averageJobDifficulty1Hour": 9523,
			"averageJobDifficulty6Hours": 9621,
			"averageJobDifficulty24Hours": 9585,
			"hashrate1Hour": 11.7854568548764444,
			"hashrate6Hours": 5.7894564118517,
			"hashrate24Hours": 3.123649316029629,
			"invalidShareCount1Hour": 125,
			"invalidShareCount6Hours": 185,
			"invalidShareCount24Hours": 207,
			"lastShareAcceptedOn": 1525768783
		}
	],
	"publicKey": "BOatPo5I2lCvg8n0Jtr0yueONu+PWCj16ZKp4+2JfGhkAwCDhFKwk36kjCv348LFlQV548z5nA43fS2jn1LjeZ0=",
	"acceptedShareCount1Hour": 2328,
	"acceptedShareCount6Hours": 2810,
	"acceptedShareCount24Hours": 3134,
	"averageJobDifficulty1Hour": 9573,
	"averageJobDifficulty6Hours": 9671,
	"averageJobDifficulty24Hours": 9635,
	"hashrate1Hour": 25.0724154036408884,
	"hashrate6Hours": 8.3957161382635517,
	"hashrate24Hours": 3.7752142476325919,
	"invalidShareCount1Hour": 230,
	"invalidShareCount6Hours": 340,
	"invalidShareCount24Hours": 392,
	"lastShareAcceptedOn": 1525768785
}
```

* ## /api/miners/balance?address={PublicKey}
> Returns pending and total paid balance for a miner

> String {PublicKey} - Required; The Base64-encoded public key string. Make sure this is URL-encoded.
```
Request: http://localhost:8087/api/miners/balance?address=BOatPo5I2lCvg8n0Jtr0yueONu%2BPWCj16ZKp4%2B2JfGhkAwCDhFKwk36kjCv348LFlQV548z5nA43fS2jn1LjeZ0%3D

Reply:
{
	"pendingBalance": 0.23476,
	"totalPaid": 1.2398472
}
```

* ## /api/miners/payment?address={PublicKey}
> Returns an array of recent payment history for a miner

> String {PublicKey} - Required; The Base64-encoded public key string. Make sure this is URL-encoded.
```
Request: http://localhost:8087/api/miners/payment?address=BOatPo5I2lCvg8n0Jtr0yueONu%2BPWCj16ZKp4%2B2JfGhkAwCDhFKwk36kjCv348LFlQV548z5nA43fS2jn1LjeZ0%3D

Reply:
[
	{
		"amount": 1.283746,
		"submittedOn": 152342342,
		"transactionFee": 0,
		"transactionId": "VEO_TX_ID"
	}
]
```

* ## /api/miners/workers?address={PublicKey}
> Returns an array of all worker ids for a miner

> String {PublicKey} - Required; The Base64-encoded public key string. Make sure this is URL-encoded.
```
Request: http://localhost:8087/api/miners/workers?address=BOatPo5I2lCvg8n0Jtr0yueONu%2BPWCj16ZKp4%2B2JfGhkAwCDhFKwk36kjCv348LFlQV548z5nA43fS2jn1LjeZ0%3D

Reply:
[
	"rig00",
	"rig01"
]
```

* ## /api/miners/workers?address={PublicKey}&workerId={WorkerId}
> Returns individual worker stats for a miner. Hashrate measurements are in GH/s.

> String {PublicKey} - Required; The Base64-encoded public key string. Make sure this is URL-encoded.

> String {WorkerId} - Required; The worker id.
```
Request: http://localhost:8087/api/miners/workers?address=BOatPo5I2lCvg8n0Jtr0yueONu%2BPWCj16ZKp4%2B2JfGhkAwCDhFKwk36kjCv348LFlQV548z5nA43fS2jn1LjeZ0%3D&workerId=rig00

Reply:
{
	"workerId": "rig00",
	"acceptedShareCount1Hour": 1204,
	"acceptedShareCount6Hours": 1417,
	"acceptedShareCount24Hours": 1517,
	"averageJobDifficulty1Hour": 9623,
	"averageJobDifficulty6Hours": 9721,
	"averageJobDifficulty24Hours": 9685,
	"hashrate1Hour": 13.286958548764444,
	"hashrate6Hours": 2.6062597264118517,
	"hashrate24Hours": 0.6515649316029629,
	"invalidShareCount1Hour": 105,
	"invalidShareCount6Hours": 155,
	"invalidShareCount24Hours": 185,
	"lastShareAcceptedOn": 1525768785
}
```

* ## /api/pool/blocks
> Returns an array of the 50 most recent pool blocks found.

```
Request: http://localhost:8087/api/pool/blocks

Reply:
[
	{
		"blockHeight": 18100,
		"isPaid": false,
		"minerPublicKey": "BOatPo5I2lCvg8n0Jtr0yueONu+PWCj16ZKp4+2JfGhkAwCDhFKwk36kjCv348LFlQV548z5nA43fS2jn1LjeZ0=",
		"status": "Unconfirmed (Accepted)",
		"statusId": 3,
		"submittedOn": 1522224123
	},
	{
		"blockHeight": 18095,
		"isPaid": false,
		"minerPublicKey": "BOatPo5I2lCvg8n0Jtr0yueONu+PWCj16ZKp4+2JfGhkAwCDhFKwk36kjCv348LFlQV548z5nA43fS2jn1LjeZ0=",
		"status": "Unconfirmed (Rejected)",
		"statusId": 4,
		"submittedOn": 1522222122
	},
	{
		"blockHeight": 18085,
		"isPaid": false,
		"minerPublicKey": "BOatPo5I2lCvg8n0Jtr0yueONu+PWCj16ZKp4+2JfGhkAwCDhFKwk36kjCv348LFlQV548z5nA43fS2jn1LjeZ0=",
		"status": "Orphaned",
		"statusId": 2,
		"submittedOn": 1522220121
	},
	{
		"blockHeight": 18080,
		"isPaid": true,
		"minerPublicKey": "BOatPo5I2lCvg8n0Jtr0yueONu+PWCj16ZKp4+2JfGhkAwCDhFKwk36kjCv348LFlQV548z5nA43fS2jn1LjeZ0=",
		"status": "Confirmed",
		"statusId": 1,
		"submittedOn": 1522204736
	}
]

StatusId Enum:
1 = Confirmed
2 = Orphaned
3 = Unconfirmed (Accepted)
4 = Unconfirmed (Rejected)
```

* ## /api/pool/stats
> Returns aggregate pool miner stats. Hashrate measurements are in GH/s.

> Boolean {includeMiners} - Optional, default false; Whether or not to include all individual miner stats in the response
```
Request: http://localhost:8087/api/pool/stats?includeMiners=true

Reply:
{
	"individualMinerStats": [
		{
			"individualWorkerStats": [
				{
					"workerId": "worker00",
					"acceptedShareCount1Hour": 36,
					"acceptedShareCount6Hours": 101,
					"acceptedShareCount24Hours": 101,
					"averageJobDifficulty1Hour": 9500,
					"averageJobDifficulty6Hours": 9500,
					"averageJobDifficulty24Hours": 9500,
					"hashrate1Hour": 1.5247133900799998,
					"hashrate6Hours": 0.7129446870281482,
					"hashrate24Hours": 0.17823617175703704,
					"invalidShareCount1Hour": 0,
					"invalidShareCount6Hours": 0,
					"invalidShareCount24Hours": 0,
					"lastShareAcceptedOn": 1525768781
				}
			],
			"publicKey": "BOatPo5I2lCvg8n0Jtr0yueONu+PWCj16ZKp4+2JfGhkAwCDhFKwk36kjCv348LFlQV548z5nA43fS2jn1LjeZ0=",
			"acceptedShareCount1Hour": 250,
			"acceptedShareCount6Hours": 892,
			"acceptedShareCount24Hours": 892,
			"averageJobDifficulty1Hour": 9500,
			"averageJobDifficulty6Hours": 9500,
			"averageJobDifficulty24Hours": 9500,
			"hashrate1Hour": 10.58828743111111,
			"hashrate6Hours": 6.296501592367408,
			"hashrate24Hours": 1.574125398091852,
			"invalidShareCount1Hour": 0,
			"invalidShareCount6Hours": 0,
			"invalidShareCount24Hours": 0,
			"lastShareAcceptedOn": 1525768781
		},
		{
			"individualWorkerStats": [
				{
					"workerId": "w00",
					"acceptedShareCount1Hour": 15,
					"acceptedShareCount6Hours": 15,
					"acceptedShareCount24Hours": 15,
					"averageJobDifficulty1Hour": 9500,
					"averageJobDifficulty6Hours": 9500,
					"averageJobDifficulty24Hours": 9500,
					"hashrate1Hour": 0.6352972458666667,
					"hashrate6Hours": 0.10588287431111111,
					"hashrate24Hours": 0.026470718577777778,
					"invalidShareCount1Hour": 0,
					"invalidShareCount6Hours": 0,
					"invalidShareCount24Hours": 0,
					"lastShareAcceptedOn": 1525768783
				}
			],
			"publicKey": "BGoPcp1g6hf//NdCNTSzJiKK45e5Ylro8v1WJuFZ81qdNt5HlWReEX5QxCcwwi5VEvNdvvC/Q2OU0XxbosjiZdg=",
			"acceptedShareCount1Hour": 80,
			"acceptedShareCount6Hours": 80,
			"acceptedShareCount24Hours": 80,
			"averageJobDifficulty1Hour": 9500,
			"averageJobDifficulty6Hours": 9500,
			"averageJobDifficulty24Hours": 9500,
			"hashrate1Hour": 3.388251977955556,
			"hashrate6Hours": 0.5647086629925926,
			"hashrate24Hours": 0.14117716574814815,
			"invalidShareCount1Hour": 0,
			"invalidShareCount6Hours": 0,
			"invalidShareCount24Hours": 0,
			"lastShareAcceptedOn": 1525768783
		}
	],
	"minerCount": 2,
	"workerCount": 20,
	"acceptedShareCount1Hour": 330,
	"acceptedShareCount6Hours": 972,
	"acceptedShareCount24Hours": 972,
	"averageJobDifficulty1Hour": 9500,
	"averageJobDifficulty6Hours": 9500,
	"averageJobDifficulty24Hours": 9500,
	"hashrate1Hour": 13.976539409066667,
	"hashrate6Hours": 6.8612102553600005,
	"hashrate24Hours": 1.7153025638400001,
	"invalidShareCount1Hour": 0,
	"invalidShareCount6Hours": 0,
	"invalidShareCount24Hours": 0,
	"lastShareAcceptedOn": 1525768783
}
```

* ## /api/admin/servers
> Returns the registered servers on all cache adapters.

```
Request: http://localhost:8087/api/admin/servers

Reply:
{
	"pool": [
		"pool-master-00",
		"pool-slave-00"
	]
}
```

* ## /api/admin/servers/{ServerId}
> Returns brief status information about the server.

> String {ServerId} - Required; The id of the server.
```
Request: http://localhost:8087/api/admin/servers/pool-master-00

Reply:
{
	"category": "Pool",
	"heartbeat": 1527826327,
	"type": "Master"
}
```

* ## /api/admin/servers/{ServerId}/services
> Returns an array of all registered services on the server.

> String {ServerId} - Required; The id of the server.
```
Request: http://localhost:8087/api/admin/servers/pool-master-00/services

Reply:
[
	"BanManagementService",
	"BlockCandidateProcessorService",
	"CacheCleanupService",
	"InvalidShareCleanupService",
	"InvalidShareStorageService_pool-master-00",
	"NodeFailoverService",
	"PaymentSenderService",
	"ShareArchiverService",
	"ShareStorageService_pool-master-00",
	"WorkRetrieverService"
]
```

* ## /api/admin/servers/{ServerId}/services/{ServiceId}
> Returns an array of all registered services on the server.

> String {ServerId} - Required; The id of the server.
> String {ServiceId} - Required; The id of the service.
```
Request: http://localhost:8087/api/admin/servers/pool-master-00/services/PaymentSenderService

Reply:
{
	"currentRunStartedOn": 1527847958,
	"data": {
		"batchCount": 6,
		"currentBatch": 1,
		"errorCount": 0,
		"paymentCount": 16,
		"processedCount": 2,
		"remainingInBatch": 1,
		"remainingTotal": 14,
		"sucessfullCount": 2
	},
	"lastCompletedOn": 1527847721,
	"runtimeStatus": "Running"
}
```