# nVeoPool
Mining pool backend for Amoveo supporting the HTTP-based GetWork protocol and the TCP-based Stratum protocol. nVeoPool is currently split into two components, the pool server and the API server.

## Requirements
* .NET Core 2.x
* PostgreSQL
* Redis

## Documentation
1. [Configuration](docs/configuration.md)
2. [Launching a Server](docs/launching-a-server.md)
3. [API Server Endpoints](docs/api-endpoints.md)
4. [Stratum Protocol](docs/stratum.md)