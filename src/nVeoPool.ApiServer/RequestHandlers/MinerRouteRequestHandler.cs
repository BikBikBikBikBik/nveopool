#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using nVeoPool.Common;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Models;

namespace nVeoPool.ApiServer.RequestHandlers {
	internal class MinerRouteRequestHandler : RequestHandlerBase, IMinerRouteRequestHandler {
		public MinerRouteRequestHandler(IAddressParser addressParser, IDistributedCacheAdapter cache, ILogger<MinerRouteRequestHandler> logger) : base(cache) {
			_addressParser = addressParser;
			_logger = logger;
		}

		#region IMinerRouteRequestHandler
		public Task HandleGetMinerAggregateStatsAsync(HttpContext context, String minerPublicKey) {
			return HandleRouteAsync(context, minerPublicKey, stats => {
				if (!(context.Request.Query.TryGetValue("includeWorkers", out var includeWorkers) && bool.TryParse(includeWorkers.ToString(), out var includeWorkersValue) && includeWorkersValue)) {
					stats.IndividualWorkerStats = null;
				}

				return stats;
			}, nameof(HandleGetMinerAggregateStatsAsync));
		}

		public async Task HandleGetMinerBalanceInfoAsync(HttpContext context, String minerPublicKey) {
			var responseJson = String.Empty;

			try {
				var formattedMinerPublicKey = _addressParser.FormatMinerAddressForCacheKey(minerPublicKey);
				var minerBalance = await _cache.GetMinerBalanceInfoAsync(formattedMinerPublicKey);
				responseJson = JsonConvert.SerializeObject(new { minerBalance.MinimumPayoutThreshold, minerBalance.PendingBalance, minerBalance.TotalPaid }, _jsonSerializerSettings);

				context.Response.ContentType = "application/json";
			} catch (Exception e) {
				_logger.LogError(e, "Exception during {MethodName} execution", nameof(HandleGetMinerBalanceInfoAsync));

				context.Response.StatusCode = 500;
			}

			context.Response.ContentLength = responseJson.Length;
			await context.Response.WriteAsync(responseJson);
		}

		public async Task HandleGetMinerPaymentsAsync(HttpContext context, String minerPublicKey) {
			var responseJson = String.Empty;

			try {
				var formattedMinerPublicKey = _addressParser.FormatMinerAddressForCacheKey(minerPublicKey);
				var minerPayments = await _cache.GetMinerPaymentsAsync(formattedMinerPublicKey);
				responseJson = JsonConvert.SerializeObject(minerPayments.Select(m => new { m.Amount, m.SubmittedOn, m.SubmittedOnBlockHeight, m.TransactionFee, m.TransactionId }).ToArray(), _jsonSerializerSettings);

				context.Response.ContentType = "application/json";
			} catch (Exception e) {
				_logger.LogError(e, "Exception during {MethodName} execution", nameof(HandleGetMinerPaymentsAsync));

				context.Response.StatusCode = 500;
			}

			context.Response.ContentLength = responseJson.Length;
			await context.Response.WriteAsync(responseJson);
		}

		public Task HandleGetWorkerAggregateStatsAsync(HttpContext context, String minerPublicKey, String workerId) {
			return HandleRouteAsync(context, minerPublicKey, stats => stats.IndividualWorkerStats.SingleOrDefault(w => workerId.Equals(w.WorkerId)), nameof(HandleGetWorkerAggregateStatsAsync));
		}

		public Task HandleGetWorkerListForMinerAsync(HttpContext context, String minerPublicKey) {
			return HandleRouteAsync(context, minerPublicKey, stats => stats.IndividualWorkerStats.Where(w => !String.IsNullOrWhiteSpace(w.WorkerId)).Select(w => w.WorkerId).ToArray(), nameof(HandleGetWorkerListForMinerAsync));
		}
		#endregion

		private async Task HandleRouteAsync(HttpContext context, String minerPublicKey, Func<AggregateMinerStats, Object> specificRouteHandler, String methodName) {
			var responseJson = String.Empty;

			try {
				var formattedMinerPublicKey = _addressParser.FormatMinerAddressForCacheKey(minerPublicKey);
				var aggregateStats = await _cache.GetMinerAggregateStatsAsync(formattedMinerPublicKey);

				if (aggregateStats.MinerStats != null && aggregateStats.CalculatedOn >= new DateTimeOffset(DateTime.UtcNow - TimeSpan.FromHours(MAX_CACHE_AGE_HOURS)).ToUnixTimeSeconds()) {
					var response = specificRouteHandler(aggregateStats.MinerStats);
					
					if (response != null) {
						responseJson = JsonConvert.SerializeObject(response, _jsonSerializerSettings);

						context.Response.ContentLength = responseJson.Length;
						context.Response.ContentType = "application/json";
					}
				}

				if (String.IsNullOrWhiteSpace(responseJson)) {
					context.Response.StatusCode = 404;
				}
			} catch (Exception e) {
				_logger.LogError(e, "Exception during {MethodName} execution", methodName);

				context.Response.StatusCode = 500;
			}

			await context.Response.WriteAsync(responseJson);
		}

		private const int MAX_CACHE_AGE_HOURS = 24;
		private readonly IAddressParser _addressParser;
		private readonly ILogger<MinerRouteRequestHandler> _logger;
	}
}
