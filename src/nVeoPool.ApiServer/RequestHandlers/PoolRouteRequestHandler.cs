#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using nVeoPool.Data.Caching;

namespace nVeoPool.ApiServer.RequestHandlers {
	internal class PoolRouteRequestHandler : RequestHandlerBase, IPoolRouteRequestHandler {
		public PoolRouteRequestHandler(IDistributedCacheAdapter cache, ILogger<PoolRouteRequestHandler> logger) : base(cache) {
			_logger = logger;
		}

		#region IPoolRouteRequestHandler
		public async Task HandleGetPoolAggregateStatsAsync(HttpContext context) {
			var responseJson = String.Empty;

			try {
				var poolStats = (await _cache.GetPoolAggregateStatsAsync()).PoolStats;

				if (poolStats != null) {
					if (!(context.Request.Query.TryGetValue("includeMiners", out var includeMiners) && bool.TryParse(includeMiners.ToString(), out var includeMinersValue) && includeMinersValue)) {
						poolStats.IndividualMinerStats = null;
					}
					responseJson = JsonConvert.SerializeObject(poolStats, _jsonSerializerSettings);

					context.Response.ContentLength = responseJson.Length;
					context.Response.ContentType = "application/json";
				} else {
					context.Response.StatusCode = 500;

					_logger.LogWarning("Cache returned null aggregate pool stats in {MethodName}", nameof(HandleGetPoolAggregateStatsAsync));
				}
			} catch (Exception e) {
				context.Response.StatusCode = 500;

				_logger.LogError(e, "Exception during {MethodName} execution", nameof(HandleGetPoolAggregateStatsAsync));
			}

			await context.Response.WriteAsync(responseJson);
		}

		public async Task HandleGetPoolRecentBlocksAsync(HttpContext context) {
			var responseJson = String.Empty;

			try {
				var poolBlocks = (await _cache.GetPoolRecentBlocksAsync()).ToArray();

				responseJson = JsonConvert.SerializeObject(poolBlocks, _jsonSerializerSettings);

				context.Response.ContentLength = responseJson.Length;
				context.Response.ContentType = "application/json";
			} catch (Exception e) {
				context.Response.StatusCode = 500;

				_logger.LogError(e, "Exception during {MethodName} execution", nameof(HandleGetPoolRecentBlocksAsync));
			}

			await context.Response.WriteAsync(responseJson);
		}
		#endregion

		private readonly ILogger<PoolRouteRequestHandler> _logger;
	}
}
