#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using nVeoPool.Data.Caching;

namespace nVeoPool.ApiServer.RequestHandlers {
	internal class AdminRouteRequestHandler : RequestHandlerBase, IAdminRouteRequestHandler {
		public AdminRouteRequestHandler(IDistributedCacheAdapter cache, ILogger<AdminRouteRequestHandler> logger) : base(cache) {
			_logger = logger;
		}

		#region IAdminRouteRequestHandler
		public async Task HandleGetAllServersAsync(HttpContext context) {
			var responseJson = String.Empty;

			try {
				var poolServers = await _cache.GetRegisteredServersAsync("Pool");

				if (poolServers != null) {
					responseJson = JsonConvert.SerializeObject(new { Pool = poolServers.ToArray() }, _jsonSerializerSettings);

					context.Response.ContentLength = responseJson.Length;
					context.Response.ContentType = "application/json";
				} else {
					context.Response.StatusCode = 500;
					
					_logger.LogWarning("Cache returned null server list");
				}
			} catch (Exception e) {
				context.Response.StatusCode = 500;

				_logger.LogError(e, "Exception during {MethodName} execution", nameof(HandleGetAllServersAsync));
			}

			await context.Response.WriteAsync(responseJson);
		}

		public async Task HandleGetAllServicesForServerAsync(HttpContext context) {
			var responseJson = String.Empty;

			try {
				var serverId = (String)context.GetRouteValue("serverId");
				var services = await _cache.GetRegisteredServicesForServerAsync(serverId);

				if (services != null) {
					responseJson = JsonConvert.SerializeObject(services.OrderBy(s => s).ToArray(), _jsonSerializerSettings);

					context.Response.ContentLength = responseJson.Length;
					context.Response.ContentType = "application/json";
				} else {
					context.Response.StatusCode = 500;
					
					_logger.LogWarning("Cache returned null services list for server {ServerId}", serverId);
				}
			} catch (Exception e) {
				context.Response.StatusCode = 500;

				_logger.LogError(e, "Exception during {MethodName} execution", nameof(HandleGetAllServicesForServerAsync));
			}

			await context.Response.WriteAsync(responseJson);
		}

		public async Task HandleGetServiceForServerAsync(HttpContext context) {
			var responseJson = String.Empty;

			try {
				var serverId = (String)context.GetRouteValue("serverId");
				var serviceId = (String)context.GetRouteValue("serviceId");
				var serviceStatus = await _cache.GetServiceStatusAsync(serverId, serviceId);

				if (serviceStatus != null) {
					serviceStatus.Data = !String.IsNullOrWhiteSpace(serviceStatus.Data as String) ? JsonConvert.DeserializeObject<dynamic>((String)serviceStatus.Data) : serviceStatus.Data;
					responseJson = JsonConvert.SerializeObject(serviceStatus, _jsonSerializerSettings);

					context.Response.ContentLength = responseJson.Length;
					context.Response.ContentType = "application/json";
				} else {
					context.Response.StatusCode = 500;
					
					_logger.LogWarning("Cache returned null service status for {ServiceName} on {ServerId}", serviceId, serverId);
				}
			} catch (Exception e) {
				context.Response.StatusCode = 500;

				_logger.LogError(e, "Exception during {MethodName} execution", nameof(HandleGetServiceForServerAsync));
			}

			await context.Response.WriteAsync(responseJson);
		}

		public async Task HandleGetSingleServerAsync(HttpContext context) {
			var responseJson = String.Empty;

			try {
				var serverId = (String)context.GetRouteValue("serverId");
				var serverStatus = await _cache.GetServerStatusAsync(serverId);

				if (serverStatus.Heartbeat.HasValue) {
					responseJson = JsonConvert.SerializeObject(new { Category = "Pool", Heartbeat = serverStatus.Heartbeat.Value, Type = serverStatus.ServerType }, _jsonSerializerSettings);

					context.Response.ContentLength = responseJson.Length;
					context.Response.ContentType = "application/json";
				} else {
					context.Response.StatusCode = 500;
					
					_logger.LogWarning("Cache returned null server status for server {ServerId}", serverId);
				}
			} catch (Exception e) {
				context.Response.StatusCode = 500;

				_logger.LogError(e, "Exception during {MethodName} execution", nameof(HandleGetSingleServerAsync));
			}

			await context.Response.WriteAsync(responseJson);
		}
		#endregion

		private readonly ILogger<AdminRouteRequestHandler> _logger;
	}
}
