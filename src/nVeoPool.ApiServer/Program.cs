﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Serilog;
using Serilog.Events;
using nVeoPool.ApiServer.Configuration;

namespace nVeoPool.ApiServer {
	public class Program {
		public static async Task Main(String[] args) {
			_webHost = new WebHostBuilder()
				.UseStartup<KestrelStartup>()
				#if DEBUG
				.UseSerilog((hostingContext, loggerConfiguration) => {
					var loggerConfig = loggerConfiguration.MinimumLevel.Verbose().WriteTo.Console(LogEventLevel.Debug);

					SetHttpRequestLoggingFilter(loggerConfig);
				})
				#else
				.UseSerilog((hostingContext, loggerConfiguration) => {
					var loggerConfig = loggerConfiguration.MinimumLevel.Verbose().WriteTo.File(ApiServerConfiguration.Instance.LoggingConfiguration.FileName, ApiServerConfiguration.Instance.LoggingConfiguration.Level, buffered: true, flushToDiskInterval: TimeSpan.FromSeconds(30), outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{SourceContext}]{Scope}[{Level:u3}] {Message:lj}{NewLine}{Exception}", retainedFileCountLimit: 365, rollingInterval: RollingInterval.Day, rollOnFileSizeLimit: true);

					SetHttpRequestLoggingFilter(loggerConfig);
				})
				#endif
				.UseKestrel(options => {
					options.AddServerHeader = false;
					options.Listen(ApiServerConfiguration.Instance.HttpConfiguration.ListenAddress, ApiServerConfiguration.Instance.HttpConfiguration.ListenPort);
				})
				.Build();
			_webHost.Start();
			
			Console.CancelKeyPress += async (s,e) => await OnCancelKeyPress(s, e);
			Console.WriteLine($"{ApiServerConfiguration.Instance.ClusteringConfiguration.ServerType} API server with id {ApiServerConfiguration.Instance.ClusteringConfiguration.ServerId} started...");
			Console.ReadLine();

			await Shutdown();
		}

		private static async Task OnCancelKeyPress(Object sender, ConsoleCancelEventArgs eventArgs) {
			await Shutdown();
		}

		private static void SetHttpRequestLoggingFilter(LoggerConfiguration loggerConfiguration) {
			if (!ApiServerConfiguration.Instance.HttpConfiguration.EnableRequestLogging) {
				loggerConfiguration.Filter.ByExcluding(e => e.Properties.TryGetValue("SourceContext", out var sourceContext) && sourceContext.ToString().Contains("Microsoft.AspNetCore"));
			}
		}

		private static async Task Shutdown() {
			try {
				await _webHost.StopAsync(ApiServerConfiguration.Instance.HttpConfiguration.MaximumGracefulCloseWaitSeconds > 0 ? TimeSpan.FromSeconds(ApiServerConfiguration.Instance.HttpConfiguration.MaximumGracefulCloseWaitSeconds) : TimeSpan.MaxValue);

				_webHost.Dispose();
			} finally {
				Process.GetCurrentProcess().Close();
			}
		}

		private static IWebHost _webHost;
	}
}
