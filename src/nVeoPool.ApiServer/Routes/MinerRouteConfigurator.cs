﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using nVeoPool.ApiServer.RequestHandlers;

namespace nVeoPool.ApiServer.Routes {
	internal class MinerRouteConfigurator : IMinerRouteConfigurator {
		public MinerRouteConfigurator(IMinerRouteRequestHandler minerRouteRequestHandler) {
			_minerRouteRequestHandler = minerRouteRequestHandler;
		}

		#region IMinerRouteConfigurator
		public void Configure(IRouteBuilder routeBuilder) {
			routeBuilder.MapGet("api/miners", context => HandleBasicRequestAsync(context, _minerRouteRequestHandler.HandleGetMinerAggregateStatsAsync));
			routeBuilder.MapGet("api/miners/balance", context => HandleBasicRequestAsync(context, _minerRouteRequestHandler.HandleGetMinerBalanceInfoAsync));
			routeBuilder.MapGet("api/miners/payment", context => HandleBasicRequestAsync(context, _minerRouteRequestHandler.HandleGetMinerPaymentsAsync));
			routeBuilder.MapGet("api/miners/workers", context => HandleBasicRequestAsync(context, async (ctxt, minerPublicKey) => {
				if (TryGetAndParseBase64String(context.Request.Query, "workerId", out var workerId)) {
					await _minerRouteRequestHandler.HandleGetWorkerAggregateStatsAsync(ctxt, minerPublicKey, workerId);
				} else {
					await _minerRouteRequestHandler.HandleGetWorkerListForMinerAsync(ctxt, minerPublicKey);
				}
			}));
		}
		#endregion

		private Task HandleBasicRequestAsync(HttpContext context, Func<HttpContext, String, Task> routeHandler) {
			if (TryGetAndParseBase64String(context.Request.Query, "address", out var minerPublicKey)) {
				return routeHandler(context, minerPublicKey);
			} else {
				context.Response.StatusCode = 404;
				return context.Response.WriteAsync(String.Empty);
			}
		}

		private bool TryGetAndParseBase64String(IQueryCollection query, String parameterName, out String formattedParameterValue) {
			formattedParameterValue = null;

			if (query.TryGetValue(parameterName, out var parameterValue)) {
				formattedParameterValue = parameterValue.ToString().Replace(' ', '+');

				return true;
			}

			return false;
		}

		private readonly IMinerRouteRequestHandler _minerRouteRequestHandler;
	}
}
