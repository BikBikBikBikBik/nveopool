#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.ApiServer.Configuration;
using nVeoPool.Common;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Common.Extensions;
using nVeoPool.Common.Services;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Models;
using nVeoPool.Data.Repositories;

namespace nVeoPool.ApiServer.Services {
	internal class DataCacherService : BackgroundServiceBase {
		public DataCacherService(IAddressParser addressParser, IApiServerConfiguration apiServerConfiguration, IBlockRepository blockRepository, IDistributedCacheAdapter cache, ILogger<DataCacherService> logger, IPaymentRepository paymentRepository, IWorkerRepository workerRepository) : base(apiServerConfiguration.ClusteringConfiguration, logger, apiServerConfiguration.ServicesConfiguration.DataCacherServiceUpdateIntervalSeconds, new [] { ClusterServerType.Master, ClusterServerType.Slave }, nameof(DataCacherService)) {
			_addressParser = addressParser;
			_blockRepository = blockRepository;
			_cache = cache;
			_paymentRepository = paymentRepository;
			_workerRepository = workerRepository;
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			cancellationToken.Register(LogStoppingMessage);

			while (!cancellationToken.IsCancellationRequested) {
				try {
					LogExecutingMessage();

					var minerBalanceInfoTask = _paymentRepository.GetAllMinerBalancesAsync();
					var minerPaymentHistoryTask = _paymentRepository.GetAllMinerPaymentsAsync(new DateTimeOffset(DateTime.UtcNow - TimeSpan.FromDays(MAX_MINER_PAYMENT_AGE_DAYS)).ToUnixTimeSeconds());
					var poolBlocksTask = _blockRepository.GetAllBlockCandidatesAsync(MAX_POOL_BLOCK_COUNT);

					var minerStats = (await _workerRepository.CalculateMinerStatsAsync()).ToList();
					foreach (var stats in minerStats) {
						_cache.UpdateMinerAggregateStatsImmediate(_addressParser.FormatMinerAddressForCacheKey(stats.PublicKey), stats);
					}

					var poolStats = new AggregatePoolStats {
						AcceptedShareCount1Hour = minerStats.Sum(m => m.AcceptedShareCount1Hour),
						AcceptedShareCount6Hours = minerStats.Sum(m => m.AcceptedShareCount6Hours),
						AcceptedShareCount24Hours = minerStats.Sum(m => m.AcceptedShareCount24Hours),
						AverageJobDifficulty1Hour = minerStats.Any() ? (int)minerStats.Average(m => m.AverageJobDifficulty1Hour) : 0,
						AverageJobDifficulty6Hours = minerStats.Any() ? (int)minerStats.Average(m => m.AverageJobDifficulty6Hours) : 0,
						AverageJobDifficulty24Hours = minerStats.Any() ? (int)minerStats.Average(m => m.AverageJobDifficulty24Hours) : 0,
						Hashrate1Hour = minerStats.Sum(m => m.Hashrate1Hour),
						Hashrate6Hours = minerStats.Sum(m => m.Hashrate6Hours),
						Hashrate24Hours = minerStats.Sum(m => m.Hashrate24Hours),
						IndividualMinerStats = minerStats,
						InvalidShareCount1Hour = minerStats.Sum(m => m.InvalidShareCount1Hour),
						InvalidShareCount6Hours = minerStats.Sum(m => m.InvalidShareCount6Hours),
						InvalidShareCount24Hours = minerStats.Sum(m => m.InvalidShareCount24Hours),
						LastShareAcceptedOn = minerStats.Any() ? minerStats.Max(m => m.LastShareAcceptedOn) : 0,
						MinerCount = minerStats.Count,
						WorkerCount = minerStats.Select(m => m.IndividualWorkerStats.Count).Sum()
					};
					_cache.UpdatePoolAggregateStatsImmediate(poolStats);

					var minerBalanceInfo = await minerBalanceInfoTask;
					foreach (var (minerPublicKey, minimumPayoutThreshold, pendingBalance, totalPaid) in minerBalanceInfo) {
						_cache.UpdateMinerBalanceInfoImmediate(_addressParser.FormatMinerAddressForCacheKey(minerPublicKey), minimumPayoutThreshold, pendingBalance.TruncateToPaymentPrecision(), totalPaid);
					}

					var minerPaymentHistory = await minerPaymentHistoryTask;
					var minerPaymentGroups = minerPaymentHistory.GroupBy(m => m.MinerPublicKey, m => (m.Amount, m.SubmittedOn, m.SubmittedOnBlockHeight, m.TransactionFee, m.TransactionId));
					foreach (var minerPayments in minerPaymentGroups) {
						_cache.UpdateMinerPaymentsImmediate(_addressParser.FormatMinerAddressForCacheKey(minerPayments.Key), minerPayments);
					}

					var poolBlocks = await poolBlocksTask;
					_cache.UpdatePoolRecentBlocksImmediate(poolBlocks);
				} catch (Exception e) {
					LogExceptionMessage(e);
				}

				LogSleepingMessage();
				await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
			}
		}
		#endregion

		private const int MAX_MINER_PAYMENT_AGE_DAYS = 30;
		private const int MAX_POOL_BLOCK_COUNT = 50;
		private readonly IAddressParser _addressParser;
		private readonly IBlockRepository _blockRepository;
		private readonly IDistributedCacheAdapter _cache;
		private readonly IPaymentRepository _paymentRepository;
		private readonly IWorkerRepository _workerRepository;
	}
}
