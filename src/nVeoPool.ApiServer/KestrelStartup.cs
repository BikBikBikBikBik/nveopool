﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using nVeoPool.ApiServer.Configuration;
using nVeoPool.ApiServer.RequestHandlers;
using nVeoPool.ApiServer.Routes;
using nVeoPool.ApiServer.Services;
using nVeoPool.Common;
using nVeoPool.Common.Routing;
using nVeoPool.Data;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Configuration;
using nVeoPool.Data.Context;
using nVeoPool.Data.Repositories;

namespace nVeoPool.ApiServer {
	internal class KestrelStartup {
		public KestrelStartup() {
			_apiServerConfiguration = ApiServerConfiguration.Instance;
		}

		public void Configure(IApplicationBuilder app, IServiceProvider serviceProvider) {
			app.UseRouter(serviceProvider.GetService<IRouteConfigurator>().Configure);
		}
		
		public void ConfigureServices(IServiceCollection services) {
			services.AddRouting();

			services.AddSingleton<IAddressParser, AddressParser>();
			services.AddSingleton<IAdminRouteConfigurator, AdminRouteConfigurator>();
			services.AddSingleton<IAdminRouteRequestHandler, AdminRouteRequestHandler>();
			services.AddSingleton<IApiServerConfiguration, ApiServerConfiguration>(provider => ApiServerConfiguration.Instance);
			services.AddSingleton<IBlockRepository, BlockRepository>(provider => new BlockRepository(new PostgresDatabaseContext(_apiServerConfiguration.DatabaseConfiguration.ConnectionString)));
			services.AddSingleton<IConnectionFactory, RedisConnectionFactory>(provider => new RedisConnectionFactory(_apiServerConfiguration.CacheConfiguration.ConnectionString));
			services.AddSingleton<IDistributedCacheAdapter, DistributedCacheAdapter>();
			services.AddSingleton<IDistributedCacheKeyConfiguration, DistributedCacheKeyConfiguration>();
			services.AddSingleton<IHostedService, DataCacherService>();
			services.AddSingleton<IMinerRouteConfigurator, MinerRouteConfigurator>();
			services.AddSingleton<IMinerRouteRequestHandler, MinerRouteRequestHandler>();
			services.AddSingleton<IPaymentRepository, PaymentRepository>(provider => new PaymentRepository(new PostgresDatabaseContext(_apiServerConfiguration.DatabaseConfiguration.ConnectionString)));
			services.AddSingleton<IPoolRouteConfigurator, PoolRouteConfigurator>();
			services.AddSingleton<IPoolRouteRequestHandler, PoolRouteRequestHandler>();
			services.AddSingleton<IRouteConfigurator, CompleteApiRouteConfigurator>();
			services.AddSingleton<IWorkerRepository, WorkerRepository>(provider => new WorkerRepository(new PostgresDatabaseContext(_apiServerConfiguration.DatabaseConfiguration.ConnectionString)));
		}

		private readonly IApiServerConfiguration _apiServerConfiguration;
	}
}
