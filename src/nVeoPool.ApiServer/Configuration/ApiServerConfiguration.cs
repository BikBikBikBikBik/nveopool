#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using Newtonsoft.Json;
using nVeoPool.ApiServer.Configuration.Services;
using nVeoPool.Common.Configuration;
using nVeoPool.Common.Configuration.Caching;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Common.Configuration.Daemon;
using nVeoPool.Common.Configuration.Database;
using nVeoPool.Common.Configuration.Http;
using nVeoPool.Common.Configuration.Logging;

namespace nVeoPool.ApiServer.Configuration {
	internal class ApiServerConfiguration : ServerConfigurationBase, IApiServerConfiguration {
		static ApiServerConfiguration() {
			Instance = ReadConfiguration<ApiServerConfiguration>(Environment.GetEnvironmentVariable("NVEOPOOL_API_SETTINGS") ?? "api-settings.json");
		}

		public ApiServerConfiguration(RedisCacheConfiguration cacheConfiguration, ClusteringConfiguration clusteringConfiguration, DaemonClientConfiguration daemonClientConfiguration, DatabaseConfiguration databaseConfiguration, HttpConfiguration httpConfiguration, LoggingConfiguration loggingConfiguration, ServicesConfiguration servicesConfiguration) : base(cacheConfiguration, clusteringConfiguration, daemonClientConfiguration, databaseConfiguration, httpConfiguration, loggingConfiguration) {
			ServicesConfiguration = servicesConfiguration;
		}

		public static ApiServerConfiguration Instance { get; }

		#region IApiServerConfiguration
		[JsonProperty(PropertyName = "ServiceTimers")]
		public IServicesConfiguration ServicesConfiguration { get; set; }
		#endregion
	}
}
