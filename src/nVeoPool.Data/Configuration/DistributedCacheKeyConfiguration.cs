#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;

namespace nVeoPool.Data.Configuration {
	public class DistributedCacheKeyConfiguration : IDistributedCacheKeyConfiguration {
		#region IDistributedCacheKeyConfiguration
		public String ActiveWorkersKey => "workers:active";

		public String MinerAggregateStatsKey(String minerPublicKey) => $"miners:{minerPublicKey}:stats";

		public String MinerBalanceInfoKey(String minerPublicKey) => $"miners:{minerPublicKey}:balance";

		public String MinerPaymentsKey(String minerPublicKey) => $"miners:{minerPublicKey}:payments";

		public String MinerRecentSharesKey(String minerPublicKey) => $"miners:{minerPublicKey}:shares";

		public String MinerTransactionHistoryKey(String minerPublicKey) => $"miners:{minerPublicKey}:transactions";

		public String NodeMiningDataKey => "miningData";

		public String PoolAggregateStatsKey => "pool:stats";

		public String PoolPublicKeyWhitelistKey => "pool:allowedPubKeys";

		public String PoolPublicKeyWhitelistKeyTempEmpty => "pool:apkEmpty";
		
		public String PoolPublicKeyWhitelistKeyTempNew => "pool:apkNew";

		public String PoolRecentBlocksKey => "pool:blocks";

		public String RegisteredServersKey(String serverCategory) => $"admin:{serverCategory}:svrs";

		public String RegisteredServicesOnCacheKey => "admin:svcs";

		public String RegisteredServicesOnServerKey(String serverId) => $"admin:{serverId}:svcs";

		public String ServerStatusKey(String serverId) => $"admin:{serverId}:status";

		public String ServiceStatusKey(String serverId, String serviceName) => $"admin:{serverId}:{serviceName}:status";

		public String WorkerRecentShareIntervalsKey(String workerAddress) => $"workers:{workerAddress}:shareIntervals";
		
		public String WorkerStatsKey(String workerAddress) => $"workers:{workerAddress}:stats";
		#endregion
	}
}
