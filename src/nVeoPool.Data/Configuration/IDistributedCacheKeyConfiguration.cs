#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;

namespace nVeoPool.Data.Configuration {
	public interface IDistributedCacheKeyConfiguration {
		String ActiveWorkersKey { get; }

		String MinerAggregateStatsKey(String minerPublicKey);

		String MinerBalanceInfoKey(String minerPublicKey);

		String MinerPaymentsKey(String minerPublicKey);

		String MinerRecentSharesKey(String minerPublicKey);

		String MinerTransactionHistoryKey(String minerPublicKey);

		String NodeMiningDataKey { get; }

		String PoolAggregateStatsKey { get; }

		String PoolPublicKeyWhitelistKey { get; }

		String PoolPublicKeyWhitelistKeyTempEmpty { get; }
		
		String PoolPublicKeyWhitelistKeyTempNew { get; }

		String PoolRecentBlocksKey { get; }

		String RegisteredServersKey(String serverCategory);

		String RegisteredServicesOnCacheKey { get; }

		String RegisteredServicesOnServerKey(String serverId);

		String ServerStatusKey(String serverId);

		String ServiceStatusKey(String serverId, String serviceName);

		String WorkerRecentShareIntervalsKey(String workerAddress);

		String WorkerStatsKey(String workerAddress);
	}
}
