#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Common.Services;
using nVeoPool.Data.Caching;

namespace nVeoPool.Data.Services {
	public abstract class ReportingBackgroundServiceBase : BackgroundServiceBase {
		protected ReportingBackgroundServiceBase(IDistributedCacheAdapter cache, IClusteringConfiguration clusteringConfiguration, ILogger logger, uint runInterval, ClusterServerType runOnServerType, String serviceName) : this(cache, clusteringConfiguration, logger, runInterval, new [] { runOnServerType }, serviceName) {
		}

		protected ReportingBackgroundServiceBase(IDistributedCacheAdapter cache, IClusteringConfiguration clusteringConfiguration, ILogger logger, uint runInterval, IEnumerable<ClusterServerType> runOnServerTypes, String serviceName) : this(cache, clusteringConfiguration, logger, runInterval, 1, runOnServerTypes, serviceName) {
		}

		protected ReportingBackgroundServiceBase(IDistributedCacheAdapter cache, IClusteringConfiguration clusteringConfiguration, ILogger logger, uint runInterval, decimal runIntervalMultiplier, IEnumerable<ClusterServerType> runOnServerTypes, String serviceName) : base(clusteringConfiguration, logger, runInterval, runOnServerTypes, serviceName) {
			_cache = cache;
			_runIntervalInSeconds = (uint)Math.Ceiling(runInterval * runIntervalMultiplier);
		}

		#region IHostedService
		public override async Task StartAsync(CancellationToken cancellationToken) {
			if (_enabled) {
				if (_clusteringConfiguration.ServerType != ClusterServerType.Master && await _cache.ServiceIsRegisteredOnCacheAsync(_serviceName)) {
					_logger.LogDebug("{BackgroundService} is already registered on another server, stopping on {ServerId}", _serviceName, _clusteringConfiguration.ServerId);

					CancelStartup();
				} else {
					await _cache.RegisterServiceAsync(_clusteringConfiguration.ServerId, _serviceName);

					_logger.LogDebug("{BackgroundService} successfully registered on {ServerId}", _serviceName, _clusteringConfiguration.ServerId);
				}
			}

			await base.StartAsync(cancellationToken);
		}
		#endregion

		protected virtual Task ReportServiceAsExecutingAsync() {
			return ReportServiceAsExecutingAsync(null);
		}

		protected virtual async Task ReportServiceAsExecutingAsync(EventId? loggingEventId) {
			if (loggingEventId.HasValue) {
				LogExecutingMessage(loggingEventId.Value);
			} else {
				LogExecutingMessage();
			}

			try {
				await _cache.UpdateServiceStatusAsRunningAsync(_clusteringConfiguration.ServerId, _serviceName, new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds());
			} catch (Exception e) {
				_logger.LogWarning(e, "{BackgroundService} failed to update runtime status as {ServiceStatus}", _serviceName, "Running");
			}
		}

		protected virtual async Task ReportServiceAsSleepingAsync() {
			LogSleepingMessage();

			var currentTime = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();

			try {
				await _cache.UpdateServiceStatusAsSleepingAsync(_clusteringConfiguration.ServerId, _serviceName, currentTime + _runIntervalInSeconds, currentTime);
			} catch (Exception e) {
				_logger.LogWarning(e, "{BackgroundService} failed to update runtime status as {ServiceStatus}", _serviceName, "Sleeping");
			}
		}

		protected virtual async Task ReportServiceAsStoppingAsync() {
			LogStoppingMessage();

			try {
				await _cache.UpdateServiceStatusAsStoppedAsync(_clusteringConfiguration.ServerId, _serviceName);
			} catch (Exception e) {
				_logger.LogWarning(e, "{BackgroundService} failed to update runtime status as {ServiceStatus}", _serviceName, "Stopped");
			}

			try {
				await _cache.UnregisterServiceAsync(_clusteringConfiguration.ServerId, _serviceName);
			} catch (Exception e) {
				_logger.LogWarning(e, "{BackgroundService} failed to unregister on {ServerId}", _serviceName, _clusteringConfiguration.ServerId);
			}
		}

		protected virtual void ReportUnhandledServiceException(Exception e) {
			LogExceptionMessage(e);
		}

		protected virtual async Task UpdateServiceRuntimeDataAsync(Object data) {
			try {
				await _cache.UpdateServiceStatusDataAsync(_clusteringConfiguration.ServerId, _serviceName, JsonConvert.SerializeObject(data, _jsonSerializerSettings));
			} catch (Exception e) {
				_logger.LogWarning(e, "{BackgroundService} failed to update runtime data", _serviceName);
			}
		}

		protected readonly IDistributedCacheAdapter _cache;
		protected static readonly JsonSerializerSettings _jsonSerializerSettings = new JsonSerializerSettings {
			ContractResolver = new CamelCasePropertyNamesContractResolver(),
			Converters = new List<JsonConverter> { new StringEnumConverter() },
			NullValueHandling = NullValueHandling.Ignore
		};
		private readonly uint _runIntervalInSeconds;
	}
}
