#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Models;

namespace nVeoPool.Data.Caching {
	public interface IDistributedCacheAdapter {
		void ClearWorkerCurrentJobDifficultyImmediate(String minerAddress);
		
		Task<bool> ClearWorkerInfoAsync(String workerAddress);

		Task<bool> ClearWorkerRecentShareIntervalsAsync(String workerAddress, long maximumListLength = 0);

		bool DeleteNodeMiningDataImmediate();

		Task<IEnumerable<String>> GetActiveWorkersAsync();

		Task<(AggregateMinerStats MinerStats, long CalculatedOn)> GetMinerAggregateStatsAsync(String minerPublicKey);

		Task<(decimal? MinimumPayoutThreshold, decimal PendingBalance, decimal TotalPaid)> GetMinerBalanceInfoAsync(String minerPublicKey);

		Task<IEnumerable<(decimal Amount, long SubmittedOn, int? SubmittedOnBlockHeight, decimal TransactionFee, String TransactionId)>> GetMinerPaymentsAsync(String minerPublicKey);

		Task<(String BlockHash, int BlockDifficulty, int CurrentHeight, int FieldZero, ulong NodeClientId)> GetNodeMiningDataAsync();

		Task<(AggregatePoolStats PoolStats, long CalculatedOn)> GetPoolAggregateStatsAsync();

		Task<IEnumerable<BlockCandidate>> GetPoolRecentBlocksAsync();

		Task<IEnumerable<String>> GetRegisteredServersAsync(String serverCategory);

		Task<IEnumerable<String>> GetRegisteredServicesForServerAsync(String serverId);

		Task<(long? Heartbeat, ClusterServerType ServerType)> GetServerStatusAsync(String serverId);

		Task<ServiceStatusInfo> GetServiceStatusAsync(String serverId, String serviceName);

		Task<int?> GetWorkerCurrentJobDifficultyAsync(String minerAddress);

		Task<long?> GetWorkerLastSeenTimeAsync(String minerAddress);

		Task<long?> GetWorkerLastShareTimeAsync(String minerAddress);

		Task<IEnumerable<int>> GetWorkerRecentShareIntervalsAsync(String minerAddress);

		Task<bool> MinerIsWhitelistedAsync(String minerPublicKey);

		Task<bool> MinerShareIsDuplicateAsync(String minerPublicKey, String nonce);

		Task<bool> RegisterServerAsync(String serverCategory, String serverId, ClusterServerType serverType);

		Task<bool> RegisterServiceAsync(String serverId, String serviceName);

		void RemoveMinerRecentSharesImmediate(String minerPublicKey, IEnumerable<String> nonces);

		Task<bool> ServiceIsRegisteredOnCacheAsync(String serviceName);

		Task<bool> UnregisterServerAsync(String serverCategory, String serverId);

		Task<bool> UnregisterServiceAsync(String serverId, String serviceName);

		void UpdateMinerAggregateStatsImmediate(String minerPublicKey, AggregateMinerStats aggregateMinerStats);

		void UpdateMinerBalanceInfoImmediate(String minerPublicKey, decimal? minimumPayoutThreshold, decimal pendingBalance, decimal totalPaid);

		void UpdateMinerPaymentsImmediate(String minerPublicKey, IEnumerable<(decimal Amount, long SubmittedOn, int? SubmittedOnBlockHeight, decimal TransactionFee, String TransactionId)> payments);

		void UpdateNodeMiningDataImmediate(String blockHash, int blockDifficulty, int currentHeight, int fieldZero, ulong nodeClientId);

		void UpdatePoolAggregateStatsImmediate(AggregatePoolStats aggregatePoolStats);

		void UpdatePoolRecentBlocksImmediate(IEnumerable<BlockCandidate> blockCandidates);

		Task<long> UpdatePublicKeyWhitelistAsync(IEnumerable<String> minerPublicKeys);

		Task<bool> UpdateServerHeartbeatAsync(String serverId);

		Task<bool> UpdateServiceStatusAsRunningAsync(String serverId, String serviceName, long currentRunStartedOn);
		
		Task<bool> UpdateServiceStatusAsSleepingAsync(String serverId, String serviceName, long estimatedNextRunOn, long lastCompletedOn);

		Task<bool> UpdateServiceStatusAsStoppedAsync(String serverId, String serviceName);

		Task<bool> UpdateServiceStatusDataAsync(String serverId, String serviceName, Object data);

		void UpdateWorkerCurrentJobDifficultyImmediate(String minerAddress, int difficulty);
		
		void UpdateWorkerHeartbeatImmediate(String minerAddress);

		Task UpdateWorkerRecentSharesAsync(String minerAddress, String minerPublicKey, String nonce, long shareSubmittedOn);
	}
}