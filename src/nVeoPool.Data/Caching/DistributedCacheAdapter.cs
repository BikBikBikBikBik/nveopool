#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StackExchange.Redis;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Configuration;
using nVeoPool.Data.Extensions;
using nVeoPool.Data.Models;

namespace nVeoPool.Data.Caching {
	public class DistributedCacheAdapter : IDistributedCacheAdapter {
		public DistributedCacheAdapter(IConnectionFactory connectionFactory, IDistributedCacheKeyConfiguration distributedCacheKeyConfiguration) {
			_connectionFactory = connectionFactory;			
			_distributedCacheKeyConfiguration = distributedCacheKeyConfiguration;
		}

		#region IDistributedCacheAdapter
		public void ClearWorkerCurrentJobDifficultyImmediate(String minerAddress) {
			_connectionFactory.GetDatabase().HashDelete(_distributedCacheKeyConfiguration.WorkerStatsKey(minerAddress), "currentDiff", CommandFlags.FireAndForget);
		}

		public async Task<bool> ClearWorkerInfoAsync(String workerAddress) {
			await ClearWorkerRecentShareIntervalsAsync(workerAddress);

			_connectionFactory.GetDatabase().SetRemove(_distributedCacheKeyConfiguration.ActiveWorkersKey, workerAddress, CommandFlags.FireAndForget);

			return _connectionFactory.GetDatabase().KeyDelete(_distributedCacheKeyConfiguration.WorkerStatsKey(workerAddress), CommandFlags.FireAndForget);
		}
		
		public async Task<bool> ClearWorkerRecentShareIntervalsAsync(String workerAddress, long maximumListLength = 0) {
			if (maximumListLength < 1) {
				return _connectionFactory.GetDatabase().KeyDelete(_distributedCacheKeyConfiguration.WorkerRecentShareIntervalsKey(workerAddress), CommandFlags.FireAndForget);
			} else {
				var length = await _connectionFactory.GetDatabase().ListLengthAsync(_distributedCacheKeyConfiguration.WorkerRecentShareIntervalsKey(workerAddress));

				if (length > (maximumListLength * 2)) {
					await _connectionFactory.GetDatabase().ListTrimAsync(_distributedCacheKeyConfiguration.WorkerRecentShareIntervalsKey(workerAddress), -maximumListLength, -1);
				}
			}

			return true;
		}

		public bool DeleteNodeMiningDataImmediate() {
			return _connectionFactory.GetDatabase().KeyDelete(_distributedCacheKeyConfiguration.NodeMiningDataKey, CommandFlags.FireAndForget);
		}

		public async Task<IEnumerable<String>> GetActiveWorkersAsync() {
			return (await _connectionFactory.GetDatabase().SetMembersAsync(_distributedCacheKeyConfiguration.ActiveWorkersKey)).ToStringArray();
		}

		public async Task<(AggregateMinerStats MinerStats, long CalculatedOn)> GetMinerAggregateStatsAsync(String minerPublicKey) {
			var aggregateStatsData = await _connectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.MinerAggregateStatsKey(minerPublicKey), new RedisValue[] { "aggregate", "calculatedOn" });

			return ((AggregateMinerStats)((byte[])aggregateStatsData[0]).ToObject(), (long)aggregateStatsData[1]);
		}

		public async Task<(decimal? MinimumPayoutThreshold, decimal PendingBalance, decimal TotalPaid)> GetMinerBalanceInfoAsync(String minerPublicKey) {
			var balanceInfoData = await _connectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.MinerBalanceInfoKey(minerPublicKey), new RedisValue[] { "minimumPayoutThreshold", "pendingBalance", "totalPaid" });

			return (String.IsNullOrWhiteSpace((String)balanceInfoData[0]) ? (decimal?)null : decimal.Parse((String)balanceInfoData[0]), decimal.Parse((String)balanceInfoData[1] ?? "0"), decimal.Parse((String)balanceInfoData[2] ?? "0"));
		}

		public async Task<IEnumerable<(decimal Amount, long SubmittedOn, int? SubmittedOnBlockHeight, decimal TransactionFee, String TransactionId)>> GetMinerPaymentsAsync(String minerPublicKey) {
			var minerPaymentsData = (byte[])(await _connectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.MinerPaymentsKey(minerPublicKey), "payments"));

			return minerPaymentsData != null ? ((decimal Amount, long SubmittedOn, int? SubmittedOnBlockHeight, decimal TransactionFee, String TransactionId)[])minerPaymentsData.ToObject() : new (decimal Amount, long SubmittedOn, int? SubmittedOnBlockHeight, decimal TransactionFee, String TransactionId)[0];
		}

		public async Task<(String BlockHash, int BlockDifficulty, int CurrentHeight, int FieldZero, ulong NodeClientId)> GetNodeMiningDataAsync() {
			var nodeMiningData = await _connectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.NodeMiningDataKey, new RedisValue[] { "blockHash", "blockDiff", "height", "f0", "nodeClient" });

			return (nodeMiningData[0], (int)nodeMiningData[1], (int)nodeMiningData[2], (int)nodeMiningData[3], (ulong)nodeMiningData[4]);
		}

		public async Task<(AggregatePoolStats PoolStats, long CalculatedOn)> GetPoolAggregateStatsAsync() {
			var aggregateStatsData = await _connectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.PoolAggregateStatsKey, new RedisValue[] { "aggregate", "calculatedOn" });

			return ((AggregatePoolStats)((byte[])aggregateStatsData[0]).ToObject(), (long)aggregateStatsData[1]);
		}

		public async Task<IEnumerable<BlockCandidate>> GetPoolRecentBlocksAsync() {
			var blockCandidatesData = (byte[])(await _connectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.PoolRecentBlocksKey, "blocks"));

			return blockCandidatesData != null ? (BlockCandidate[])blockCandidatesData.ToObject() : new BlockCandidate[0];
		}

		public async Task<IEnumerable<String>> GetRegisteredServersAsync(String serverCategory) {
			return (await _connectionFactory.GetDatabase().SetMembersAsync(_distributedCacheKeyConfiguration.RegisteredServersKey(serverCategory))).ToStringArray();
		}

		public async Task<IEnumerable<String>> GetRegisteredServicesForServerAsync(String serverId) {
			return (await _connectionFactory.GetDatabase().SetMembersAsync(_distributedCacheKeyConfiguration.RegisteredServicesOnServerKey(serverId))).ToStringArray();
		}

		public async Task<(long? Heartbeat, ClusterServerType ServerType)> GetServerStatusAsync(String serverId) {
			var serverStatusData = await _connectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.ServerStatusKey(serverId), new RedisValue[] { "heartbeat", "type" });

			return ((long?)serverStatusData[0], (ClusterServerType)(int)serverStatusData[1]);
		}

		public async Task<ServiceStatusInfo> GetServiceStatusAsync(String serverId, String serviceName) {
			var serviceStatusData = await _connectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), new RedisValue[] { "currentStartedOn", "lastCompletedOn", "nextRunOn", "status", "data" });

			return !serviceStatusData[3].IsNull ? new ServiceStatusInfo { CurrentRunStartedOn = (long?)serviceStatusData[0], Data = ((byte[])serviceStatusData[4]).ToObject(), EstimatedNextRunOn = (long?)serviceStatusData[2], LastCompletedOn = (long)serviceStatusData[1], RuntimeStatus = (ServiceRuntimeStatusType)(int)serviceStatusData[3] } : null;
		}

		public async Task<int?> GetWorkerCurrentJobDifficultyAsync(String minerAddress) {
			return (int?)(await _connectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.WorkerStatsKey(minerAddress), "currentDiff"));
		}

		public async Task<long?> GetWorkerLastSeenTimeAsync(String minerAddress) {
			return (long?)(await _connectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.WorkerStatsKey(minerAddress), "heartbeat"));
		}

		public async Task<long?> GetWorkerLastShareTimeAsync(String minerAddress) {
			return (long?)(await _connectionFactory.GetDatabase().HashGetAsync(_distributedCacheKeyConfiguration.WorkerStatsKey(minerAddress), "lastShareOn"));
		}

		public async Task<IEnumerable<int>> GetWorkerRecentShareIntervalsAsync(String minerAddress) {
			return (await _connectionFactory.GetDatabase().ListRangeAsync(_distributedCacheKeyConfiguration.WorkerRecentShareIntervalsKey(minerAddress))).ToIntArray();
		}

		public Task<bool> MinerIsWhitelistedAsync(String minerPublicKey) {
			return _connectionFactory.GetDatabase().SetContainsAsync(_distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKey, minerPublicKey);
		}

		public Task<bool> MinerShareIsDuplicateAsync(String minerPublicKey, String nonce) {
			return _connectionFactory.GetDatabase().SetContainsAsync(_distributedCacheKeyConfiguration.MinerRecentSharesKey(minerPublicKey), nonce);
		}

		public async Task<bool> RegisterServerAsync(String serverCategory, String serverId, ClusterServerType serverType) {
			await _connectionFactory.GetDatabase().SetAddAsync(_distributedCacheKeyConfiguration.RegisteredServersKey(serverCategory), serverId);
			await _connectionFactory.GetDatabase().HashSetAsync(_distributedCacheKeyConfiguration.ServerStatusKey(serverId), "type", (int)serverType);

			return true;
		}

		public async Task<bool> RegisterServiceAsync(String serverId, String serviceName) {
			return await _connectionFactory.GetDatabase().SetAddAsync(_distributedCacheKeyConfiguration.RegisteredServicesOnCacheKey, serviceName) && await _connectionFactory.GetDatabase().SetAddAsync(_distributedCacheKeyConfiguration.RegisteredServicesOnServerKey(serverId), serviceName);
		}

		public void RemoveMinerRecentSharesImmediate(String minerPublicKey, IEnumerable<String> nonces) {
			_connectionFactory.GetDatabase().SetRemove(_distributedCacheKeyConfiguration.MinerRecentSharesKey(minerPublicKey), nonces.Select(n => (RedisValue)n).ToArray(), CommandFlags.FireAndForget);
		}

		public Task<bool> ServiceIsRegisteredOnCacheAsync(String serviceName) {
			return _connectionFactory.GetDatabase().SetContainsAsync(_distributedCacheKeyConfiguration.RegisteredServicesOnCacheKey, serviceName);
		}

		public async Task<bool> UnregisterServerAsync(String serverCategory, String serverId) {
			await _connectionFactory.GetDatabase().SetRemoveAsync(_distributedCacheKeyConfiguration.RegisteredServersKey(serverCategory), serverId);
			await _connectionFactory.GetDatabase().KeyDeleteAsync(_distributedCacheKeyConfiguration.RegisteredServicesOnServerKey(serverId));
			await _connectionFactory.GetDatabase().KeyDeleteAsync(_distributedCacheKeyConfiguration.ServerStatusKey(serverId));

			return true;
		}

		public async Task<bool> UnregisterServiceAsync(String serverId, String serviceName) {
			await _connectionFactory.GetDatabase().SetRemoveAsync(_distributedCacheKeyConfiguration.RegisteredServicesOnCacheKey, serviceName);
			await _connectionFactory.GetDatabase().SetRemoveAsync(_distributedCacheKeyConfiguration.RegisteredServicesOnServerKey(serverId), serviceName);

			return true;
		}

		public void UpdateMinerAggregateStatsImmediate(String minerPublicKey, AggregateMinerStats aggregateMinerStats) {
			_connectionFactory.GetDatabase().HashSet(_distributedCacheKeyConfiguration.MinerAggregateStatsKey(minerPublicKey), new [] { new HashEntry("aggregate", aggregateMinerStats.ToByteArray()), new HashEntry("calculatedOn", new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds()) }, CommandFlags.FireAndForget);
		}

		public void UpdateMinerBalanceInfoImmediate(String minerPublicKey, decimal? minimumPayoutThreshold, decimal pendingBalance, decimal totalPaid) {
			_connectionFactory.GetDatabase().HashSet(_distributedCacheKeyConfiguration.MinerBalanceInfoKey(minerPublicKey), new [] { new HashEntry("minimumPayoutThreshold", minimumPayoutThreshold.HasValue ? minimumPayoutThreshold.Value.ToString() : String.Empty), new HashEntry("pendingBalance", pendingBalance.ToString()), new HashEntry("totalPaid", totalPaid.ToString()) }, CommandFlags.FireAndForget);
		}

		public void UpdateMinerPaymentsImmediate(String minerPublicKey, IEnumerable<(decimal Amount, long SubmittedOn, int? SubmittedOnBlockHeight, decimal TransactionFee, String TransactionId)> payments) {
			var paymentArray = payments as (decimal Amount, long SubmittedOn, int? SubmittedOnBlockHeight, decimal TransactionFee, String TransactionId)[] ?? payments.ToArray();

			_connectionFactory.GetDatabase().HashSet(_distributedCacheKeyConfiguration.MinerPaymentsKey(minerPublicKey), new [] { new HashEntry("payments", paymentArray.ToByteArray()), new HashEntry("calculatedOn", new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds()) }, flags: CommandFlags.FireAndForget);
		}

		public void UpdateNodeMiningDataImmediate(String blockHash, int blockDifficulty, int currentHeight, int fieldZero, ulong nodeClientId) {
			_connectionFactory.GetDatabase().HashSet(_distributedCacheKeyConfiguration.NodeMiningDataKey, new [] { new HashEntry("blockHash", blockHash), new HashEntry("blockDiff", blockDifficulty), new HashEntry("height", currentHeight), new HashEntry("f0", fieldZero), new HashEntry("nodeClient", nodeClientId) }, CommandFlags.FireAndForget);
		}

		public void UpdatePoolAggregateStatsImmediate(AggregatePoolStats aggregatePoolStats) {
			_connectionFactory.GetDatabase().HashSet(_distributedCacheKeyConfiguration.PoolAggregateStatsKey, new [] { new HashEntry("aggregate", aggregatePoolStats.ToByteArray()), new HashEntry("calculatedOn", new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds()) }, CommandFlags.FireAndForget);
		}

		public void UpdatePoolRecentBlocksImmediate(IEnumerable<BlockCandidate> blockCandidates) {
			var blockCandidatesArray = blockCandidates as BlockCandidate[] ?? blockCandidates.ToArray();

			_connectionFactory.GetDatabase().HashSet(_distributedCacheKeyConfiguration.PoolRecentBlocksKey, new [] { new HashEntry("blocks", blockCandidatesArray.ToByteArray()), new HashEntry("calculatedOn", new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds()) }, flags: CommandFlags.FireAndForget);
		}

		public async Task<long> UpdatePublicKeyWhitelistAsync(IEnumerable<String> minerPublicKeys) {
			var minerPublicKeysArray = (minerPublicKeys as String[] ?? minerPublicKeys.ToArray()).Select(m => (RedisValue)m).ToArray();

			if (minerPublicKeysArray.Length > 0) {
				await _connectionFactory.GetDatabase().KeyDeleteAsync(new [] { (RedisKey)_distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKeyTempEmpty, (RedisKey)_distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKeyTempNew });
				
				await _connectionFactory.GetDatabase().SetAddAsync(_distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKeyTempEmpty, new [] { minerPublicKeysArray.First() });
				await _connectionFactory.GetDatabase().SetAddAsync(_distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKeyTempNew, minerPublicKeysArray);

				return await _connectionFactory.GetDatabase().SetCombineAndStoreAsync(SetOperation.Union, _distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKey, _distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKeyTempEmpty, _distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKeyTempNew);
			} else {
				await _connectionFactory.GetDatabase().KeyDeleteAsync(_distributedCacheKeyConfiguration.PoolPublicKeyWhitelistKey);

				return 0L;
			}
		}

		public Task<bool> UpdateServerHeartbeatAsync(String serverId) {
			return _connectionFactory.GetDatabase().HashSetAsync(_distributedCacheKeyConfiguration.ServerStatusKey(serverId), "heartbeat", new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds());
		}

		public async Task<bool> UpdateServiceStatusAsRunningAsync(String serverId, String serviceName, long currentRunStartedOn) {
			await _connectionFactory.GetDatabase().HashSetAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), new [] { new HashEntry("status", (int)ServiceRuntimeStatusType.Running), new HashEntry("currentStartedOn", currentRunStartedOn) });
			await _connectionFactory.GetDatabase().HashDeleteAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), "nextRunOn");

			return true;
		}

		public async Task<bool> UpdateServiceStatusAsSleepingAsync(String serverId, String serviceName, long estimatedNextRunOn, long lastCompletedOn) {
			await _connectionFactory.GetDatabase().HashSetAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), new [] { new HashEntry("lastCompletedOn", lastCompletedOn), new HashEntry("nextRunOn", estimatedNextRunOn), new HashEntry("status", (int)ServiceRuntimeStatusType.Sleeping) });
			await _connectionFactory.GetDatabase().HashDeleteAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), new RedisValue[] { "currentStartedOn", "data", });

			return true;
		}

		public async Task<bool> UpdateServiceStatusAsStoppedAsync(String serverId, String serviceName) {
			await _connectionFactory.GetDatabase().HashSetAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), "status", (int)ServiceRuntimeStatusType.Stopped);
			await _connectionFactory.GetDatabase().HashDeleteAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), new RedisValue[] { "currentStartedOn", "data", "nextRunOn" });

			return true;
		}

		public Task<bool> UpdateServiceStatusDataAsync(String serverId, String serviceName, Object data) {
			return _connectionFactory.GetDatabase().HashSetAsync(_distributedCacheKeyConfiguration.ServiceStatusKey(serverId, serviceName), "data", data.ToByteArray());
		}

		public void UpdateWorkerCurrentJobDifficultyImmediate(String minerAddress, int difficulty) {
			_connectionFactory.GetDatabase().HashSet(_distributedCacheKeyConfiguration.WorkerStatsKey(minerAddress), "currentDiff", difficulty, flags: CommandFlags.FireAndForget);
		}
		
		public void UpdateWorkerHeartbeatImmediate(String minerAddress) {
			_connectionFactory.GetDatabase().HashSet(_distributedCacheKeyConfiguration.WorkerStatsKey(minerAddress), "heartbeat", new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds(), flags: CommandFlags.FireAndForget);

			_connectionFactory.GetDatabase().SetAdd(_distributedCacheKeyConfiguration.ActiveWorkersKey, minerAddress, CommandFlags.FireAndForget);
		}

		public async Task UpdateWorkerRecentSharesAsync(String minerAddress, String minerPublicKey, String nonce, long shareSubmittedOn) {
			_connectionFactory.GetDatabase().SetAdd(_distributedCacheKeyConfiguration.MinerRecentSharesKey(minerPublicKey), nonce, CommandFlags.FireAndForget);

			await UpdateWorkerRecentShareIntervalsAsync(minerAddress, shareSubmittedOn);
		}
		#endregion

		private async Task UpdateWorkerRecentShareIntervalsAsync(String minerAddress, long shareSubmittedOn) {
			var lastShareTimeTask = GetWorkerLastShareTimeAsync(minerAddress);

			_connectionFactory.GetDatabase().HashSet(_distributedCacheKeyConfiguration.WorkerStatsKey(minerAddress), "lastShareOn", shareSubmittedOn, flags: CommandFlags.FireAndForget);

			var lastShareTime = await lastShareTimeTask;
			if (lastShareTime.HasValue) {
				_connectionFactory.GetDatabase().ListRightPush(_distributedCacheKeyConfiguration.WorkerRecentShareIntervalsKey(minerAddress), Math.Abs(shareSubmittedOn - lastShareTime.Value), flags: CommandFlags.FireAndForget);
			}
		}

		private readonly IConnectionFactory _connectionFactory;
		private readonly IDistributedCacheKeyConfiguration _distributedCacheKeyConfiguration;
	}
}