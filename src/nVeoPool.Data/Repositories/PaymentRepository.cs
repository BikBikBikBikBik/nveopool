#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NpgsqlTypes;
using nVeoPool.Data.Context;

namespace nVeoPool.Data.Repositories {
	public class PaymentRepository : IPaymentRepository {
		public PaymentRepository(PostgresDatabaseContext context) {
			_context = context;
		}
		
		#region PaymentRepository
		public async Task<IEnumerable<(String MinerPublicKey, decimal? MinimumPayoutThreshold, decimal PendingBalance, decimal TotalPaid)>> GetAllMinerBalancesAsync() {
			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					command.CommandText = "SELECT \"MinerPublicKey\", \"MinimumPayoutThreshold\", \"PendingBalance\", \"TotalPaid\" FROM \"MinerBalance\"";

					await connection.OpenAsync();
					command.Prepare();

					var minerBalances = new List<(String MinerPublicKey, decimal? MinimumPayoutThreshold, decimal PendingBalance, decimal TotalPaid)>();
					using (var reader = await command.ExecuteReaderAsync()) {
						while (await reader.ReadAsync()) {
							minerBalances.Add(((String)reader["MinerPublicKey"], reader["MinimumPayoutThreshold"] as decimal?, (decimal)reader["PendingBalance"], (decimal)reader["TotalPaid"]));
						}
					}

					return minerBalances;
				}
			}
		}

		public async Task<IEnumerable<(decimal Amount, String MinerPublicKey, long SubmittedOn, int? SubmittedOnBlockHeight, decimal TransactionFee, String TransactionId)>> GetAllMinerPaymentsAsync(long cutoffTime) {
			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					command.CommandText = "SELECT \"Amount\", \"MinerPublicKey\", \"SubmittedOn\", \"SubmittedOnBlockHeight\", \"TransactionFee\", \"TransactionId\" FROM \"PaymentTransaction\" WHERE \"SubmittedOn\" >= @cutoff ORDER BY \"SubmittedOn\" DESC";

					command.Parameters.AddWithValue("cutoff", NpgsqlDbType.Bigint, cutoffTime);

					await connection.OpenAsync();
					command.Prepare();

					var minerPayments = new List<(decimal Amount, String MinerPublicKey, long SubmittedOn, int? SubmittedOnBlockHeight, decimal TransactionFee, String TransactionId)>();
					using (var reader = await command.ExecuteReaderAsync()) {
						while (await reader.ReadAsync()) {
							minerPayments.Add(((decimal)reader["Amount"], (String)reader["MinerPublicKey"], (long)reader["SubmittedOn"], reader["SubmittedOnBlockHeight"] as int?, (decimal)reader["TransactionFee"], reader["TransactionId"] as String));
						}
					}

					return minerPayments;
				}
			}
		}

		public async Task<IEnumerable<(String MinerPublicKey, decimal PendingBalance)>> GetMinersForPaymentAsync(decimal minimumBalance) {
			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					command.CommandText = "SELECT \"MinerPublicKey\", \"PendingBalance\" FROM \"MinerBalance\" WHERE ((\"MinimumPayoutThreshold\" IS NULL AND \"PendingBalance\" >= @mBalance) OR (\"MinimumPayoutThreshold\" >= 0 AND \"PendingBalance\" >= \"MinimumPayoutThreshold\" AND \"PendingBalance\" > 0)) ORDER BY \"PendingBalance\" DESC";

					command.Parameters.AddWithValue("mBalance", NpgsqlDbType.Numeric, minimumBalance);

					await connection.OpenAsync();
					command.Prepare();

					var minersForPayment = new List<(String MinerPublicKey, decimal PendingBalance)>();
					using (var reader = await command.ExecuteReaderAsync()) {
						while (await reader.ReadAsync()) {
							minersForPayment.Add(((String)reader["MinerPublicKey"], (decimal)reader["PendingBalance"]));
						}
					}

					return minersForPayment;
				}
			}
		}

		public async Task<int> SaveRewardsForBlockAsync(int blockHeight, IEnumerable<(decimal FeePercent, String MinerPublicKey, decimal Reward)> blockRewards) {
			var affectedRows = 0;

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var blockRewardsArray = blockRewards as (decimal FeePercent, String MinerPublicKey, decimal Reward)[] ?? blockRewards.ToArray();

					if (blockRewardsArray.Length > 0) {
						command.CommandText = "UPDATE \"BlockCandidate\" SET \"IsPaid\" = true WHERE \"BlockHeight\" = @bHeight";
						command.Parameters.AddWithValue("bHeight", NpgsqlDbType.Integer, blockHeight);

						for (var i = 0; i < blockRewardsArray.Length; i++) {
							command.CommandText = $"INSERT INTO \"BlockReward\" (\"BlockHeight\", \"FeePercent\", \"MinerPublicKey\", \"Reward\") VALUES (@bHeight, @fPercent{i}, @mPubKey{i}, @reward{i}); {command.CommandText}";
							command.CommandText = $"INSERT INTO \"MinerBalance\" (\"MinerPublicKey\", \"PendingBalance\", \"TotalPaid\") VALUES (@mPubKey{i}, @pBalance{i}, 0) ON CONFLICT (\"MinerPublicKey\") DO UPDATE SET \"PendingBalance\" = \"MinerBalance\".\"PendingBalance\" + @pBalance{i}; {command.CommandText}";
							
							command.Parameters.AddWithValue($"fPercent{i}", NpgsqlDbType.Numeric, blockRewardsArray[i].FeePercent);
							command.Parameters.AddWithValue($"mPubKey{i}", NpgsqlDbType.Text, blockRewardsArray[i].MinerPublicKey);
							command.Parameters.AddWithValue($"pBalance{i}", NpgsqlDbType.Numeric, blockRewardsArray[i].Reward);
							command.Parameters.AddWithValue($"reward{i}", NpgsqlDbType.Numeric, blockRewardsArray[i].Reward);
						}

						await connection.OpenAsync();
						command.Prepare();

						using (var transaction = connection.BeginTransaction()) {
							command.Transaction = transaction;

							affectedRows = await command.ExecuteNonQueryAsync();

							transaction.Commit();
						}

						return affectedRows;
					}
				}
			}

			return affectedRows;
		}

		public async Task<int> SaveTransactionsAndUpdateMinerBalancesAsync(IEnumerable<(String MinerPublicKey, decimal Amount, long SubmittedOn, int SubmittedOnBlockHeight, decimal TransactionFee, String TransactionId)> minerPaymentTransactions) {
			var affectedRows = 0;

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var minerPaymentTransactionsArray = minerPaymentTransactions as (String MinerPublicKey, decimal Amount, long SubmittedOn, int SubmittedOnBlockHeight, decimal TransactionFee, String TransactionId)[] ?? minerPaymentTransactions.ToArray();

					if (minerPaymentTransactionsArray.Length > 0) {
						for (var i = 0; i < minerPaymentTransactionsArray.Length; i++) {
							command.CommandText = $"UPDATE \"MinerBalance\" SET \"PendingBalance\" = \"PendingBalance\" - @amount{i}, \"TotalPaid\" = \"TotalPaid\" + @amount{i} WHERE \"MinerPublicKey\" = @mPubKey{i};";
							command.CommandText = $"INSERT INTO \"PaymentTransaction\" (\"Amount\", \"MinerPublicKey\", \"SubmittedOn\", \"SubmittedOnBlockHeight\", \"TransactionFee\", \"TransactionId\") VALUES (@amount{i}, @mPubKey{i}, @submittedOn{i}, @submittedOnBlk{i}, @txFee{i}, @txId{i}); {command.CommandText}";

							command.Parameters.AddWithValue($"amount{i}", NpgsqlDbType.Numeric, minerPaymentTransactionsArray[i].Amount);
							command.Parameters.AddWithValue($"mPubKey{i}", NpgsqlDbType.Text, minerPaymentTransactionsArray[i].MinerPublicKey);
							command.Parameters.AddWithValue($"submittedOn{i}", NpgsqlDbType.Bigint, minerPaymentTransactionsArray[i].SubmittedOn);
							command.Parameters.AddWithValue($"submittedOnBlk{i}", NpgsqlDbType.Integer, minerPaymentTransactionsArray[i].SubmittedOnBlockHeight);
							command.Parameters.AddWithValue($"txFee{i}", NpgsqlDbType.Numeric, minerPaymentTransactionsArray[i].TransactionFee);
							command.Parameters.AddWithValue($"txId{i}", NpgsqlDbType.Text, minerPaymentTransactionsArray[i].TransactionId);
						}

						await connection.OpenAsync();
						command.Prepare();

						using (var transaction = connection.BeginTransaction()) {
							command.Transaction = transaction;

							affectedRows = await command.ExecuteNonQueryAsync();

							transaction.Commit();
						}
					}
				}
			}

			return affectedRows;
		}
		#endregion

		private readonly PostgresDatabaseContext _context;
	}
}