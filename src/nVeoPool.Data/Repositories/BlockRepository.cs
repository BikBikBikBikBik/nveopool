#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NpgsqlTypes;
using nVeoPool.Data.Context;
using nVeoPool.Data.Models;

namespace nVeoPool.Data.Repositories {
	public class BlockRepository : IBlockRepository {
		public BlockRepository(PostgresDatabaseContext context) {
			_context = context;
		}
		
		#region IBlockRepository
		public async Task<IEnumerable<int>> GetAllBlockCandidatesAfterHeightAsync(int minimumBlockHeight) {
			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					command.CommandText = "SELECT \"BlockHeight\" FROM \"BlockCandidate\" WHERE \"BlockHeight\" >= @minBlkHeight";

					command.Parameters.AddWithValue("minBlkHeight", NpgsqlDbType.Integer, minimumBlockHeight);

					await connection.OpenAsync();
					command.Prepare();

					var blockCandidateHeights = new List<int>();
					using (var reader = await command.ExecuteReaderAsync()) {
						while (await reader.ReadAsync()) {
							blockCandidateHeights.Add((int)reader["BlockHeight"]);
						}
					}

					return blockCandidateHeights;
				}
			}
		}

		public async Task<IEnumerable<BlockCandidate>> GetAllBlockCandidatesAsync(int limit) {
			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					command.CommandText = "SELECT \"BlockHeight\", \"IsPaid\", \"MinerPublicKey\", \"StatusId\", \"SubmittedOn\" FROM \"BlockCandidate\" ORDER BY \"BlockHeight\" DESC LIMIT @limit";

					command.Parameters.AddWithValue("limit", NpgsqlDbType.Integer, limit);

					await connection.OpenAsync();
					command.Prepare();

					var blockCandidates = new List<BlockCandidate>();
					using (var reader = await command.ExecuteReaderAsync()) {
						while (await reader.ReadAsync()) {
							blockCandidates.Add(new BlockCandidate {
								BlockHeight = (int)reader["BlockHeight"],
								IsPaid = (bool)reader["IsPaid"],
								MinerPublicKey = (String)reader["MinerPublicKey"],
								StatusId = (short)reader["StatusId"],
								SubmittedOn = (long)reader["SubmittedOn"]
							});
						}
					}

					return blockCandidates;
				}
			}
		}

		public async Task<IEnumerable<int>> GetBlockCandidatesForProcessingAsync(int maximumBlockHeight) {
			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					command.CommandText = "SELECT \"BlockHeight\" FROM \"BlockCandidate\" WHERE \"BlockHeight\" <= @maxBlkHeight AND (\"StatusId\" = @statusAccepted OR \"StatusId\" = @statusRejected)";

					command.Parameters.AddWithValue("maxBlkHeight", NpgsqlDbType.Integer, maximumBlockHeight);
					command.Parameters.AddWithValue("statusAccepted", NpgsqlDbType.Integer, BlockCandidateStatusType.UnconfirmedAccepted);
					command.Parameters.AddWithValue("statusRejected", NpgsqlDbType.Integer, BlockCandidateStatusType.UnconfirmedRejected);

					await connection.OpenAsync();
					command.Prepare();

					var blockCandidateHeights = new List<int>();
					using (var reader = await command.ExecuteReaderAsync()) {
						while (await reader.ReadAsync()) {
							blockCandidateHeights.Add((int)reader["BlockHeight"]);
						}
					}

					return blockCandidateHeights;
				}
			}
		}

		public async Task<IEnumerable<int>> GetConfirmedBlocksForPaymentAsync() {
			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					command.CommandText = "SELECT \"BlockHeight\" FROM \"BlockCandidate\" WHERE \"StatusId\" = @statusConfirmed AND \"IsPaid\" = false";

					command.Parameters.AddWithValue("statusConfirmed", NpgsqlDbType.Integer, BlockCandidateStatusType.Confirmed);

					await connection.OpenAsync();
					command.Prepare();

					var confirmedBlockHeights = new List<int>();
					using (var reader = await command.ExecuteReaderAsync()) {
						while (await reader.ReadAsync()) {
							confirmedBlockHeights.Add((int)reader["BlockHeight"]);
						}
					}

					return confirmedBlockHeights;
				}
			}
		}

		public async Task<int> GetLastScannedBlockAsync() {
			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					command.CommandText = "SELECT \"LastBlockHeight\" FROM \"UnrecordedBlockScan\" ORDER BY \"LastBlockHeight\" DESC LIMIT 1";

					await connection.OpenAsync();
					command.Prepare();

					var lastScannedBlock = 0;
					using (var reader = await command.ExecuteReaderAsync()) {
						while (await reader.ReadAsync()) {
							lastScannedBlock = (int)reader["LastBlockHeight"];
						}
					}

					return lastScannedBlock;
				}
			}
		}

		public async Task<int> SaveUnrecordedBlockCandidateAsync(BlockCandidate blockCandidate) {
			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					command.CommandText = "INSERT INTO \"BlockCandidate\" (\"BlockHeight\", \"IsPaid\", \"MinerPublicKey\", \"Nonce\", \"StatusId\", \"SubmittedOn\") VALUES (@bCandHeight, false, @mPubKey, @nonce, @status, @submittedOn)";

					command.Parameters.AddWithValue("bCandHeight", NpgsqlDbType.Integer, blockCandidate.BlockHeight);
					command.Parameters.AddWithValue("mPubKey", NpgsqlDbType.Text, blockCandidate.MinerPublicKey);
					command.Parameters.AddWithValue("nonce", NpgsqlDbType.Text, blockCandidate.Nonce);
					command.Parameters.AddWithValue("status", NpgsqlDbType.Integer, BlockCandidateStatusType.UnconfirmedAccepted);
					command.Parameters.AddWithValue("submittedOn", NpgsqlDbType.Bigint, blockCandidate.SubmittedOn);

					await connection.OpenAsync();
					command.Prepare();
					
					return await command.ExecuteNonQueryAsync();
				}
			}
		}

		public async Task<int> UpdateBlockCandidateStatusesAsync(IEnumerable<int> confirmedBlockHeights, IEnumerable<int> orphanedBlockHeights) {
			var affectedRows = 0;

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var confirmedBlockHeightsArray = confirmedBlockHeights as int[] ?? confirmedBlockHeights.ToArray();
					var orphanedBlockHeightsArray = orphanedBlockHeights as int[] ?? orphanedBlockHeights.ToArray();

					if (confirmedBlockHeightsArray.Length > 0) {
						command.CommandText = "UPDATE \"BlockCandidate\" SET \"StatusId\" = @statusConfirmed WHERE \"BlockHeight\" = ANY(@confBlkHeightArr);";

						command.Parameters.AddWithValue("confBlkHeightArr", NpgsqlDbType.Array | NpgsqlDbType.Integer, confirmedBlockHeightsArray);
						command.Parameters.AddWithValue("statusConfirmed", NpgsqlDbType.Integer, BlockCandidateStatusType.Confirmed);
					}
					if (orphanedBlockHeightsArray.Length > 0) {
						command.CommandText = $"UPDATE \"BlockCandidate\" SET \"StatusId\" = @statusOrphaned WHERE \"BlockHeight\" = ANY(@orphBlkHeightArr); {command.CommandText}";

						command.Parameters.AddWithValue("orphBlkHeightArr", NpgsqlDbType.Array | NpgsqlDbType.Integer, orphanedBlockHeightsArray);
						command.Parameters.AddWithValue("statusOrphaned", NpgsqlDbType.Integer, BlockCandidateStatusType.Orphaned);
					}

					if (command.CommandText.Length > 0) {
						await connection.OpenAsync();
						command.Prepare();

						using (var transaction = connection.BeginTransaction()) {
							command.Transaction = transaction;

							affectedRows = await command.ExecuteNonQueryAsync();

							transaction.Commit();
						}
					}
				}
			}

			return affectedRows;
		}

		public async Task<int> UpdateLastScannedBlockAsync(int blockHeight, bool firstRun = false) {
			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					if (firstRun) {
						command.CommandText = "INSERT INTO \"UnrecordedBlockScan\" (\"LastBlockHeight\") VALUES (@lastBlkHeight)";
					} else {
						command.CommandText = "UPDATE \"UnrecordedBlockScan\" SET \"LastBlockHeight\" = @lastBlkHeight";
					}

					command.Parameters.AddWithValue("lastBlkHeight", NpgsqlDbType.Integer, blockHeight);

					await connection.OpenAsync();
					command.Prepare();
					
					return await command.ExecuteNonQueryAsync();
				}
			}
		}
		#endregion

		private readonly PostgresDatabaseContext _context;
	}
}