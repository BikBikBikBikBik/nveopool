#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NpgsqlTypes;
using nVeoPool.Data.Context;
using nVeoPool.Data.Models;

namespace nVeoPool.Data.Repositories {
	public class ShareRepository : IShareRepository {
		public ShareRepository(PostgresDatabaseContext context) {
			_context = context;
		}
		
		#region IShareRepository
		public async Task<int> ArchiveSharesBeforeCutoffAsync(long cutoffTime) {
			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					command.CommandText = "WITH \"SharesForArchive\" AS (DELETE FROM \"AcceptedShare\" AS a WHERE \"SubmittedOn\" <= @cutoff RETURNING a.*) INSERT INTO \"AcceptedShareArchive\" SELECT * FROM \"SharesForArchive\";";

					command.Parameters.AddWithValue("cutoff", NpgsqlDbType.Bigint, cutoffTime);

					await connection.OpenAsync();
					command.Prepare();

					var affectedRows = 0;
					using (var transaction = connection.BeginTransaction()) {
						command.Transaction = transaction;

						affectedRows = await command.ExecuteNonQueryAsync();

						transaction.Commit();
					}

					return affectedRows;
				}
			}
		}

		public async Task<IEnumerable<(String MinerPublicKey, String Nonce)>> GetNoncesInRangeAsync(long startShareTime, long endShareTime) {
			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					command.CommandText = "SELECT \"MinerPublicKey\", \"Nonce\" FROM \"AcceptedShare\" WHERE \"SubmittedOn\" >= @startShareTime AND \"SubmittedOn\" <= @endShareTime";

					command.Parameters.AddWithValue("endShareTime", NpgsqlDbType.Bigint, endShareTime);
					command.Parameters.AddWithValue("startShareTime", NpgsqlDbType.Bigint, startShareTime);

					await connection.OpenAsync();
					command.Prepare();

					var noncesBeforeCutoff = new List<(String MinerPublicKey, String Nonce)>();
					using (var reader = await command.ExecuteReaderAsync()) {
						while (await reader.ReadAsync()) {
							noncesBeforeCutoff.Add(((String)reader["MinerPublicKey"], (String)reader["Nonce"]));
						}
					}

					return noncesBeforeCutoff;
				}
			}
		}

		public async Task<IDictionary<int, IEnumerable<Share>>> GetSharesForBlocksAsync(IEnumerable<(int ForBlockHeight, int StartBlockHeight, int EndBlockHeight)> blockHeightRanges) {
			var blockSharesMap = new Dictionary<int, IEnumerable<Share>>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var blockHeightRangesArray = blockHeightRanges as (int ForBlockHeight, int StartBlockHeight, int EndBlockHeight)[] ?? blockHeightRanges.ToArray();

					if (blockHeightRangesArray.Length > 0) {
						blockSharesMap = blockHeightRangesArray.Distinct().ToDictionary(b => b.ForBlockHeight, b => (IEnumerable<Share>)new List<Share>());
						
						for (var i = 0; i < blockHeightRangesArray.Length; i++) {
							command.CommandText = $"SELECT \"BlockHeight\", \"HashCount\", \"IsBlockCandidate\", \"JobDifficulty\", \"MinerPublicKey\", \"SubmittedOn\", {blockHeightRangesArray[i].ForBlockHeight} AS \"ForBlockHeight\" FROM \"AcceptedShare\" WHERE \"BlockHeight\" >= @startHeight{i} AND \"BlockHeight\" <= @endHeight{i} ORDER BY \"SubmittedOn\" DESC; {command.CommandText}";

							command.Parameters.AddWithValue($"endHeight{i}", NpgsqlDbType.Integer, blockHeightRangesArray[i].EndBlockHeight);
							command.Parameters.AddWithValue($"startHeight{i}", NpgsqlDbType.Integer, blockHeightRangesArray[i].StartBlockHeight);
						}

						await connection.OpenAsync();
						command.Prepare();

						using (var reader = await command.ExecuteReaderAsync()) {
							while (true) {
								var forBlockHeight = 0;
								var shares = new List<Share>();

								while (await reader.ReadAsync()) {
									forBlockHeight = (int)reader["ForBlockHeight"];

									shares.Add(new Share {
										BlockHeight = (int)reader["BlockHeight"],
										HashCount = (double)reader["HashCount"],
										IsBlockCandidate = (bool)reader["IsBlockCandidate"],
										JobDifficulty = (int)reader["JobDifficulty"],
										MinerPublicKey = (String)reader["MinerPublicKey"],
										SubmittedOn = (long)reader["SubmittedOn"]
									});
								}

								blockSharesMap[forBlockHeight] = shares;

								if (await reader.NextResultAsync()) {
									continue;
								}

								break;
							}
						}
					}
				}
			}

			return blockSharesMap;
		}

		public async Task<int> RemoveArchivedSharesBeforeCutoffAsync(long cutoffTime) {
			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					command.CommandText = "DELETE FROM \"AcceptedShareArchive\" WHERE \"SubmittedOn\" <= @cutoff;";

					command.Parameters.AddWithValue("cutoff", NpgsqlDbType.Bigint, cutoffTime);

					await connection.OpenAsync();
					command.Prepare();

					var affectedRows = 0;
					using (var transaction = connection.BeginTransaction()) {
						command.Transaction = transaction;

						affectedRows = await command.ExecuteNonQueryAsync();

						transaction.Commit();
					}

					return affectedRows;
				}
			}
		}

		public async Task<int> RemoveInvalidSharesBeforeCutoffAsync(long cutoffTime) {
			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					command.CommandText = "DELETE FROM \"InvalidShare\" WHERE \"SubmittedOn\" <= @cutoff;";

					command.Parameters.AddWithValue("cutoff", NpgsqlDbType.Bigint, cutoffTime);

					await connection.OpenAsync();
					command.Prepare();

					var affectedRows = 0;
					using (var transaction = connection.BeginTransaction()) {
						command.Transaction = transaction;

						affectedRows = await command.ExecuteNonQueryAsync();

						transaction.Commit();
					}

					return affectedRows;
				}
			}
		}

		public async Task<int> SaveInvalidSharesAsync(IEnumerable<Share> shares) {
			var affectedRows = 0;

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var shareArray = shares as Share[] ?? shares.ToArray();

					if (shareArray.Length > 0) {
						for (var i = 0; i < shareArray.Length; i++) {
							command.CommandText = $"INSERT INTO \"InvalidShare\" (\"IsDuplicate\", \"MinerPublicKey\", \"MinerWorkerId\", \"SubmittedOn\") VALUES (@isDup{i}, @mPubKey{i}, @mWorkerId{i}, @shareTime{i}); {command.CommandText}";

							command.Parameters.AddWithValue($"isDup{i}", NpgsqlDbType.Boolean, shareArray[i].IsDuplicate);
							command.Parameters.AddWithValue($"mPubKey{i}", NpgsqlDbType.Text, shareArray[i].MinerPublicKey);
							command.Parameters.AddWithValue($"mWorkerId{i}", NpgsqlDbType.Text, shareArray[i].MinerWorkerId);
							command.Parameters.AddWithValue($"shareTime{i}", NpgsqlDbType.Bigint, shareArray[i].SubmittedOn);
						}

						await connection.OpenAsync();
						command.Prepare();

						using (var transaction = connection.BeginTransaction()) {
							command.Transaction = transaction;

							affectedRows = await command.ExecuteNonQueryAsync();

							transaction.Commit();
						}
					}
				}
			}

			return affectedRows;
		}

		public async Task<int> SaveSharesAsync(IEnumerable<Share> shares, bool saveBlockCandidates) {
			var affectedRows = 0;

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					var shareArray = shares as Share[] ?? shares.ToArray();

					if (shareArray.Length > 0) {
						for (var i = 0; i < shareArray.Length; i++) {
							command.CommandText = $"INSERT INTO \"AcceptedShare\" (\"BlockDifficulty\", \"BlockHash\", \"BlockHeight\", \"HashCount\", \"IsBlockCandidate\", \"JobDifficulty\", \"MinerPublicKey\", \"MinerWorkerId\", \"Nonce\", \"SubmittedOn\") VALUES (@bDiff{i}, @bHash{i}, @bHeight{i}, @hCount{i}, @isBlkCand{i}, @jDiff{i}, @mPubKey{i}, @mWorkerId{i}, @nonce{i}, @shareTime{i}) ON CONFLICT DO NOTHING; {command.CommandText}";

							command.Parameters.AddWithValue($"bDiff{i}", NpgsqlDbType.Integer, shareArray[i].BlockDifficulty);
							command.Parameters.AddWithValue($"bHash{i}", NpgsqlDbType.Text, shareArray[i].BlockHash);
							command.Parameters.AddWithValue($"bHeight{i}", NpgsqlDbType.Integer, shareArray[i].BlockHeight);
							command.Parameters.AddWithValue($"hCount{i}", NpgsqlDbType.Double, shareArray[i].HashCount);
							command.Parameters.AddWithValue($"isBlkCand{i}", NpgsqlDbType.Boolean, shareArray[i].IsBlockCandidate);
							command.Parameters.AddWithValue($"jDiff{i}", NpgsqlDbType.Integer, shareArray[i].JobDifficulty);
							command.Parameters.AddWithValue($"mPubKey{i}", NpgsqlDbType.Text, shareArray[i].MinerPublicKey);
							command.Parameters.AddWithValue($"mWorkerId{i}", NpgsqlDbType.Text, shareArray[i].MinerWorkerId);
							command.Parameters.AddWithValue($"nonce{i}", NpgsqlDbType.Text, shareArray[i].Nonce);
							command.Parameters.AddWithValue($"shareTime{i}", NpgsqlDbType.Bigint, shareArray[i].SubmittedOn);

							if (saveBlockCandidates && shareArray[i].IsBlockCandidate) {
								command.CommandText = $"INSERT INTO \"BlockCandidate\" (\"BlockHeight\", \"IsPaid\", \"MinerPublicKey\", \"Nonce\", \"StatusId\", \"SubmittedOn\") VALUES (@bCandHeight{i}, false, @mPubKey{i}, @nonce{i}, @status{i}, @shareTime{i}) ON CONFLICT DO NOTHING; UPDATE \"BlockCandidate\" SET \"StatusId\" = @statusOrphaned WHERE \"BlockHeight\" = @bCandHeight{i} AND @status{i} = @statusAccepted AND \"SubmittedOn\" < @shareTime{i}; {command.CommandText}";

								command.Parameters.AddWithValue($"bCandHeight{i}", NpgsqlDbType.Integer, shareArray[i].BlockHeight + 1);
								command.Parameters.AddWithValue($"status{i}", NpgsqlDbType.Integer, shareArray[i].IsRejectedBlockCandidate ? BlockCandidateStatusType.UnconfirmedRejected : BlockCandidateStatusType.UnconfirmedAccepted);
							}
						}

						command.Parameters.AddWithValue("statusAccepted", NpgsqlDbType.Integer, BlockCandidateStatusType.UnconfirmedAccepted);
						command.Parameters.AddWithValue("statusOrphaned", NpgsqlDbType.Integer, BlockCandidateStatusType.Orphaned);

						await connection.OpenAsync();
						command.Prepare();

						using (var transaction = connection.BeginTransaction()) {
							command.Transaction = transaction;

							affectedRows = await command.ExecuteNonQueryAsync();

							transaction.Commit();
						}
					}
				}
			}

			return affectedRows;
		}
		#endregion

		private readonly PostgresDatabaseContext _context;
	}
}