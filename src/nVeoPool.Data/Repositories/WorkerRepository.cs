#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using nVeoPool.Data.Context;
using nVeoPool.Data.Models;

namespace nVeoPool.Data.Repositories {
	public class WorkerRepository : IWorkerRepository {
		public WorkerRepository(PostgresDatabaseContext context) {
			_context = context;
		}

		#region IWorkerRepository
		public async Task<IEnumerable<AggregateMinerStats>> CalculateMinerStatsAsync() {
			var workerStats = new List<(String PublicKey, String WorkerId, double HashrateGhs, decimal AverageJobDifficulty, long AcceptedShareCount, long LastShareAcceptedOn, long InvalidShareCount, int WindowLengthSeconds)>();

			using (var connection = _context.CreateConnection()) {
				using (var command = connection.CreateCommand()) {
					command.CommandText = $"{CreateSelectForHashrateWindow(ONE_HOUR_SECONDS)}; {CreateSelectForHashrateWindow(SIX_HOURS_SECONDS)}; {CreateSelectForHashrateWindow(TWENTY_FOUR_HOURS_SECONDS)}";

					await connection.OpenAsync();
					command.Prepare();

					using (var reader = await command.ExecuteReaderAsync()) {
						while (true) {
							while (await reader.ReadAsync()) {
								workerStats.Add(((String)reader["MinerPublicKey"], (String)reader["MinerWorkerId"], (double)reader["GHs"], (decimal)reader["AverageJobDifficulty"], (long)reader["AcceptedShareCount"], (long)reader["LastShareAcceptedOn"], (long)reader["InvalidShareCount"], (int)reader["WindowLengthSeconds"]));
							}

							if (await reader.NextResultAsync()) {
								continue;
							}

							break;
						}
					}
				}
			}

			return workerStats.GroupBy(s => s.PublicKey, s => (s.WorkerId, s.HashrateGhs, s.AverageJobDifficulty, s.AcceptedShareCount, s.LastShareAcceptedOn, s.InvalidShareCount, s.WindowLengthSeconds), (k, v) => {
				var individualWorkers = v.ToArray();
				var oneHourAggregateStats = individualWorkers.Where(s => s.WindowLengthSeconds == ONE_HOUR_SECONDS).ToArray();
				var sixHourAggregateStats = individualWorkers.Where(s => s.WindowLengthSeconds == SIX_HOURS_SECONDS).ToArray();
				var twentyFourHourAggregateStats = individualWorkers.Where(s => s.WindowLengthSeconds == TWENTY_FOUR_HOURS_SECONDS).ToArray();

				return new AggregateMinerStats {
					AcceptedShareCount1Hour = oneHourAggregateStats.Sum(s => s.AcceptedShareCount),
					AcceptedShareCount6Hours = sixHourAggregateStats.Sum(s => s.AcceptedShareCount),
					AcceptedShareCount24Hours = twentyFourHourAggregateStats.Sum(s => s.AcceptedShareCount),
					AverageJobDifficulty1Hour = oneHourAggregateStats.Any() ? (int)oneHourAggregateStats.Average(s => s.AverageJobDifficulty) : 0,
					AverageJobDifficulty6Hours = sixHourAggregateStats.Any() ? (int)sixHourAggregateStats.Average(s => s.AverageJobDifficulty) : 0,
					AverageJobDifficulty24Hours = twentyFourHourAggregateStats.Any() ? (int)twentyFourHourAggregateStats.Average(s => s.AverageJobDifficulty) : 0,
					Hashrate1Hour = oneHourAggregateStats.Sum(s => s.HashrateGhs),
					Hashrate6Hours = sixHourAggregateStats.Sum(s => s.HashrateGhs),
					Hashrate24Hours = twentyFourHourAggregateStats.Sum(s => s.HashrateGhs),
					IndividualWorkerStats = individualWorkers.GroupBy(w => w.WorkerId, w => (w.HashrateGhs, w.AverageJobDifficulty, w.AcceptedShareCount, w.LastShareAcceptedOn, w.InvalidShareCount, w.WindowLengthSeconds), (kw, vw) => {
						var innerIndividualWorkers = vw.ToArray();
						var oneHourIndividualStats = innerIndividualWorkers.SingleOrDefault(s => s.WindowLengthSeconds == ONE_HOUR_SECONDS);
						var sixHourIndividualStats = innerIndividualWorkers.SingleOrDefault(s => s.WindowLengthSeconds == SIX_HOURS_SECONDS);
						var twentyFourHourIndividualStats = innerIndividualWorkers.SingleOrDefault(s => s.WindowLengthSeconds == TWENTY_FOUR_HOURS_SECONDS);

						return new IndividualWorkerStats {
							AcceptedShareCount1Hour = oneHourIndividualStats.AcceptedShareCount,
							AcceptedShareCount6Hours = sixHourIndividualStats.AcceptedShareCount,
							AcceptedShareCount24Hours = twentyFourHourIndividualStats.AcceptedShareCount,
							AverageJobDifficulty1Hour = (int)oneHourIndividualStats.AverageJobDifficulty,
							AverageJobDifficulty6Hours = (int)sixHourIndividualStats.AverageJobDifficulty,
							AverageJobDifficulty24Hours = (int)twentyFourHourIndividualStats.AverageJobDifficulty,
							Hashrate1Hour = oneHourIndividualStats.HashrateGhs,
							Hashrate6Hours = sixHourIndividualStats.HashrateGhs,
							Hashrate24Hours = twentyFourHourIndividualStats.HashrateGhs,
							InvalidShareCount1Hour = oneHourIndividualStats.InvalidShareCount,
							InvalidShareCount6Hours = sixHourIndividualStats.InvalidShareCount,
							InvalidShareCount24Hours = twentyFourHourIndividualStats.InvalidShareCount,
							LastShareAcceptedOn = innerIndividualWorkers.Max(s => s.LastShareAcceptedOn),
							WorkerId = kw
						};
					}).ToList(),
					InvalidShareCount1Hour = oneHourAggregateStats.Sum(s => s.InvalidShareCount),
					InvalidShareCount6Hours = sixHourAggregateStats.Sum(s => s.InvalidShareCount),
					InvalidShareCount24Hours = twentyFourHourAggregateStats.Sum(s => s.InvalidShareCount),
					LastShareAcceptedOn = individualWorkers.Max(s => s.LastShareAcceptedOn),
					PublicKey = k
				};
			}).ToList();
		}
		#endregion

		private String CreateSelectForHashrateWindow(uint windowLengthInSeconds) {
			return $"SELECT a.\"MinerPublicKey\", a.\"MinerWorkerId\", SUM(\"HashCount\")/{windowLengthInSeconds}/1000000000 AS \"GHs\", AVG(\"JobDifficulty\") AS \"AverageJobDifficulty\", COUNT(*) AS \"AcceptedShareCount\", MAX(\"SubmittedOn\") AS \"LastShareAcceptedOn\","
					+$" (SELECT COUNT(*) FROM \"InvalidShare\" AS i WHERE i.\"MinerPublicKey\" = a.\"MinerPublicKey\" AND i.\"MinerWorkerId\" = a.\"MinerWorkerId\" AND i.\"SubmittedOn\" > (EXTRACT(epoch FROM now()) - {windowLengthInSeconds})) AS \"InvalidShareCount\", {windowLengthInSeconds} AS \"WindowLengthSeconds\""
						+" FROM ("
							+$" SELECT * FROM \"AcceptedShareArchive\" WHERE \"SubmittedOn\" > (EXTRACT(epoch FROM now()) - {windowLengthInSeconds})"
								+" UNION"
							+$" SELECT * FROM \"AcceptedShare\" WHERE \"SubmittedOn\" > (EXTRACT(epoch FROM now()) - {windowLengthInSeconds})"
						+") AS a"
					+" GROUP BY a.\"MinerPublicKey\", a.\"MinerWorkerId\";";
		}

		private const int ONE_HOUR_SECONDS = 1 * 60 * 60;
		private const int SIX_HOURS_SECONDS = 6 * ONE_HOUR_SECONDS;
		private const int TWENTY_FOUR_HOURS_SECONDS = 4 * SIX_HOURS_SECONDS;
		private readonly PostgresDatabaseContext _context;
	}
}