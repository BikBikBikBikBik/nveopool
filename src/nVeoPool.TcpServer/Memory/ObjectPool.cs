#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;

namespace nVeoPool.TcpServer.Memory {
	public class ObjectPool<T> : IObjectPool<T> {
		public const int DefaultInitialSize = 1000;

		public ObjectPool(Func<T> factory) : this(factory, DefaultInitialSize) {
		}

		public ObjectPool(Func<T> factory, int initialPoolSize) {
			_factory = factory;
			_initialPoolSize = initialPoolSize;
			_pool = new Stack<T>();

			Initialize();
		}

		protected virtual void Initialize() {
			for (var i = 0; i < _initialPoolSize; i++) {
				_pool.Push(_factory());
			}
		}

		#region IObjectPool
		public T Acquire() {
			T obj = default(T);

			if (_pool.Count > 0) {
				obj = _pool.Pop();
			} else {
				obj = _factory();
			}

			return obj;
		}

		public void Release(T obj) {
			_pool.Push(obj);
		}
		#endregion

		public void Dispose() {
			while (_pool.Count > 0) {
				if (_pool.Pop() is IDisposable disposableObj) {
					disposableObj.Dispose();
				}
			}
		}

		private readonly Func<T> _factory;
		private readonly int _initialPoolSize;
		private readonly Stack<T> _pool;
	}
}