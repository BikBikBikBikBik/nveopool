#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;

namespace nVeoPool.PoolServer.Services.RuntimeData {
	[Serializable]
	internal class PaymentSenderData {
		public int BatchCount { get; set; }

		public int CurrentBatch { get; set; }

		public int ErrorCount { get; set; }

		public int PaymentCount { get; set; }

		public int PendingRetryCount { get; set; }

		public int ProcessedCount { get; set; }

		public int RemainingInBatch { get; set; }

		public int RemainingTotal { get; set; }

		public int SucessfullCount { get; set; }
	}
}