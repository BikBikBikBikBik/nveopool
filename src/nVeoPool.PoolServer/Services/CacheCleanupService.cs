#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Repositories;
using nVeoPool.Data.Services;
using nVeoPool.PoolServer.Configuration;

namespace nVeoPool.PoolServer.Services {
	internal class CacheCleanupService : ReportingBackgroundServiceBase {
		public CacheCleanupService(IAddressParser addressParser, IDistributedCacheAdapter cache, ILogger<CacheCleanupService> logger, IPoolServerConfiguration poolServerConfiguration, IShareRepository shareRepository) : base(cache, poolServerConfiguration.ClusteringConfiguration, logger, poolServerConfiguration.ServicesConfiguration.CacheCleanupServiceUpdateIntervalSeconds, new [] { ClusterServerType.Master, ClusterServerType.Slave }, nameof(CacheCleanupService)) {
			_addressParser = addressParser;
			_poolServerConfiguration = poolServerConfiguration;
			_shareRepository = shareRepository;
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			cancellationToken.Register(async () => await ReportServiceAsStoppingAsync());

			while (!cancellationToken.IsCancellationRequested) {
				try {
					await ReportServiceAsExecutingAsync();

					var activeWorkers = await _cache.GetActiveWorkersAsync();
					foreach (var worker in activeWorkers) {
						var cutoffWorkerTime = new DateTimeOffset(DateTime.UtcNow - TimeSpan.FromSeconds(_runInterval)).ToUnixTimeSeconds();
						var lastSeenTime = await _cache.GetWorkerLastSeenTimeAsync(worker);

						if (!lastSeenTime.HasValue || lastSeenTime <= cutoffWorkerTime) {
							await _cache.ClearWorkerInfoAsync(worker);
						} else {
							await _cache.ClearWorkerRecentShareIntervalsAsync(worker, _poolServerConfiguration.VarDiffConfiguration.IntervalSampleCount);
						}
					}

					var endShareTime = new DateTimeOffset(DateTime.UtcNow - TimeSpan.FromHours(MIN_CACHE_AGE_FOR_SHARE_REMOVAL_HOURS)).ToUnixTimeSeconds();
					var startShareTime = new DateTimeOffset(DateTime.UtcNow - TimeSpan.FromHours(MIN_CACHE_AGE_FOR_SHARE_REMOVAL_HOURS) - TimeSpan.FromHours(CACHE_AGE_RANGE_FOR_SHARE_REMOVAL_HOURS)).ToUnixTimeSeconds();
					var noncesForRemoval = (await _shareRepository.GetNoncesInRangeAsync(startShareTime, endShareTime)).GroupBy(s => s.MinerPublicKey, s => s.Nonce);

					foreach (var nonceGroup in noncesForRemoval) {
						_cache.RemoveMinerRecentSharesImmediate(_addressParser.FormatMinerAddressForCacheKey(nonceGroup.Key), nonceGroup);
					}
				} catch (Exception e) {
					ReportUnhandledServiceException(e);
				}

				await ReportServiceAsSleepingAsync();
				await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
			}
		}
		#endregion

		private const int CACHE_AGE_RANGE_FOR_SHARE_REMOVAL_HOURS = 2;
		private const int MIN_CACHE_AGE_FOR_SHARE_REMOVAL_HOURS = 1;
		private readonly IAddressParser _addressParser;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private readonly IShareRepository _shareRepository;
	}
}
