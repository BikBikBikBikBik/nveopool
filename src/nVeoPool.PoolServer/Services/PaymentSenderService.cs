#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Common.Extensions;
using nVeoPool.Common.Rpc;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Repositories;
using nVeoPool.Data.Services;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Notifications;
using nVeoPool.PoolServer.Services.RuntimeData;

namespace nVeoPool.PoolServer.Services {
	internal class PaymentSenderService : ReportingBackgroundServiceBase {
		public PaymentSenderService(IDistributedCacheAdapter cache, ILogger<PaymentSenderService> logger, IPaymentRepository paymentRepository, IPoolServerConfiguration poolServerConfiguration, IRpcClientAdapterFactory rpcClientAdapterFactory) : base(cache, poolServerConfiguration.ClusteringConfiguration, logger, poolServerConfiguration.ServicesConfiguration.PaymentSenderServiceUpdateIntervalSeconds, ClusterServerType.Master, nameof(PaymentSenderService)) {
			_batchSendEnabled = poolServerConfiguration.PaymentConfiguration.BatchSendDelaySeconds > 0 && poolServerConfiguration.PaymentConfiguration.BatchSendSize > 0;
			_effectiveTransactionFee = poolServerConfiguration.PaymentConfiguration.PoolPaysTransactionFee ? 0 : _poolServerConfiguration.PaymentConfiguration.TransactionFee;
			_lastRunWasSuccessful = true;
			_paymentRepository = paymentRepository;
			_poolServerConfiguration = poolServerConfiguration;
			_rpcClientAdapterFactory = rpcClientAdapterFactory;
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			cancellationToken.Register(async () => await ReportServiceAsStoppingAsync());
			var coinbaseAddress = String.Empty;

			while (!cancellationToken.IsCancellationRequested) {
				try {
					if (!_lastRunWasSuccessful) {
						_logger.LogError(GlobalEventList.Payment.StoppedPaymentsUntilPoolRestart, "{BackgroundService} skipping payments to avoid possible double-spend, payments were sent earlier but miner balances were not updated", _serviceName);
					} else {
						await ReportServiceAsExecutingAsync(GlobalEventList.Payment.PaymentSenderServiceExecuting);

						if (String.IsNullOrWhiteSpace(coinbaseAddress)) {
							coinbaseAddress = await _rpcClientAdapterFactory.GetClientAdapter().GetPubKeyAsync(cancellationToken);
						}

						if (await _rpcClientAdapterFactory.GetClientAdapter().GetKeysStatusAsync(cancellationToken)) {
							var minersForPayment = (await _paymentRepository.GetMinersForPaymentAsync(_poolServerConfiguration.PaymentConfiguration.MinimumPayoutThreshold)).ToList();
							var minerBatches = minersForPayment.Split(_poolServerConfiguration.PaymentConfiguration.BatchSendSize).ToList();

							if (_batchSendEnabled) {
								_logger.LogInformation("{BackgroundService} sending {PaymentCount} payments in {PaymentBatchCount} payment batches of size {PaymentBatchSize}", _serviceName, minersForPayment.Count, minerBatches.Count, _poolServerConfiguration.PaymentConfiguration.BatchSendSize);
							} else {
								_logger.LogInformation("{BackgroundService} sending {PaymentCount} payments without batching", _serviceName, minersForPayment.Count);
							}

							var failedMinerPayments = new List<(String MinerPublicKey, decimal PendingBalance)>();
							var runtimeData = new PaymentSenderData {
								BatchCount = minerBatches.Count,
								CurrentBatch = 1,
								ErrorCount = 0,
								PaymentCount = minersForPayment.Count,
								PendingRetryCount = 0,
								ProcessedCount = 0,
								RemainingInBatch = 0,
								RemainingTotal = minersForPayment.Count,
								SucessfullCount = 0
							};

							for (var i = 0; i < minerBatches.Count; i++) {
								runtimeData.CurrentBatch = i + 1;
								runtimeData.RemainingInBatch = minerBatches[i].Count;
								await UpdateServiceRuntimeDataAsync(runtimeData);

								var failedBatchPayments = await SendPaymentBatchAsync(coinbaseAddress, minerBatches[i], runtimeData, true, cancellationToken);

								if (failedBatchPayments == null) {
									_lastRunWasSuccessful = false;

									_logger.LogError("{BackgroundService} failed on payment batch {PaymentBatchNumber} of {PaymentBatchCount}", _serviceName, i + 1, minerBatches.Count);

									break;
								} else if (failedBatchPayments.Any()) {
									failedMinerPayments.AddRange(failedBatchPayments);

									_logger.LogInformation("{BackgroundService} saved {FailedBatchCount} failed payments to retry", _serviceName, failedBatchPayments.Count);
								}

								if (_batchSendEnabled) {
									_logger.LogInformation("{BackgroundService} successfully sent payment batch {PaymentBatchNumber} of {PaymentBatchCount}", _serviceName, i + 1, minerBatches.Count);

									if ((i + 1) < minerBatches.Count) {
										await Task.Delay(TimeSpan.FromSeconds(_poolServerConfiguration.PaymentConfiguration.BatchSendDelaySeconds), cancellationToken);
									}
								}
							}

							if (_lastRunWasSuccessful && failedMinerPayments.Any()) {
								var failedRetryPayments = await SendPaymentBatchAsync(coinbaseAddress, failedMinerPayments, runtimeData, false, cancellationToken);

								_logger.LogInformation("{BackgroundService} successfully sent {SuccessfulRetryCount} of {FailedPaymentCount} payments on retry", _serviceName, failedMinerPayments.Count - failedRetryPayments.Count, failedMinerPayments.Count);
							}
						} else {
							_logger.LogWarning("Unable to send payments from {BackgroundService}, keys must be unlocked", _serviceName);
						}
					}
				} catch (Exception e) {
					ReportUnhandledServiceException(e);
				}

				await ReportServiceAsSleepingAsync();
				await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
			}
		}
		#endregion

		private async Task<List<(String MinerPublicKey, decimal PendingBalance)>> SendPaymentBatchAsync(String coinbaseAddress, IEnumerable<(String MinerPublicKey, decimal PendingBalance)> minersForPayment, PaymentSenderData runtimeData, bool saveFailedForRetry, CancellationToken cancellationToken) {
			var failedMinerPayments = new List<(String MinerPublicKey, decimal PendingBalance)>();
			var minerPaymentTransactions = new List<(String MinerPublicKey, decimal Amount, long SubmittedOn, int SubmittedOnBlockHeight, decimal TransactionFee, String TransactionId)>();
							
			foreach (var (minerPublicKey, pendingBalance) in minersForPayment) {
				var sendAmount = (pendingBalance - _effectiveTransactionFee).TruncateToPaymentPrecision();

				try {
					var currentBlockHeight = await _rpcClientAdapterFactory.GetClientAdapter().GetCurrentHeightAsync(cancellationToken);
					var transactionId = "[INTERNAL]";

					if (!coinbaseAddress.Equals(minerPublicKey)) {
						transactionId = await _rpcClientAdapterFactory.GetClientAdapter().SpendAsync(minerPublicKey, sendAmount, _poolServerConfiguration.PaymentConfiguration.TransactionFee, true);

						_logger.LogInformation("{BackgroundService} got transaction id {TransactionId} from call to spend", _serviceName, transactionId);
					}

					minerPaymentTransactions.Add((minerPublicKey, sendAmount, new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds(), currentBlockHeight, _effectiveTransactionFee, transactionId));

					_logger.LogInformation("{BackgroundService} successfully sent payment for {Amount} to {MinerPublicKey}", _serviceName, sendAmount, minerPublicKey);
				} catch (Exception e) {
					failedMinerPayments.Add((minerPublicKey, pendingBalance));

					if (saveFailedForRetry) {
						_logger.LogInformation("{BackgroundService} saving failed payment for {Amount} to {MinerPublicKey} for retry", _serviceName, sendAmount, minerPublicKey);

						runtimeData.PendingRetryCount += 1;
						runtimeData.RemainingInBatch -= 1;
					} else {
						_logger.LogError(GlobalEventList.Payment.ErrorSendingIndividualPayment, e, "Exception during {BackgroundService} while attempting to send payment for {Amount} to {MinerPublicKey}", _serviceName, sendAmount, minerPublicKey);

						runtimeData.ErrorCount += 1;
						runtimeData.PendingRetryCount -= 1;
						runtimeData.ProcessedCount += 1;
						runtimeData.RemainingTotal -= 1;
					}

					await UpdateServiceRuntimeDataAsync(runtimeData);
				}

				try {
					if (minerPaymentTransactions.Any()) {
						await _paymentRepository.SaveTransactionsAndUpdateMinerBalancesAsync(minerPaymentTransactions);

						_logger.LogInformation("{BackgroundService} successfully saved {SentTransactionCount} sent transactions", _serviceName, minerPaymentTransactions.Count);

						runtimeData.PendingRetryCount -= saveFailedForRetry ? 0 : minerPaymentTransactions.Count;
						runtimeData.ProcessedCount += minerPaymentTransactions.Count;
						runtimeData.RemainingInBatch -= saveFailedForRetry ? minerPaymentTransactions.Count : 0;
						runtimeData.RemainingTotal -= minerPaymentTransactions.Count;
						runtimeData.SucessfullCount += minerPaymentTransactions.Count;

						minerPaymentTransactions.Clear();

						await UpdateServiceRuntimeDataAsync(runtimeData);
					}
				} catch (Exception e) {
					_logger.LogError(e, "Exception during {BackgroundService} while attempting to save {SentTransactionCount} sent transactions", _serviceName, minerPaymentTransactions.Count);
				}

				await Task.Delay(TimeSpan.FromMilliseconds(_poolServerConfiguration.PaymentConfiguration.IndividualSendDelayMilliseconds), cancellationToken);
			}

			if (minerPaymentTransactions.Any()) {
				var transactionBundleId = Guid.NewGuid().ToString();
				_logger.LogWarning("{BackgroundService} final attempt to save {TransactionBundleId} with transactions: {FailedSavedTransactions}", _serviceName, transactionBundleId, minerPaymentTransactions);

				try {
					await _paymentRepository.SaveTransactionsAndUpdateMinerBalancesAsync(minerPaymentTransactions);

					_logger.LogInformation("{BackgroundService} successfully saved {TransactionBundleId}", _serviceName, transactionBundleId);

					runtimeData.PendingRetryCount -= saveFailedForRetry ? 0 : minerPaymentTransactions.Count;
					runtimeData.ProcessedCount += minerPaymentTransactions.Count;
					runtimeData.RemainingInBatch -= saveFailedForRetry ? minerPaymentTransactions.Count : 0;
					runtimeData.RemainingTotal -= minerPaymentTransactions.Count;
					runtimeData.SucessfullCount += minerPaymentTransactions.Count;

					await UpdateServiceRuntimeDataAsync(runtimeData);
				} catch (Exception e) {
					_logger.LogError(GlobalEventList.Payment.ErrorSavingSuccessfullySentPayments, e, "Critical error during {BackgroundService} while attempting final save of {TransactionBundleId}, payments were sent but miner balances were not updated", _serviceName, transactionBundleId);

					runtimeData.ErrorCount += minerPaymentTransactions.Count;
					runtimeData.PendingRetryCount -= saveFailedForRetry ? 0 : minerPaymentTransactions.Count;
					runtimeData.ProcessedCount += minerPaymentTransactions.Count;
					runtimeData.RemainingInBatch -= saveFailedForRetry ? minerPaymentTransactions.Count : 0;
					runtimeData.RemainingTotal -= minerPaymentTransactions.Count;

					await UpdateServiceRuntimeDataAsync(runtimeData);

					return null;
				}
			}

			return failedMinerPayments;
		}

		private readonly bool _batchSendEnabled;
		private readonly decimal _effectiveTransactionFee;
		private bool _lastRunWasSuccessful;
		private readonly IPaymentRepository _paymentRepository;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private readonly IRpcClientAdapterFactory _rpcClientAdapterFactory;
	}
}
