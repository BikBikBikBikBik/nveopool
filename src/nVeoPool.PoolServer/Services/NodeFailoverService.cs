#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Common.Models;
using nVeoPool.Common.Rpc;
using nVeoPool.Data;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Services;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Models;
using nVeoPool.PoolServer.Notifications;

namespace nVeoPool.PoolServer.Services {
	internal class NodeFailoverService : ReportingBackgroundServiceBase {
		public NodeFailoverService(IDistributedCacheAdapter cache, IConnectionFactory connectionFactory, ILogger<NodeFailoverService> logger, IPoolServerConfiguration poolServerConfiguration, IRpcClientAdapterFactory rpcClientAdapterFactory) : base(cache, poolServerConfiguration.ClusteringConfiguration, logger, poolServerConfiguration.ServicesConfiguration.NodeFailoverServiceUpdateIntervalSeconds, new [] { ClusterServerType.Master, ClusterServerType.Slave }, nameof(NodeFailoverService)) {
			_connectionFactory = connectionFactory;
			_currentPreferredNodeClientId = 1;
			_poolServerConfiguration = poolServerConfiguration;
			_rpcClientAdapterFactory = rpcClientAdapterFactory;
			_backupNodes = _rpcClientAdapterFactory.GetBackupClientAdapters().ToList();
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			if (!_backupNodes.Any()) {
				_logger.LogInformation("{BackgroundService} found no backup nodes", _serviceName);

				await ReportServiceAsStoppingAsync();
			} else {
				cancellationToken.Register(async () => await ReportServiceAsStoppingAsync());

				while (!cancellationToken.IsCancellationRequested) {
					try {
						await ReportServiceAsExecutingAsync();

						var topBlocksTask = GetTopBlocksAsync(cancellationToken);
						var primaryNodeClientHostName = "[UNAVAILABLE]";
						var primaryNodeClientId = (ulong?)null;

						var primaryNodeTopBlock = new Block { Height = 0 };
						try {
							var primaryNodeClient = _rpcClientAdapterFactory.GetClientAdapter();

							(primaryNodeClientHostName, primaryNodeClientId) = (primaryNodeClient.HostName, primaryNodeClient.Id);

							primaryNodeTopBlock = await primaryNodeClient.GetTopAsync(cancellationToken);
						} catch (Exception e) {
							_logger.LogWarning(e, "{BackgroundService} was unable to retrieve top height from primary node {NodeAddress}", _serviceName, primaryNodeClientHostName);
						}

						var topBlocks = (await topBlocksTask).ToList();

						if (!topBlocks.Any()) {
							_logger.LogWarning("{BackgroundService} received no backup node responses", _serviceName);
						} else {
							var higherBackupNodes = topBlocks.Where(b => b.Block != null && b.Block.Height > primaryNodeTopBlock.Height && (b.Block.Height - primaryNodeTopBlock.Height) >= _poolServerConfiguration.WorkProtocolConfiguration.MaximumTrailingBlocks).ToList();

							if (higherBackupNodes.Any()) {
								var (highestBackupNodeClient, highestBlock) = higherBackupNodes.First();

								if (highestBackupNodeClient.Id != _currentPreferredNodeClientId && highestBlock.Height > (topBlocks.SingleOrDefault(b => b.NodeClient.Id == _currentPreferredNodeClientId).Block?.Height ?? 0)) {
									_logger.LogWarning(GlobalEventList.Mining.PossiblePrimaryNodeSyncIssue, "{BackgroundService} found potentially out of sync primary node {NodeAddress} at height {PrimaryBlockHeight}, switching to backup node {BackupNodeAddress} at height {BackupBlockHeight}", _serviceName, primaryNodeClientHostName, primaryNodeTopBlock.Height, highestBackupNodeClient.HostName, highestBlock.Height);

									var orderedBackupNodes = higherBackupNodes.Skip(1).Select(b => b.NodeClient.Id).ToList();
									if (primaryNodeClientId.HasValue) {
										orderedBackupNodes.Add(primaryNodeClientId.Value);
									}

									_connectionFactory.GetSubscriber().PublishImmediate(_poolServerConfiguration.ChannelConfiguration.PreferredNodeClientChanged, new BackupInfo {
										BackupNodeClientIds = orderedBackupNodes,
										PreferredNodeClientId = highestBackupNodeClient.Id
									});

									_currentPreferredNodeClientId = highestBackupNodeClient.Id;
								} else {
									_logger.LogWarning(GlobalEventList.Mining.PossiblePrimaryNodeSyncIssue, "{BackgroundService} found potentially out of sync primary node {NodeAddress}, keeping current backup node", _serviceName, primaryNodeClientHostName);
								}
							} else {
								if (primaryNodeClientId.HasValue && primaryNodeClientId.Value != _currentPreferredNodeClientId) {
									_logger.LogInformation(GlobalEventList.Mining.PrimaryNodeRecovered, "{BackgroundService} switching from backup node to primary node {NodeAddress}", _serviceName, primaryNodeClientHostName);

									var orderedBackupNodes = topBlocks.Where(b => b.Block != null).Select(b => b.NodeClient.Id).ToList();

									_connectionFactory.GetSubscriber().PublishImmediate(_poolServerConfiguration.ChannelConfiguration.PreferredNodeClientChanged, new BackupInfo {
										BackupNodeClientIds = orderedBackupNodes,
										PreferredNodeClientId = primaryNodeClientId.Value
									});

									_currentPreferredNodeClientId = primaryNodeClientId.Value;
								}

								var lowerBackupNodes = topBlocks.Where(b => b.Block == null || (primaryNodeTopBlock.Height > b.Block.Height && (primaryNodeTopBlock.Height - b.Block.Height) >= _poolServerConfiguration.WorkProtocolConfiguration.MaximumTrailingBlocks)).ToList();

								if (lowerBackupNodes.Any()) {
									_logger.LogInformation(GlobalEventList.Mining.PossibleBackupNodeSyncIssue, "{BackgroundService} found potentially out of sync backup nodes [{NodeAddresses}]", _serviceName, String.Join(", ", lowerBackupNodes.Select(b => $"{b.NodeClient.HostName} - {(b.Block != null ? b.Block.Height.ToString() : "?")}")));
								} else {
									_logger.LogDebug("{BackgroundService} found all backup nodes in sync with primary node", _serviceName);
								}
							}
						}
					} catch (Exception e) {
						ReportUnhandledServiceException(e);
					}

					await ReportServiceAsSleepingAsync();
					await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
				}
			}
		}
		#endregion

		private async Task<IEnumerable<(IRpcClientAdapter NodeClient, Block Block)>> GetTopBlocksAsync(CancellationToken cancellationToken) {
			try {
				var topBlocks = await Task.WhenAll(_backupNodes.Select(n => GetTopBlockAsync(n, cancellationToken)));

				return topBlocks.OrderByDescending(b => b.Block != null ? b.Block.Height : 0);
			} catch (Exception e) {
				_logger.LogWarning(e, "{BackgroundService} was unable to retrieve backup node top heights", _serviceName);
			}

			return new List<(IRpcClientAdapter NodeClient, Block Block)>();
		}

		private async Task<(IRpcClientAdapter NodeClient, Block Block)> GetTopBlockAsync(IRpcClientAdapter nodeClient, CancellationToken cancellationToken) {
			try {
				var topBlock = await nodeClient.GetTopAsync(cancellationToken);

				_logger.LogInformation("{BackgroundService} retrieved top height {BackupNodeBlockHeight} from backup node {NodeAddress}", _serviceName, topBlock.Height, nodeClient.HostName);

				return (nodeClient, topBlock);
			} catch (Exception) {
				_logger.LogWarning("{BackgroundService} was unable to retrieve top height from backup node {NodeAddress}", _serviceName, nodeClient.HostName);
			}

			return (nodeClient, null);
		}

		private readonly IList<IRpcClientAdapter> _backupNodes;
		private readonly IConnectionFactory _connectionFactory;
		private ulong _currentPreferredNodeClientId;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private readonly IRpcClientAdapterFactory _rpcClientAdapterFactory;
	}
}
