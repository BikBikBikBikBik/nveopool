#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Repositories;
using nVeoPool.Data.Services;
using nVeoPool.PoolServer.Configuration;

namespace nVeoPool.PoolServer.Services {
	internal class ShareArchiverService : ReportingBackgroundServiceBase {
		public ShareArchiverService(IDistributedCacheAdapter cache, ILogger<ShareArchiverService> logger, IPoolServerConfiguration poolServerConfiguration, IShareRepository shareRepository) : base(cache, poolServerConfiguration.ClusteringConfiguration, logger, poolServerConfiguration.ServicesConfiguration.ShareArchiverServiceUpdateIntervalSeconds, ClusterServerType.Master, nameof(ShareArchiverService)) {
			_poolServerConfiguration = poolServerConfiguration;
			_shareRepository = shareRepository;
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			cancellationToken.Register(async () => await ReportServiceAsStoppingAsync());

			while (!cancellationToken.IsCancellationRequested) {
				try {
					await ReportServiceAsExecutingAsync();

					var archivedShareCutoffTime = new DateTimeOffset(DateTime.UtcNow - TimeSpan.FromSeconds(_runInterval)).ToUnixTimeSeconds();
					var archivedShareCount = await _shareRepository.ArchiveSharesBeforeCutoffAsync(archivedShareCutoffTime);

					_logger.LogInformation("{BackgroundService} successfully archived {ArchivedShareCount} accepted shares", _serviceName, archivedShareCount);

					var archivedShareRemovalCutoffTime = new DateTimeOffset(DateTime.UtcNow - TimeSpan.FromDays(_poolServerConfiguration.ServicesConfiguration.ShareArchiverServiceArchivedShareLifetimeDays)).ToUnixTimeSeconds();
					var removedArchivedShareCount = await _shareRepository.RemoveArchivedSharesBeforeCutoffAsync(archivedShareRemovalCutoffTime);

					_logger.LogInformation("{BackgroundService} successfully removed {DeletedArchivedShareCount} archived accepted shares", _serviceName, removedArchivedShareCount);
				} catch (Exception e) {
					ReportUnhandledServiceException(e);
				}

				await ReportServiceAsSleepingAsync();
				await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
			}
		}
		#endregion

		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private readonly IShareRepository _shareRepository;
	}
}
