#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Common.Rpc;
using nVeoPool.Data;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Models;
using nVeoPool.Data.Repositories;
using nVeoPool.Data.Services;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Models;
using nVeoPool.PoolServer.Notifications;
using nVeoPool.PoolServer.Payment;

namespace nVeoPool.PoolServer.Services {
	internal class BlockCandidateProcessorService : ReportingBackgroundServiceBase {
		public BlockCandidateProcessorService(IBlockRepository blockRepository, IDistributedCacheAdapter cache, IConnectionFactory connectionFactory, ILogger<BlockCandidateProcessorService> logger, IPaymentRepository paymentRepository, IPaymentStrategy paymentStrategy, IPoolServerConfiguration poolServerConfiguration, IRpcClientAdapterFactory rpcClientAdapterFactory, IShareRepository shareRepository) : base(cache, poolServerConfiguration.ClusteringConfiguration, logger, poolServerConfiguration.ServicesConfiguration.BlockCandidateProcessorServiceUpdateIntervalSeconds, ClusterServerType.Master, nameof(BlockCandidateProcessorService)) {
			_blockRepository = blockRepository;
			_paymentRepository = paymentRepository;
			_paymentStrategy = paymentStrategy;
			_poolServerConfiguration = poolServerConfiguration;
			_preferredNodeClientId = 1;
			_rpcClientAdapterFactory = rpcClientAdapterFactory;
			_shareRepository = shareRepository;
			_totalFeePercentPoints = _poolServerConfiguration.PaymentConfiguration.FeeMap.Sum(f => f.Value) + _poolServerConfiguration.PaymentConfiguration.PoolFee;
			_totalFee = _totalFeePercentPoints / 100;

			connectionFactory.GetSubscriber().SubscribeImmediate<BackupInfo>(_poolServerConfiguration.ChannelConfiguration.PreferredNodeClientChanged, OnPreferredNodeClientChangedMessageReceived);
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			cancellationToken.Register(async () => await ReportServiceAsStoppingAsync());
			var coinbaseAddress = String.Empty;

			while (!cancellationToken.IsCancellationRequested) {
				try {
					await ReportServiceAsExecutingAsync();

					if (String.IsNullOrWhiteSpace(coinbaseAddress)) {
						coinbaseAddress = await _rpcClientAdapterFactory.GetClientAdapter().GetPubKeyAsync(cancellationToken);
					}

					await ProcessBlockCandidatesAsync(coinbaseAddress, cancellationToken);
					
					await ProcessBlockRewardsAsync(coinbaseAddress);

					await ScanForUnrecordedBlocksAsync(coinbaseAddress, cancellationToken);
				} catch (Exception e) {
					ReportUnhandledServiceException(e);
				}

				await ReportServiceAsSleepingAsync();
				await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
			}
		}
		#endregion

		private void OnPreferredNodeClientChangedMessageReceived(String channelName, BackupInfo backupInfo) {
			_preferredNodeClientId = backupInfo.PreferredNodeClientId;

			_logger.LogDebug("{BackgroundService} received new preferred client id {PreferredNodeClientId}", _serviceName, _preferredNodeClientId);
		}

		private async Task ProcessBlockCandidatesAsync(String coinbaseAddress, CancellationToken cancellationToken) {
			var currentBlockHeight = await _rpcClientAdapterFactory.GetClientAdapter(_preferredNodeClientId).GetCurrentHeightAsync(cancellationToken);
			var blockCandidateHeights = (await _blockRepository.GetBlockCandidatesForProcessingAsync(currentBlockHeight - _poolServerConfiguration.PaymentConfiguration.BlockMaturityDepth)).ToList();
			var confirmedBlockHeights = new List<int>();
			var orphanedBlockHeights = new List<int>();

			foreach (var blockHeight in blockCandidateHeights) {
				var fullBlock = await _rpcClientAdapterFactory.GetClientAdapter(_preferredNodeClientId).GetBlockAsync(blockHeight, cancellationToken);

				if (coinbaseAddress.Equals(fullBlock.CoinbaseAddress)) {
					confirmedBlockHeights.Add(blockHeight);
				} else {
					orphanedBlockHeights.Add(blockHeight);
				}
			}

			if (blockCandidateHeights.Any()) {
				await _blockRepository.UpdateBlockCandidateStatusesAsync(confirmedBlockHeights, orphanedBlockHeights);

				_logger.LogInformation(GlobalEventList.Blocks.BlockCandidatesProcessed, "{BackgroundService} successfully confirmed blocks {ConfirmedBlocks} and orphaned blocks {OrphanedBlocks}", _serviceName, confirmedBlockHeights, orphanedBlockHeights);
			}
		}

		private async Task ProcessBlockRewardsAsync(String coinbaseAddress) {
			var confirmedBlockHeights = await _blockRepository.GetConfirmedBlocksForPaymentAsync();
			var blockSharesMap = await _shareRepository.GetSharesForBlocksAsync(confirmedBlockHeights.Select(b => (b, b - _poolServerConfiguration.PaymentConfiguration.RoundLengthBlocks, b - 1)));

			foreach (var blockShares in blockSharesMap.Where(b => b.Key > 0)) {
				if (!blockShares.Value.Any()) {
					_logger.LogError("{BackgroundService} skipping reward processing for block {BlockNumber}, found 0 shares", _serviceName, blockShares.Key);

					continue;
				}

				try {
					var blockRewards = new List<(decimal FeePercent, String MinerPublicKey, decimal Reward)>();
					var minerPaymentMap = _paymentStrategy.CalculateRewardsForBlock(blockShares.Value);
					var totalMinerRewards = 0m;

					foreach (var minerPayment in minerPaymentMap) {
						//TODO: Get overall block reward (base + tx fees)?
						var preFeeReward = minerPayment.Value * BLOCK_REWARD;
						var reward = preFeeReward - (preFeeReward * _totalFee);
						blockRewards.Add((_totalFee, minerPayment.Key, reward));

						totalMinerRewards += reward;
					}
					
					var remainingBlockReward = BLOCK_REWARD - totalMinerRewards;
					foreach (var feeRecipient in _poolServerConfiguration.PaymentConfiguration.FeeMap.Where(f => !coinbaseAddress.Equals(f.Key))) {
						var reward = (feeRecipient.Value / _totalFeePercentPoints) * remainingBlockReward;

						var existingBlockReward = blockRewards.SingleOrDefault(b => b.MinerPublicKey.Equals(feeRecipient.Key));

						if (!String.IsNullOrWhiteSpace(existingBlockReward.MinerPublicKey)) {
							blockRewards.Remove(existingBlockReward);

							blockRewards.Add((existingBlockReward.FeePercent, feeRecipient.Key, existingBlockReward.Reward + reward));
						} else {
							blockRewards.Add((0, feeRecipient.Key, reward));
						}
					}

					await _paymentRepository.SaveRewardsForBlockAsync(blockShares.Key, blockRewards);

					_logger.LogInformation(GlobalEventList.Payment.RewardsSavedForBlock, "{BackgroundService} successfully processed rewards for block {BlockNumber}", _serviceName, blockShares.Key);
				} catch (Exception e) {
					_logger.LogError(e, "Error during {BackgroundService} while attempting to process rewards for block {BlockNumber}", _serviceName, blockShares.Key);
				}
			}
		}

		private async Task ScanForUnrecordedBlocksAsync(String coinbaseAddress, CancellationToken cancellationToken) {
			var lastScannedBlockHeight = await _blockRepository.GetLastScannedBlockAsync();

			if (lastScannedBlockHeight > 0) {
				var blockHeightToScan = lastScannedBlockHeight + 1;
				var recordedBlockCandidates = (await _blockRepository.GetAllBlockCandidatesAfterHeightAsync(blockHeightToScan)).ToList();

				while (blockHeightToScan < (await _rpcClientAdapterFactory.GetClientAdapter(_preferredNodeClientId).GetCurrentHeightAsync(cancellationToken) - _poolServerConfiguration.PaymentConfiguration.BlockMaturityDepth)) {
					if (recordedBlockCandidates.All(b => b != blockHeightToScan)) {
						var fullBlock = await _rpcClientAdapterFactory.GetClientAdapter(_preferredNodeClientId).GetBlockAsync(blockHeightToScan, cancellationToken);

						if (coinbaseAddress.Equals(fullBlock.CoinbaseAddress)) {
							try {
								await _blockRepository.SaveUnrecordedBlockCandidateAsync(new BlockCandidate {
									BlockHeight = blockHeightToScan,
									MinerPublicKey = "[UNKNOWN]",
									Nonce = "[UNKNOWN]",
									SubmittedOn = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds()
								});

								_logger.LogInformation(GlobalEventList.Blocks.UnrecordedBlockCandidateSaved, "{BackgroundService} successfully saved unrecorded block candidate {BlockNumber}", _serviceName, blockHeightToScan);
							} catch (Exception e) {
								_logger.LogError(e, "Error during {BackgroundService} while attempting to save unrecorded block candidate {BlockNumber}", _serviceName, blockHeightToScan);
							}
						}
					}

					await _blockRepository.UpdateLastScannedBlockAsync(blockHeightToScan++);
				}

				_logger.LogInformation("{BackgroundService} successfully scanned for unrecorded blocks through block {BlockNumber}", _serviceName, blockHeightToScan - 1);
			} else {
				var currentHeight = await _rpcClientAdapterFactory.GetClientAdapter(false).GetCurrentHeightAsync(cancellationToken);
				
				await _blockRepository.UpdateLastScannedBlockAsync(currentHeight, true);

				_logger.LogInformation("{BackgroundService} starting unrecorded block scan at height {BlockNumber}", _serviceName, currentHeight);
			}
		}

		private const decimal BLOCK_REWARD = 1;
		private readonly IBlockRepository _blockRepository;
		private readonly IPaymentRepository _paymentRepository;
		private readonly IPaymentStrategy _paymentStrategy;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private ulong _preferredNodeClientId;
		private readonly IRpcClientAdapterFactory _rpcClientAdapterFactory;
		private readonly IShareRepository _shareRepository;
		private readonly decimal _totalFee;
		private readonly decimal _totalFeePercentPoints;
	}
}
