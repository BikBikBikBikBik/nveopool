#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Repositories;
using nVeoPool.Data.Services;
using nVeoPool.PoolServer.Configuration;

namespace nVeoPool.PoolServer.Services {
	internal class BanManagementService : ReportingBackgroundServiceBase {
		public BanManagementService(IBanRepository banRepository, IDistributedCacheAdapter cache, ILogger<BanManagementService> logger, IPoolServerConfiguration poolServerConfiguration) : base(cache, poolServerConfiguration.ClusteringConfiguration, logger, poolServerConfiguration.ServicesConfiguration.BanManagementServiceUpdateIntervalSeconds, new [] { ClusterServerType.Master, ClusterServerType.Slave }, nameof(BanManagementService)) {
			_banManagementEnabled = poolServerConfiguration.BanningConfiguration != null;
			_banRepository = banRepository;
			_whitelistEnabled = _banManagementEnabled && poolServerConfiguration.BanningConfiguration.WhitelistEnabled;
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			if (!_banManagementEnabled) {
				_logger.LogInformation("{BackgroundService} detected empty banning configuration", _serviceName);

				await ReportServiceAsStoppingAsync();
			} else {
				cancellationToken.Register(async () => await ReportServiceAsStoppingAsync());

				while (!cancellationToken.IsCancellationRequested) {
					try {
						await ReportServiceAsExecutingAsync();

						await ManagePublicKeyWhitelistAsync();
					} catch (Exception e) {
						ReportUnhandledServiceException(e);
					}

					await ReportServiceAsSleepingAsync();
					await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
				}
			}
		}
		#endregion

		private async Task ManagePublicKeyWhitelistAsync() {
			if (_whitelistEnabled) {
				var whitelistedPublicKeyCount = await _cache.UpdatePublicKeyWhitelistAsync((await _banRepository.GetMinerPublicKeyWhitelistAsync()).Select(m => m.ToLowerInvariant()));

				if (whitelistedPublicKeyCount > 0) {
					_logger.LogInformation("{BackgroundService} successfully cached {WhitelistCount} whitelisted public keys", _serviceName, whitelistedPublicKeyCount);
				} else {
					_logger.LogError("{BackgroundService} whitelisted 0 public keys", _serviceName, whitelistedPublicKeyCount);
				}
			}
		}

		private readonly bool _banManagementEnabled;
		private readonly IBanRepository _banRepository;
		private readonly bool _whitelistEnabled;
	}
}
