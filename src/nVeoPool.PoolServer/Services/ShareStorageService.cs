#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Models;
using nVeoPool.Data.Repositories;
using nVeoPool.Data.Services;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.PoW;

namespace nVeoPool.PoolServer.Services {
	internal class ShareStorageService : ReportingBackgroundServiceBase {
		public ShareStorageService(IAddressParser addressParser, IDistributedCacheAdapter cache, IConnectionFactory connectionFactory, ILogger<ShareStorageService> logger, IPoolServerConfiguration poolServerConfiguration, IPoWAlgorithm powAlgorithm, IShareRepository shareRepository) : base(cache, poolServerConfiguration.ClusteringConfiguration, logger, poolServerConfiguration.ServicesConfiguration.ShareStorageServiceUpdateIntervalSeconds, new [] { ClusterServerType.Master, ClusterServerType.Slave }, $"{nameof(ShareStorageService)}_{poolServerConfiguration.ClusteringConfiguration.ServerId}") {
			_addressParser = addressParser;
			_connectionFactory = connectionFactory;
			_lastSaveTime = DateTime.UtcNow + MAX_WAIT_BETWEEN_SAVES;
			_poolServerConfiguration = poolServerConfiguration;
			_powAlgorithm = powAlgorithm;
			_shareBuffer = new List<Share>();
			_shareQueue = new ConcurrentQueue<Share>();
			_shareRepository = shareRepository;

			_connectionFactory.GetSubscriber().SubscribeImmediate<Share>(poolServerConfiguration.ChannelConfiguration.ShareFound(_poolServerConfiguration.ClusteringConfiguration.ServerId), OnShareFoundMessageReceived);
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			cancellationToken.Register(async () => {
				try {
					await ProcessShareQueueAsync(true);
				} finally {
					await ReportServiceAsStoppingAsync();
				}
			});

			while (!cancellationToken.IsCancellationRequested) {
				try {
					await ReportServiceAsExecutingAsync();

					await ProcessShareQueueAsync(false);
				} catch (Exception e) {
					ReportUnhandledServiceException(e);
				}

				await ReportServiceAsSleepingAsync();
				await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
			}
		}
		#endregion

		private void OnShareFoundMessageReceived(String channelName, Share share) {
			_shareQueue.Enqueue(share);
		}

		private async Task ProcessShareQueueAsync(bool fullFlush) {
			while (_shareQueue.Count > 0) {
				if (_shareQueue.TryDequeue(out var share)) {
					share.HashCount = _powAlgorithm.GetHashCountForDifficulty(share.JobDifficulty);
					(share.MinerPublicKey, share.MinerWorkerId) = _addressParser.ParseMinerAddress(share.MinerAddress);
					_shareBuffer.Add(share);
					
					if (_shareBuffer.Count >= _poolServerConfiguration.DatabaseConfiguration.MaxBatchSize) {
						await SaveShareBufferAsync();
					}
				}
			}

			if ((DateTime.UtcNow - _lastSaveTime >= MAX_WAIT_BETWEEN_SAVES) || (fullFlush && _shareBuffer.Count > 0)) {
				await SaveShareBufferAsync();
			}
		}

		private async Task SaveShareBufferAsync() {
			await _shareRepository.SaveSharesAsync(_shareBuffer, true);

			_shareBuffer.Clear();
			_lastSaveTime = DateTime.UtcNow;
		}

		private readonly TimeSpan MAX_WAIT_BETWEEN_SAVES = TimeSpan.FromSeconds(15);
		private readonly IAddressParser _addressParser;
		private readonly IConnectionFactory _connectionFactory;
		private DateTime _lastSaveTime;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private readonly IPoWAlgorithm _powAlgorithm;
		private readonly List<Share> _shareBuffer;
		private readonly ConcurrentQueue<Share> _shareQueue;
		private readonly IShareRepository _shareRepository;
	}
}
