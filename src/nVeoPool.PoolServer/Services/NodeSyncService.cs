#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Common.Rpc;
using nVeoPool.Data;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Services;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Models;

namespace nVeoPool.PoolServer.Services {
	internal class NodeSyncService : ReportingBackgroundServiceBase {
		public NodeSyncService(IDistributedCacheAdapter cache, IConnectionFactory connectionFactory, ILogger<NodeSyncService> logger, IPoolServerConfiguration poolServerConfiguration, IRpcClientAdapterFactory rpcClientAdapterFactory) : base(cache, poolServerConfiguration.ClusteringConfiguration, logger, poolServerConfiguration.ServicesConfiguration.NodeSyncServiceUpdateIntervalSeconds, ClusterServerType.Master, nameof(NodeSyncService)) {
			_poolServerConfiguration = poolServerConfiguration;
			_preferredNodeClientId = 1;
			_rpcClientAdapterFactory = rpcClientAdapterFactory;

			connectionFactory.GetSubscriber().SubscribeImmediate<BackupInfo>(_poolServerConfiguration.ChannelConfiguration.PreferredNodeClientChanged, OnPreferredNodeClientChangedMessageReceived);
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			if (!_poolServerConfiguration.DaemonClientConfiguration.PreferredPeerAddresses.Any()) {
				_logger.LogInformation("{BackgroundService} found no preferred peers", _serviceName);

				await ReportServiceAsStoppingAsync();
			} else {
				cancellationToken.Register(async () => await ReportServiceAsStoppingAsync());

				var formattedPeerAddresses = _poolServerConfiguration.DaemonClientConfiguration.PreferredPeerAddresses.Select(p => p.Port == 80 ? new UriBuilder(p) { Port = DEFAULT_NODE_EXTERNAL_API_PORT }.Uri : p).ToList();

				while (!cancellationToken.IsCancellationRequested) {
					try {
						await ReportServiceAsExecutingAsync();

						var nodeHeights = (await GetNodeHeightsAsync(formattedPeerAddresses, cancellationToken)).ToList();
						var nodeClient = _rpcClientAdapterFactory.GetClientAdapter(_preferredNodeClientId);
						var currentHeight = await nodeClient.GetCurrentHeightAsync(cancellationToken);

						if (nodeHeights.First().Height > currentHeight) {
							_logger.LogInformation("{BackgroundService} syncing with peer {NodeAddress} with height {PeerBlockHeight}, current internal height is {CurrentBlockHeight}", _serviceName, nodeHeights.First().NodeAddress, nodeHeights.First().Height, currentHeight);

							await nodeClient.SyncHeadersAsync(nodeHeights.First().NodeAddress);
						}
					} catch (Exception e) {
						ReportUnhandledServiceException(e);
					}

					await ReportServiceAsSleepingAsync();
					await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
				}
			}
		}
		#endregion

		private async Task<IEnumerable<(Uri NodeAddress, int Height)>> GetNodeHeightsAsync(IEnumerable<Uri> nodeAddresses, CancellationToken cancellationToken) {
			try {
				var nodeHeights = await Task.WhenAll(nodeAddresses.Select(n => GetNodeHeightAsync(n, cancellationToken)));

				return nodeHeights.Where(h => h.Height.HasValue).Select(h => (h.NodeAddress, h.Height.Value)).OrderByDescending(h => h.Value);
			} catch (Exception e) {
				_logger.LogError(e, "{BackgroundService} was unable to retrieve peer heights", _serviceName);
			}

			return new List<(Uri NodeAddress, int Height)>();
		}

		private async Task<(Uri NodeAddress, int? Height)> GetNodeHeightAsync(Uri nodeAddress, CancellationToken cancellationToken) {
			try {
				var client = _rpcClientAdapterFactory.GetClientAdapter(nodeAddress);

				var nodeHeight = await client.GetCurrentHeightAsync(cancellationToken);

				return (nodeAddress, nodeHeight);
			} catch (Exception e) {
				_logger.LogError(e, "{BackgroundService} was unable to retrieve height from peer {NodeAddress}", _serviceName, nodeAddress);
			}

			return (null, null);
		}

		private void OnPreferredNodeClientChangedMessageReceived(String channelName, BackupInfo backupInfo) {
			_preferredNodeClientId = backupInfo.PreferredNodeClientId;

			_logger.LogDebug("{BackgroundService} received new preferred client id {PreferredNodeClientId}", _serviceName, _preferredNodeClientId);
		}

		private const int DEFAULT_NODE_EXTERNAL_API_PORT = 8080;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private ulong _preferredNodeClientId;
		private readonly IRpcClientAdapterFactory _rpcClientAdapterFactory;
	}
}
