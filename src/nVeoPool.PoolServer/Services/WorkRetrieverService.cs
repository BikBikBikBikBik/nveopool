﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Common.Rpc;
using nVeoPool.Data;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Services;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Models;
using nVeoPool.PoolServer.Notifications;

namespace nVeoPool.PoolServer.Services {
	internal class WorkRetrieverService : ReportingBackgroundServiceBase {
		public WorkRetrieverService(IDistributedCacheAdapter cache, IConnectionFactory connectionFactory, ILogger<WorkRetrieverService> logger, IPoolServerConfiguration poolServerConfiguration, IRpcClientAdapterFactory rpcClientAdapterFactory) : base(cache, poolServerConfiguration.ClusteringConfiguration, logger, poolServerConfiguration.ServicesConfiguration.WorkRetrieverServiceUpdateIntervalMilliseconds, new [] { ClusterServerType.Master, ClusterServerType.Slave }, nameof(WorkRetrieverService)) {
			_backupInfo = new BackupInfo { BackupNodeClientIds = rpcClientAdapterFactory.GetBackupClientAdapters().Select(n => n.Id).ToList() };
			_connectionFactory = connectionFactory;
			_currentBlockHash = String.Empty;
			_currentBlockHeight = 0;
			_lastWorkRetrievedOn = 0;
			_poolServerConfiguration = poolServerConfiguration;
			_preferredNodeClientId = 1;
			_rpcClientAdapterFactory = rpcClientAdapterFactory;

			_connectionFactory.GetSubscriber().SubscribeImmediate<BackupInfo>(_poolServerConfiguration.ChannelConfiguration.PreferredNodeClientChanged, OnPreferredNodeClientChangedMessageReceived);
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			cancellationToken.Register(async () => await ReportServiceAsStoppingAsync());

			while (!cancellationToken.IsCancellationRequested) {
				try {
					await ReportServiceAsExecutingAsync();

					var nodeClient = _rpcClientAdapterFactory.GetClientAdapter(_preferredNodeClientId);
					var heightRetrievedSuccessfully = false;
					var workRetrivedSuccesfully = false;
					var blockDifficulty = 0;
					var fieldZero = 0;
					var blockHash = String.Empty;
					
					var miningData = await TryGetMiningDataAsync(nodeClient, _backupInfo.BackupNodeClientIds, cancellationToken);
					if (miningData.BlockDifficulty > 0) {
						(blockDifficulty, blockHash, fieldZero, nodeClient) = miningData;

						workRetrivedSuccesfully = true;
					}
					
					try {
						_currentBlockHeight = await nodeClient.GetCurrentHeightAsync(cancellationToken);

						heightRetrievedSuccessfully = true;
					} catch (Exception e) {
						_logger.LogError(GlobalEventList.Mining.ErrorRetrievingHeight, e, "Exception during {BackgroundService} while retrieving current block height", _serviceName);
					}

					if (heightRetrievedSuccessfully && workRetrivedSuccesfully && !_currentBlockHash.Equals(blockHash)) {
						_cache.UpdateNodeMiningDataImmediate(blockHash, blockDifficulty, _currentBlockHeight, fieldZero, nodeClient.Id);
						
						_connectionFactory.GetSubscriber().PublishImmediate(_poolServerConfiguration.ChannelConfiguration.NewMiningJob, new Work {
							BlockDifficulty = blockDifficulty,
							BlockHash = blockHash,
							BlockHeight = _currentBlockHeight,
						});

						_currentBlockHash = blockHash;

						_lastWorkRetrievedOn = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
					}

					var currentTime = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
					if (currentTime - _lastWorkRetrievedOn >= _poolServerConfiguration.WorkProtocolConfiguration.MaximumStaleWorkDurationSeconds) {
						_cache.DeleteNodeMiningDataImmediate();
						_connectionFactory.GetSubscriber().PublishImmediate(_poolServerConfiguration.ChannelConfiguration.WorkUnavailable, _lastWorkRetrievedOn);
						_currentBlockHash = String.Empty;

						_logger.LogError(GlobalEventList.Mining.NoWorkAvailable, "{BackgroundService} has not retrieved new work in {StaleWorkDuration} seconds, last retrieved at {LastWorkRetrievedOn}", _serviceName, currentTime - _lastWorkRetrievedOn, _lastWorkRetrievedOn);
					}
				} catch (Exception e) {
					ReportUnhandledServiceException(e);
				}

				await ReportServiceAsSleepingAsync();
				await Task.Delay(TimeSpan.FromMilliseconds(_runInterval), cancellationToken);
			}
		}
		#endregion

		private void OnPreferredNodeClientChangedMessageReceived(String channelName, BackupInfo backupInfo) {
			_backupInfo = backupInfo;
			_preferredNodeClientId = backupInfo.PreferredNodeClientId;

			_logger.LogDebug("{BackgroundService} received new preferred client id {PreferredNodeClientId} with backups [{BackupNodeClientIds}]", _serviceName, _preferredNodeClientId, String.Join(", ", _backupInfo.BackupNodeClientIds));
		}

		private async Task<(int BlockDifficulty, String BlockHash, int FieldZero, IRpcClientAdapter NodeClient)> TryGetMiningDataAsync(IRpcClientAdapter nodeClient, List<ulong> backupNodeClientIds, CancellationToken cancellationToken) {
			while (true) {
				try {
					var miningData = await nodeClient.GetWorkAsync(cancellationToken);

					return (miningData.BlockDifficulty, miningData.BlockHash, miningData.FieldZero, nodeClient);
				} catch (Exception eOuter) {
					_logger.LogWarning(eOuter, "Exception during {BackgroundService} while retrieving current mining data, attempting to set normal sync mode", _serviceName);

					try {
						await nodeClient.SetNormalSyncModeAsync(cancellationToken);

						var miningData = await nodeClient.GetWorkAsync(cancellationToken);

						return (miningData.BlockDifficulty, miningData.BlockHash, miningData.FieldZero, nodeClient);
					} catch (Exception eInner) {
						_logger.LogError(GlobalEventList.Mining.ErrorRetrievingMiningData, eInner, "Exception during {BackgroundService} while retrieving current mining data", _serviceName);
					}
				}

				if (backupNodeClientIds.Count > 0) {
					var nextNodeClient = _rpcClientAdapterFactory.GetClientAdapter(backupNodeClientIds.First());

					nodeClient = nextNodeClient;
					backupNodeClientIds = backupNodeClientIds.Skip(1).ToList();

					_logger.LogDebug("{BackgroundService} attempting to retrieve mining data again with {NodeAddress}", _serviceName, nodeClient.HostName);
				} else {
					return (0, null, 0, null);
				}
			}
		}

		private BackupInfo _backupInfo;
		private readonly IConnectionFactory _connectionFactory;
		private String _currentBlockHash;
		private int _currentBlockHeight;
		private long _lastWorkRetrievedOn;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private ulong _preferredNodeClientId;
		private readonly IRpcClientAdapterFactory _rpcClientAdapterFactory;
	}
}
