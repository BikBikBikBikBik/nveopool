#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Data;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Services;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Models;

namespace nVeoPool.PoolServer.Services {
	internal class DifficultyCalculatorService : ReportingBackgroundServiceBase {
		public DifficultyCalculatorService(IDistributedCacheAdapter cache, IConnectionFactory connectionFactory, ILogger<DifficultyCalculatorService> logger, IPoolServerConfiguration poolServerConfiguration) : base(cache, poolServerConfiguration.ClusteringConfiguration, logger, poolServerConfiguration.ServicesConfiguration.DifficultyCalculatorServiceUpdateIntervalSeconds, new [] { ClusterServerType.Master, ClusterServerType.Slave }, nameof(DifficultyCalculatorService)) {
			_connectionFactory = connectionFactory;
			_poolServerConfiguration = poolServerConfiguration;

			if ((!_poolServerConfiguration.VarDiffConfiguration.MaximumDifficulty.HasValue && !_poolServerConfiguration.VarDiffConfiguration.MinimumDifficulty.HasValue) || (_poolServerConfiguration.VarDiffConfiguration.MaximumDifficulty.Value == _poolServerConfiguration.VarDiffConfiguration.MinimumDifficulty.Value && _poolServerConfiguration.VarDiffConfiguration.MaximumDifficulty.Value == _poolServerConfiguration.VarDiffConfiguration.InitialDifficulty)) {
				_isFixedDifficulty = true;
			} else {
				var variance = _poolServerConfiguration.VarDiffConfiguration.IntervalVariancePercent / 100m * _poolServerConfiguration.VarDiffConfiguration.TargetShareIntervalSeconds;
				_maxShareIntervalSeconds = (int)(_poolServerConfiguration.VarDiffConfiguration.TargetShareIntervalSeconds + variance);
				_minShareIntervalSeconds = (int)(_poolServerConfiguration.VarDiffConfiguration.TargetShareIntervalSeconds - variance);
			}
		}

		#region BackgroundServiceBase
		protected override async Task ExecuteAsync(CancellationToken cancellationToken) {
			if (_isFixedDifficulty) {
				_logger.LogInformation("{BackgroundService} detected fixed difficulty of {FixedDifficulty}", _serviceName, _poolServerConfiguration.VarDiffConfiguration.InitialDifficulty);

				try {
					var activeWorkers = await _cache.GetActiveWorkersAsync();

					foreach (var worker in activeWorkers) {
						try {
							_cache.ClearWorkerCurrentJobDifficultyImmediate(worker);
						} catch (Exception e) {
							_logger.LogWarning(e, "{BackgroundService} failed to clear job difficulty for {MinerAddress}", _serviceName, worker);
						}
					}
				} catch (Exception e) {
					_logger.LogError(e, "{BackgroundService} failed to clear job difficulties on startup, miners may be using incorrect job difficulty", _serviceName);
				}

				await ReportServiceAsStoppingAsync();
			} else {
				cancellationToken.Register(async () => await ReportServiceAsStoppingAsync());

				while (!cancellationToken.IsCancellationRequested) {
					try {
						await ReportServiceAsExecutingAsync();

						foreach (var worker in await _cache.GetActiveWorkersAsync()) {
							var currentJobDifficultyTask = _cache.GetWorkerCurrentJobDifficultyAsync(worker);
							var lastShareTimeTask = _cache.GetWorkerLastShareTimeAsync(worker);
							var recentShareIntervals = (await _cache.GetWorkerRecentShareIntervalsAsync(worker)).Reverse().Take(_poolServerConfiguration.VarDiffConfiguration.IntervalSampleCount).ToList();
							var currentTime = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
							var lastShareTime = await lastShareTimeTask ?? 0;
							var timeSinceLastShare = (int)(currentTime - lastShareTime);
							if (timeSinceLastShare > _maxShareIntervalSeconds) {
								recentShareIntervals.Add(timeSinceLastShare);
							}
							var averageShareInterval = recentShareIntervals.Any() ? recentShareIntervals.Average() : 1;

							var newDifficulty = 0;
							var direction = 0;
							var currentJobDifficulty = await currentJobDifficultyTask ?? _poolServerConfiguration.VarDiffConfiguration.InitialDifficulty;

							if (averageShareInterval > _maxShareIntervalSeconds && currentJobDifficulty > _poolServerConfiguration.VarDiffConfiguration.MinimumDifficulty.Value) {
								newDifficulty = (int)(_poolServerConfiguration.VarDiffConfiguration.TargetShareIntervalSeconds / averageShareInterval * currentJobDifficulty);
								newDifficulty = newDifficulty > _poolServerConfiguration.VarDiffConfiguration.MinimumDifficulty.Value ? newDifficulty : _poolServerConfiguration.VarDiffConfiguration.MinimumDifficulty.Value;
								direction = -1;
							} else if (averageShareInterval < _minShareIntervalSeconds && currentJobDifficulty < _poolServerConfiguration.VarDiffConfiguration.MaximumDifficulty.Value) {
								newDifficulty = (int)(_poolServerConfiguration.VarDiffConfiguration.TargetShareIntervalSeconds / averageShareInterval * currentJobDifficulty);
								newDifficulty = newDifficulty < _poolServerConfiguration.VarDiffConfiguration.MaximumDifficulty.Value ? newDifficulty : _poolServerConfiguration.VarDiffConfiguration.MaximumDifficulty.Value;
								direction = 1;
							} else {
								continue;
							}

							if (Math.Abs(newDifficulty - currentJobDifficulty) / (decimal)currentJobDifficulty * 100 > _poolServerConfiguration.VarDiffConfiguration.MaximumDifficultyJumpPercent) {
								var change = (int)Math.Floor(_poolServerConfiguration.VarDiffConfiguration.MaximumDifficultyJumpPercent / 100m * currentJobDifficulty * direction);
								newDifficulty = currentJobDifficulty + change;
							}

							_cache.UpdateWorkerCurrentJobDifficultyImmediate(worker, newDifficulty);

							if (newDifficulty != currentJobDifficulty) {
								_connectionFactory.GetSubscriber().PublishImmediate(_poolServerConfiguration.ChannelConfiguration.WorkerDifficultyChanged, new WorkerDifficulty {
									FormattedMinerAddress = worker,
									NewDifficulty = newDifficulty
								});
							}
						}
					} catch (Exception e) {
						ReportUnhandledServiceException(e);
					}

					await ReportServiceAsSleepingAsync();
					await Task.Delay(TimeSpan.FromSeconds(_runInterval), cancellationToken);
				}
			}
		}
		#endregion

		private readonly IConnectionFactory _connectionFactory;
		private readonly bool _isFixedDifficulty;
		private readonly int _maxShareIntervalSeconds;
		private readonly int _minShareIntervalSeconds;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
	}
}
