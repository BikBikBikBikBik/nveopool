#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Serilog.Core;
using Serilog.Debugging;
using Serilog.Events;
using nVeoPool.Common.Rpc;

namespace nVeoPool.PoolServer.Notifications {
	internal class DiscordNotificationSink : ApiClientBase, ILogEventSink {
		public DiscordNotificationSink(IEnumerable<int> eventIds, String webHookId, String webHookToken) : base(new Uri($"https://discordapp.com/api/webhooks/{webHookId}/{webHookToken}")) {
			_eventIds = eventIds?.ToList() ?? new List<int>();
			_webHookId = webHookId;
			_webHookToken = webHookToken;
		}

		#region ILogEventSink
		public void Emit(LogEvent logEvent) {
			try {
				if (logEvent.Properties.ContainsKey("EventId")) {
					var eventId = JsonConvert.DeserializeObject<EventId>(logEvent.Properties["EventId"].ToString());

					if (_eventIds.Contains(eventId.Id)) {
						GetNodeResponseAsync("{\"content\": \"" + logEvent.RenderMessage().Replace('"', '\'') + "\"}", "application/json");

						SelfLog.WriteLine("Successfully forwarded event with Id '{0}' to WebHook with Id '{1}' and Token '{2}'", eventId.Id, _webHookId, _webHookToken);
					}
				}
			} catch (Exception e) {
				SelfLog.WriteLine("An exception occurred while sending to WebHook with Id '{0}' and Token '{1}': {2}", _webHookId, _webHookToken, e);
			}
		}
		#endregion

		private readonly IList<int> _eventIds;
		private readonly String _webHookId;
		private readonly String _webHookToken;
	}
}
