﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using nVeoPool.Common;
using nVeoPool.Common.Configuration;
using nVeoPool.Common.Routing;
using nVeoPool.Common.Rpc;
using nVeoPool.Data;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Configuration;
using nVeoPool.Data.Context;
using nVeoPool.Data.Repositories;
using nVeoPool.PoolServer.Banning;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Payment;
using nVeoPool.PoolServer.PoW;
using nVeoPool.PoolServer.Services;
using nVeoPool.PoolServer.WorkProtocol.GetWorkAmoveo;
using nVeoPool.PoolServer.WorkProtocol.Stratum;
using nVeoPool.PoolServer.WorkProtocol.Stratum.Messaging;

namespace nVeoPool.PoolServer {
	internal class KestrelStartup {
		public KestrelStartup() {
			_poolServerConfiguration = PoolServerConfiguration.Instance;
		}

		public void Configure(IApplicationBuilder app, IServiceProvider serviceProvider) {
			app.UseRouter(serviceProvider.GetService<IRouteConfigurator>().Configure);

			//Bit dirty but this is an easy way to capture a ready-to-use IServiceProvider, useful
			// in the Program class startup process...
			ServiceProvider = serviceProvider;
		}
		
		public void ConfigureServices(IServiceCollection services) {
			services.AddRouting();
			
			services.AddSingleton<IAddressParser, AddressParser>();
			services.AddSingleton<IBanManager, BanManager>();
			services.AddSingleton<IBanRepository, BanRepository>(provider => new BanRepository(new PostgresDatabaseContext(_poolServerConfiguration.DatabaseConfiguration.ConnectionString)));
			services.AddSingleton<IBlockRepository, BlockRepository>(provider => new BlockRepository(new PostgresDatabaseContext(_poolServerConfiguration.DatabaseConfiguration.ConnectionString)));
			services.AddSingleton<IConnectionFactory, RedisConnectionFactory>(provider => new RedisConnectionFactory(_poolServerConfiguration.CacheConfiguration.ConnectionString));
			services.AddSingleton<IDistributedCacheAdapter, DistributedCacheAdapter>();
			services.AddSingleton<IDistributedCacheKeyConfiguration, DistributedCacheKeyConfiguration>();
			services.AddSingleton<IExternalApiClient, ExternalApiClient>(provider => new ExternalApiClient(_poolServerConfiguration.DaemonClientConfiguration.LocalDaemonConfiguration.ExternalApiAddress));
			services.AddSingleton<IHostedService, BanManagementService>();
			services.AddSingleton<IHostedService, BlockCandidateProcessorService>();
			services.AddSingleton<IHostedService, CacheCleanupService>();
			services.AddSingleton<IHostedService, DifficultyCalculatorService>();
			services.AddSingleton<IHostedService, InvalidShareCleanupService>();
			services.AddSingleton<IHostedService, InvalidShareStorageService>();
			services.AddSingleton<IHostedService, NodeFailoverService>();
			services.AddSingleton<IHostedService, NodeSyncService>();
			services.AddSingleton<IHostedService, PaymentSenderService>();
			services.AddSingleton<IHostedService, PoolStatusService>();
			services.AddSingleton<IHostedService, ShareArchiverService>();
			services.AddSingleton<IHostedService, ShareStorageService>();
			services.AddSingleton<IHostedService, WorkRetrieverService>();
			services.AddSingleton<IInternalApiClient, InternalApiClient>(provider => new InternalApiClient(_poolServerConfiguration.DaemonClientConfiguration.LocalDaemonConfiguration.InternalApiAddress));
			services.AddSingleton<IPaymentRepository, PaymentRepository>(provider => new PaymentRepository(new PostgresDatabaseContext(_poolServerConfiguration.DatabaseConfiguration.ConnectionString)));
			services.AddSingleton<IPaymentStrategy, PplnsPaymentStrategy>();
			services.AddSingleton<IPoolServerConfiguration, PoolServerConfiguration>(provider => PoolServerConfiguration.Instance);
			services.AddSingleton<IPoWAlgorithm, AmoveoPoWAlgorithm>();
			services.AddSingleton<IRouteConfigurator, RouteConfigurator>();
			services.AddSingleton<IRpcClientAdapterFactory, RpcClientAdapterFactory>();
			services.AddSingleton<IServerConfiguration, PoolServerConfiguration>(provider => PoolServerConfiguration.Instance);
			services.AddSingleton<IShareRepository, ShareRepository>(provider => new ShareRepository(new PostgresDatabaseContext(_poolServerConfiguration.DatabaseConfiguration.ConnectionString)));
			services.AddSingleton<IStratumMessageParser, StratumMessageParser>();
			services.AddSingleton<IStratumServer, StratumServer>();
			services.AddSingleton<IWorkRequestHandler, WorkRequestHandler>();
		}

		internal static IServiceProvider ServiceProvider { get; private set; }

		private readonly IPoolServerConfiguration _poolServerConfiguration;
	}
}
