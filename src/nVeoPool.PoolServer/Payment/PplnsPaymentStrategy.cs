#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using nVeoPool.Data.Models;

namespace nVeoPool.PoolServer.Payment {
	internal class PplnsPaymentStrategy : IPaymentStrategy {
		#region IPaymentStrategy
		public IDictionary<String, decimal> CalculateRewardsForBlock(IEnumerable<Share> shares) {
			var shareArray = shares as Share[] ?? shares.ToArray();
			var minerGroups = shareArray.GroupBy(s => s.MinerPublicKey).ToList();
			var minerPaymentMap = new Dictionary<String, decimal>();
			var initialShareTime = shareArray.Last().SubmittedOn - 1;
			var roundDurationSeconds = (double)(shareArray.First().SubmittedOn - initialShareTime);

			var totalScore = 0m;
			foreach (var miner in minerGroups) {
				var totalMinerScore = 0m;

				foreach (var share in miner) {
					var shareMultiplier = (share.SubmittedOn - initialShareTime) / roundDurationSeconds;
					var shareScore = shareMultiplier * share.HashCount;
					
					totalMinerScore += (decimal)shareScore;
				}

				minerPaymentMap[miner.Key] = totalMinerScore;
				totalScore += totalMinerScore;
			}

			foreach (var miner in minerPaymentMap.Keys.ToList()) {
				minerPaymentMap[miner] /= totalScore;
			}
			
			return minerPaymentMap;
		}
		#endregion
	}
}