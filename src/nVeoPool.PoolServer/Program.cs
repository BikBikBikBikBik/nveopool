﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Serilog;
using Serilog.Events;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Notifications;
using nVeoPool.PoolServer.WorkProtocol.Stratum;

namespace nVeoPool.PoolServer {
	public class Program {
		public static async Task Main(String[] args) {
			_webHost = new WebHostBuilder()
				.UseStartup<KestrelStartup>()
				#if DEBUG
				.UseSerilog((hostingContext, loggerConfiguration) => {
					var loggerConfig = loggerConfiguration.MinimumLevel.Verbose().WriteTo.Console(LogEventLevel.Debug);

					SetHttpRequestLoggingFilter(loggerConfig);
				})
				#else
				.UseSerilog((hostingContext, loggerConfiguration) => {
					var loggerConfig = loggerConfiguration.MinimumLevel.Verbose().WriteTo.File(PoolServerConfiguration.Instance.LoggingConfiguration.FileName, PoolServerConfiguration.Instance.LoggingConfiguration.Level, buffered: true, flushToDiskInterval: TimeSpan.FromSeconds(30), outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{SourceContext}]{Scope}[{Level:u3}] {Message:lj}{NewLine}{Exception}", retainedFileCountLimit: 365, rollingInterval: RollingInterval.Day, rollOnFileSizeLimit: true);

					SetDiscordNotificationSink(loggerConfig);
					SetHttpRequestLoggingFilter(loggerConfig);
				})
				#endif
				.UseKestrel(options => {
					if (PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration != null) {
						options.AddServerHeader = false;
						options.Limits.KeepAliveTimeout = TimeSpan.FromSeconds(PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.KeepAliveTimeoutSeconds);
						options.Limits.MaxConcurrentConnections = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.MaximumConcurrentConnections;
						options.Limits.MaxRequestBodySize = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.MaximumRequestBodySizeBytes;
						options.Limits.MaxRequestBufferSize = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.MaximumRequestBufferSizeBytes;
						options.Limits.MaxRequestHeaderCount = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.MaximumRequestHeaderCount;
						options.Limits.MaxRequestHeadersTotalSize = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.MaximumRequestHeadersTotalSizeBytes;
						options.Limits.MaxRequestLineSize = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.MaximumRequestLineSizeBytes;
						options.Limits.MaxResponseBufferSize = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.MaximumResponseBufferSizeBytes;
						options.Limits.RequestHeadersTimeout = TimeSpan.FromSeconds(PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.RequestHeadersTimeoutSeconds);
						options.Listen(PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.ListenAddress, PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.ListenPort);
					}
				})
				.Build();
			_webHost.Start();

			if (PoolServerConfiguration.Instance.WorkProtocolConfiguration.StratumProtocolConfiguration != null) {
				var serviceProvider = KestrelStartup.ServiceProvider;
				_stratumServer = (IStratumServer)serviceProvider.GetService(typeof(IStratumServer));
				
				_stratumServer.Start(new System.Net.IPEndPoint(PoolServerConfiguration.Instance.WorkProtocolConfiguration.StratumProtocolConfiguration.ListenAddress, PoolServerConfiguration.Instance.WorkProtocolConfiguration.StratumProtocolConfiguration.ListenPort));
			}
			
			Console.CancelKeyPress += async (s,e) => await OnCancelKeyPress(s, e);
			Console.WriteLine($"{PoolServerConfiguration.Instance.ClusteringConfiguration.ServerType} pool server with id {PoolServerConfiguration.Instance.ClusteringConfiguration.ServerId} started...");
			Console.ReadLine();

			await Shutdown();
		}

		private static async Task OnCancelKeyPress(Object sender, ConsoleCancelEventArgs eventArgs) {
			await Shutdown();
		}

		private static void SetDiscordNotificationSink(LoggerConfiguration loggerConfiguration) {
			var discordConfigurationCount = PoolServerConfiguration.Instance.NotificationsConfiguration?.DiscordSinkConfigurations?.Count;

			if (discordConfigurationCount.HasValue && discordConfigurationCount.Value > 0) {
				foreach (var discordConfiguration in PoolServerConfiguration.Instance.NotificationsConfiguration.DiscordSinkConfigurations.Where(d => d.EventIds.Count > 0)) {
					loggerConfiguration = loggerConfiguration.WriteTo.DiscordNotificationSink(discordConfiguration.EventIds, discordConfiguration.WebHookId, discordConfiguration.WebHookToken);
				}
			}
		}

		private static void SetHttpRequestLoggingFilter(LoggerConfiguration loggerConfiguration) {
			if (PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration != null && !PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration.EnableRequestLogging) {
				loggerConfiguration.Filter.ByExcluding(e => e.Properties.TryGetValue("SourceContext", out var sourceContext) && sourceContext.ToString().Contains("Microsoft.AspNetCore"));
			}
		}

		private static async Task Shutdown() {
			var gracefulCloseWaitSeconds = PoolServerConfiguration.Instance.WorkProtocolConfiguration.GetWorkProtocolConfiguration?.MaximumGracefulCloseWaitSeconds ?? 0;

			try {
				_stratumServer?.Dispose();

				await _webHost.StopAsync(gracefulCloseWaitSeconds > 0 ? TimeSpan.FromSeconds(gracefulCloseWaitSeconds) : TimeSpan.MaxValue);

				_webHost.Dispose();
			} finally {
				Process.GetCurrentProcess().Close();
			}
		}

		private static IStratumServer _stratumServer;
		private static IWebHost _webHost;
	}
}
