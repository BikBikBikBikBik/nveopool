﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using nVeoPool.Common;
using nVeoPool.Common.Rpc;
using nVeoPool.Data;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Models;
using nVeoPool.PoolServer.Banning;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Notifications;
using nVeoPool.PoolServer.PoW;

namespace nVeoPool.PoolServer.WorkProtocol.GetWorkAmoveo {
	internal class WorkRequestHandler : IWorkRequestHandler {
		public WorkRequestHandler(IAddressParser addressParser, IBanManager banManager, IConnectionFactory connectionFactory, IDistributedCacheAdapter cache, ILogger<WorkRequestHandler> logger, IPoolServerConfiguration poolServerConfiguration, IPoWAlgorithm powAlgorithm, IRpcClientAdapterFactory rpcClientAdapterFactory) {
			_addressParser = addressParser;
			_banManager = banManager;
			_cache = cache;
			_connectionFactory = connectionFactory;
			_logger = logger;
			_poolServerConfiguration = poolServerConfiguration;
			_powAlgorithm = powAlgorithm;
			_rpcClientAdapterFactory = rpcClientAdapterFactory;
		}

		#region IWorkRequestHandler
		public async Task HandleWorkRequestAsync(HttpContext context) {
			var requestArray = JArray.Load(new JsonTextReader(new StreamReader(context.Request.Body)));
			if (requestArray.Any()) {
				var command = requestArray[0].Value<String>();
				var param1 = requestArray[1].Value<String>();
				var param2 = requestArray.Count > 2 ? requestArray[2].Value<String>() : null;

				if (command.Equals("mining_data") && requestArray.Count == 2 && _addressParser.ValidateMinerAddress(param1) && await _banManager.MinerIsAllowedAsync(_addressParser.ParseMinerAddress(param1).PublicKey)) {
					await HandleGetWorkRequestAsync(context, param1);
					return;
				}
				if (command.Equals("work") && requestArray.Count == 3 && _addressParser.ValidateMinerAddress(param2) && await _banManager.MinerIsAllowedAsync(_addressParser.ParseMinerAddress(param2).PublicKey)) {
					await HandleSubmitWorkRequestAsync(context, param1, param2);
					return;
				}
			}

			_logger.LogWarning("Invalid request {RequestBody} in {MethodName}", requestArray, nameof(HandleWorkRequestAsync));

			context.Response.ContentLength = 0;
			context.Response.StatusCode = 500;

			await context.Response.WriteAsync(String.Empty);
		}
		#endregion

		private async Task HandleGetWorkRequestAsync(HttpContext context, String minerAddress) {
			var responseSent = false;

			try {
				var formattedMinerAddress = _addressParser.FormatMinerAddressForCacheKey(minerAddress);
				var currentJobDifficultyTask = _cache.GetWorkerCurrentJobDifficultyAsync(formattedMinerAddress);
				var miningData = await _cache.GetNodeMiningDataAsync();

				if (!String.IsNullOrWhiteSpace(miningData.BlockHash)) {
					_cache.UpdateWorkerHeartbeatImmediate(formattedMinerAddress);
					
					var currentJobDifficulty = await currentJobDifficultyTask;
					if (!currentJobDifficulty.HasValue) {
						currentJobDifficulty = _poolServerConfiguration.VarDiffConfiguration.InitialDifficulty;
						_cache.UpdateWorkerCurrentJobDifficultyImmediate(formattedMinerAddress, currentJobDifficulty.Value);
					}
					var clientMiningData = $"[\"ok\",[{miningData.FieldZero},\"{miningData.BlockHash}\",{miningData.BlockDifficulty},{currentJobDifficulty.Value}]]";

					context.Response.ContentLength = clientMiningData.Length;
					context.Response.ContentType = "application/octet-stream";
					await context.Response.WriteAsync(clientMiningData);

					responseSent = true;
				} else {
					_logger.LogWarning("Cache returned null mining data in {MethodName}", nameof(HandleGetWorkRequestAsync));
				}
			} catch (Exception e) {
				_logger.LogError(e, "Exception during {MethodName} execution", nameof(HandleGetWorkRequestAsync));
			}

			if (!responseSent) {
				context.Response.ContentLength = 0;
				context.Response.StatusCode = 500;
				await context.Response.WriteAsync(String.Empty);
			}
		}

		private async Task HandleSubmitWorkRequestAsync(HttpContext context, String nonce, String minerAddress) {
			var responseSent = false;
			String responseString = null;

			try {
				var formattedMinerAddress = _addressParser.FormatMinerAddressForCacheKey(minerAddress);
				var formattedMinerPublicKey = _addressParser.ParseMinerAddress(formattedMinerAddress).PublicKey;
				var miningDataTask = _cache.GetNodeMiningDataAsync();
				var currentJobDifficultyTask = _cache.GetWorkerCurrentJobDifficultyAsync(formattedMinerAddress);
				var submittedOn = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
				_cache.UpdateWorkerHeartbeatImmediate(formattedMinerAddress);

				if (!await _cache.MinerShareIsDuplicateAsync(formattedMinerPublicKey, nonce)) {
					var miningData = await miningDataTask;

					if (!String.IsNullOrWhiteSpace(miningData.BlockHash)) {
						var maybeCurrentJobDifficulty = await currentJobDifficultyTask;
						
						if (!maybeCurrentJobDifficulty.HasValue && _poolServerConfiguration.WorkProtocolConfiguration.RequireSubscribeBeforeSubmit) {
							_logger.LogWarning("Cache returned null difficulty for {MinerAddress}", minerAddress);
						} else {
							var currentJobDifficulty = maybeCurrentJobDifficulty ?? _poolServerConfiguration.VarDiffConfiguration.InitialDifficulty;
							var powValidationResult = _powAlgorithm.ValidatePoW(nonce, miningData.BlockHash, miningData.BlockDifficulty, currentJobDifficulty);

							if (powValidationResult.IsValid) {
								var isBlockCandidate = powValidationResult.Difficulty > miningData.BlockDifficulty;
								var isRejectedBlockCandidate = false;

								if (isBlockCandidate) {
									if (await _rpcClientAdapterFactory.GetClientAdapter(miningData.NodeClientId).SubmitWorkAsync(nonce)) {
										_logger.LogInformation(GlobalEventList.Blocks.BlockCandidateAcceptedByNode, "Block candidate {Nonce} for {BlockNumber} with difficulty {ShareDifficulty} from {MinerAddress}", nonce, miningData.CurrentHeight + 1, powValidationResult.Difficulty, minerAddress);
									} else {
										_logger.LogWarning("Node rejected block candidate {Nonce} with difficulty {ShareDifficulty} from {MinerAddress}, retrying submit", nonce, powValidationResult.Difficulty, minerAddress);

										if (await _rpcClientAdapterFactory.GetClientAdapter(miningData.NodeClientId).SubmitWorkAsync(nonce)) {
											_logger.LogInformation(GlobalEventList.Blocks.BlockCandidateAcceptedByNode, "Block candidate {Nonce} for {BlockNumber} with difficulty {ShareDifficulty} from {MinerAddress}", nonce, miningData.CurrentHeight + 1, powValidationResult.Difficulty, minerAddress);
										} else {
											_logger.LogWarning(GlobalEventList.Blocks.BlockCandidateRejectedByNode, "Node rejected block candidate {Nonce} with difficulty {ShareDifficulty} from {MinerAddress}", nonce, powValidationResult.Difficulty, minerAddress);

											isRejectedBlockCandidate = true;
										}
									}

									responseString = RESPONSE_BLOCK_CANDIDATE;
								} else {
									_logger.LogInformation("Valid share {Nonce} from {MinerAddress}", nonce, minerAddress);
								}

								var updateWorkerRecentSharesTask = _cache.UpdateWorkerRecentSharesAsync(formattedMinerAddress, formattedMinerPublicKey, nonce, submittedOn);
								_connectionFactory.GetSubscriber().PublishImmediate(_poolServerConfiguration.ChannelConfiguration.ShareFound(_poolServerConfiguration.ClusteringConfiguration.ServerId), new Share {
									BlockDifficulty = miningData.BlockDifficulty,
									BlockHash = miningData.BlockHash,
									BlockHeight = miningData.CurrentHeight,
									IsBlockCandidate = isBlockCandidate,
									IsRejectedBlockCandidate = isRejectedBlockCandidate,
									JobDifficulty = currentJobDifficulty,
									MinerAddress = minerAddress,
									Nonce = nonce,
									SubmittedOn = submittedOn
								});

								responseString = responseString ?? RESPONSE_VALID_SHARE;
								
								context.Response.ContentLength = responseString.Length;
								await context.Response.WriteAsync(responseString);
								await updateWorkerRecentSharesTask;

								responseSent = true;
							} else {
								_logger.LogWarning("Invalid share {Nonce} from {MinerAddress}", nonce, minerAddress);

								_connectionFactory.GetSubscriber().PublishImmediate(_poolServerConfiguration.ChannelConfiguration.InvalidShareFound(_poolServerConfiguration.ClusteringConfiguration.ServerId), new Share {
									IsDuplicate = false,
									MinerAddress = minerAddress,
									SubmittedOn = submittedOn
								});

								responseString = powValidationResult.Difficulty == 0 ? RESPONSE_INVALID_SHARE : RESPONSE_LOW_DIFFICULTY_SHARE;
							}
						}
					} else {
						_logger.LogWarning("Cache returned null mining data in {MethodName}", nameof(HandleSubmitWorkRequestAsync));
					}
				} else {
					_logger.LogWarning("Duplicate share {Nonce} from {MinerAddress}", nonce, minerAddress);

					_connectionFactory.GetSubscriber().PublishImmediate(_poolServerConfiguration.ChannelConfiguration.InvalidShareFound(_poolServerConfiguration.ClusteringConfiguration.ServerId), new Share {
						IsDuplicate = true,
						MinerAddress = minerAddress,
						SubmittedOn = submittedOn
					});

					responseString = RESPONSE_INVALID_SHARE;
				}
			} catch (Exception e) {
				_logger.LogError(e, "Exception during {MethodName} execution", nameof(HandleSubmitWorkRequestAsync));
			}

			if (!responseSent) {
				var response = responseString ?? String.Empty;
				context.Response.ContentLength = response.Length;
				context.Response.StatusCode = response.Length > 0 ? 200 : 500;

				await context.Response.WriteAsync(response);
			}
		}

		private readonly IAddressParser _addressParser;
		private readonly IBanManager _banManager;
		private readonly IConnectionFactory _connectionFactory;
		private readonly IDistributedCacheAdapter _cache;
		private readonly ILogger<WorkRequestHandler> _logger;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private readonly IPoWAlgorithm _powAlgorithm;
		private readonly IRpcClientAdapterFactory _rpcClientAdapterFactory;
		private static readonly String RESPONSE_BLOCK_CANDIDATE = $"[-6,{String.Join(",", Encoding.ASCII.GetBytes("found block"))}]";
		private static readonly String RESPONSE_INVALID_SHARE = $"[-6,{String.Join(",", Encoding.ASCII.GetBytes("invalid work"))}]";
		private static readonly String RESPONSE_LOW_DIFFICULTY_SHARE = $"[-6,{String.Join(",", Encoding.ASCII.GetBytes("low diff"))}]";
		private static readonly String RESPONSE_VALID_SHARE = $"[-6,{String.Join(",", Encoding.ASCII.GetBytes("found work"))}]";
	}
}
