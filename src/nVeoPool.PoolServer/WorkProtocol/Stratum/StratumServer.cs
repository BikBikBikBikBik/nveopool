#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Reactive.Concurrency;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using nVeoPool.Common;
using nVeoPool.Common.Rpc;
using nVeoPool.Data;
using nVeoPool.Data.Caching;
using nVeoPool.PoolServer.Banning;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Models;
using nVeoPool.PoolServer.PoW;
using nVeoPool.PoolServer.WorkProtocol.Stratum.Messaging;
using nVeoPool.TcpServer;
using nVeoPool.TcpServer.Events;

namespace nVeoPool.PoolServer.WorkProtocol.Stratum {
	internal class StratumServer : TcpServerBase, IStratumServer {
		public StratumServer(IAddressParser addressParser, IBanManager banManager, IDistributedCacheAdapter cache, IConnectionFactory connectionFactory, ILogger<StratumServer> logger, ILoggerFactory loggerFactory, IPoolServerConfiguration poolServerConfiguration, IPoWAlgorithm powAlgorithm, IRpcClientAdapterFactory rpcClientAdapterFactory, IStratumMessageParser stratumMessageParser) : base(logger, new EventLoopScheduler(t => new Thread(t) { IsBackground = false } )) {
			_addressParser = addressParser;
			_banManager = banManager;
			_cache = cache;
			_cancellationTokenSource = new CancellationTokenSource();
			_connectionFactory = connectionFactory;
			_loggerFactory = loggerFactory;
			_poolServerConfiguration = poolServerConfiguration;
			_powAlgorithm = powAlgorithm;
			_rpcClientAdapterFactory = rpcClientAdapterFactory;
			_stratumMessageParser = stratumMessageParser;
			_workIsAvailable = true;

			EventStream.Subscribe(e => Task.Run(async () => await HandleEventAsync(e)), HandleError);

			_connectionFactory.GetSubscriber().SubscribeImmediate<Work>(_poolServerConfiguration.ChannelConfiguration.NewMiningJob, OnNewWorkMessageReceived);
			_connectionFactory.GetSubscriber().SubscribeImmediate<WorkerDifficulty>(_poolServerConfiguration.ChannelConfiguration.WorkerDifficultyChanged, OnDifficultyChangedMessageReceived);
			_connectionFactory.GetSubscriber().SubscribeImmediate<long>(_poolServerConfiguration.ChannelConfiguration.WorkUnavailable, OnWorkUnavailbleMessageReceived);

			StartRemoveInactiveClientsLoop(_cancellationTokenSource.Token);
		}

		#region IDisposable
		public void Dispose() {
			_cancellationTokenSource.Cancel();

			Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion

		#region TcpServerBase
		protected override TcpClientBase CreateClient(ulong clientId, Socket socket) {
			return new StratumClient(_addressParser, _banManager, _cache, _connectionFactory, clientId, _loggerFactory.CreateLogger<StratumClient>(), _poolServerConfiguration, _powAlgorithm, _rpcClientAdapterFactory, socket, _stratumMessageParser);
		}
		#endregion

		private void HandleError(Exception ex) {
			_logger.LogError(ex, "Exception during {ServerType} execution", nameof(StratumServer));
		}

		private async Task HandleEventAsync(IServerEvent @event) {
			switch (@event) {
				case ClientConnectedEvent clientConnectedEvent:
					_logger.LogInformation("{ServerType} accepted new client with id {ClientId}", nameof(StratumServer), clientConnectedEvent.Client.Id);
				break;

				case ClientDisconnectedEvent clientDisconnectedEvent:
					_logger.LogInformation("Client with id {ClientId} disconnected from {ServerType}", clientDisconnectedEvent.Client.Id, nameof(StratumServer));
				break;

				case DataReceivedEvent dataReceivedEvent:
					var client = (StratumClient)dataReceivedEvent.Client;
					var serverMessages = await client.ProcessMessageAsync(dataReceivedEvent.Buffer);

					SendMessage(client, serverMessages);
				break;
			}
		}

		private void OnDifficultyChangedMessageReceived(String channelName, WorkerDifficulty workerDifficulty) {
			var desiredClient = Clients.Cast<StratumClient>().SingleOrDefault(c => workerDifficulty.FormattedMinerAddress.Equals(c.FormattedLogin));

			if (desiredClient != null) {
				var newJob = desiredClient.UpdateCurrentDifficulty(workerDifficulty.NewDifficulty);

				if (newJob != null && _workIsAvailable) {
					SendMessage(desiredClient, new [] {new StratumServerMessage {
						Method = StratumMethodType.Job,
						Result = newJob
					}});
				}
			}
		}

		private void OnNewWorkMessageReceived(String channelName, Work newWork) {
			foreach (var client in Clients.Cast<StratumClient>().Where(c => !String.IsNullOrWhiteSpace(c.FormattedLogin))) {
				var newJob = client.UpdateCurrentJob(newWork);

				if (newJob != null) {
					SendMessage(client, new [] {new StratumServerMessage {
						Method = StratumMethodType.Job,
						Result = newJob
					}});
				}
			}

			_workIsAvailable = true;
		}

		private void OnWorkUnavailbleMessageReceived(String channelName, long lastAvailableWorkReceivedOn) {
			if (_workIsAvailable) {
				_workIsAvailable = false;

				foreach (var client in Clients.Cast<StratumClient>().Where(c => !String.IsNullOrWhiteSpace(c.FormattedLogin))) {
					SendMessage(client, new [] {new StratumServerMessage {
						Error = new StratumError { ErrorCode = StratumErrorType.NoWorkAvailable },
						Method = StratumMethodType.Job
					}});
				}
			}
		}

		private void SendMessage(StratumClient client, IEnumerable<StratumServerMessage> messages) {
			if (client.IsConnected) {
				var serializedMessages = messages.Select(m => JsonConvert.SerializeObject(m, _jsonSerializerSettings));
				var serializedMessageString = String.Join('\n', serializedMessages);

				try {
					Send(client, Encoding.UTF8.GetBytes($"{serializedMessageString}\n"));
				} catch (Exception e) {
					_logger.LogError(e, "Exception during {ServerType} execution while sending message {ServerMessage} to client {ClientId}", nameof(StratumServer), serializedMessageString, client.Id);
				}
			} else {
				_logger.LogWarning("Attempted to send to disconnected client {ClientId}: {MessageCount}", client.Id, messages.Count());
			}
		}

		private void StartRemoveInactiveClientsLoop(CancellationToken cancellationToken) {
			if (_poolServerConfiguration.WorkProtocolConfiguration.StratumProtocolConfiguration.IdleClientTimeoutSeconds > 0) {
				Task.Run(async () => {
					while (!cancellationToken.IsCancellationRequested) {
						var cutoffTime = new DateTimeOffset(DateTime.UtcNow - TimeSpan.FromSeconds(_poolServerConfiguration.WorkProtocolConfiguration.StratumProtocolConfiguration.IdleClientTimeoutSeconds)).ToUnixTimeSeconds();

						foreach (var client in Clients.Cast<StratumClient>().Where(c => c.LastSubmittedOn > 0 && c.LastSubmittedOn < cutoffTime)) {
							_logger.LogInformation("Disconnecting idle client with id {ClientId} in {ServerType}", client.Id, nameof(StratumServer));

							try {
								client.Socket.Close(_poolServerConfiguration.WorkProtocolConfiguration.StratumProtocolConfiguration.MaximumGracefulIdleDisconnectWaitSeconds);
							} catch (Exception e) {
								_logger.LogError(e, "Exception in {ServerType} while disconnecting client with id {ClientId}", nameof(StratumServer), client.Id);
							}
						}

						await Task.Delay(TimeSpan.FromSeconds(_poolServerConfiguration.WorkProtocolConfiguration.StratumProtocolConfiguration.IdleClientTimeoutSeconds), cancellationToken);
					}
				}, cancellationToken);
			} else {
				_logger.LogInformation("Disabled idle client disconnect in {ServerType}", nameof(StratumServer));
			}
		}

		private readonly IAddressParser _addressParser;
		private readonly IBanManager _banManager;
		private readonly IDistributedCacheAdapter _cache;
		private readonly CancellationTokenSource _cancellationTokenSource;
		private readonly IConnectionFactory _connectionFactory;
		private readonly ILoggerFactory _loggerFactory;
		private static readonly JsonSerializerSettings _jsonSerializerSettings = new JsonSerializerSettings {
			ContractResolver = new CamelCasePropertyNamesContractResolver(),
			NullValueHandling = NullValueHandling.Ignore
		};
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private readonly IPoWAlgorithm _powAlgorithm;
		private readonly IRpcClientAdapterFactory _rpcClientAdapterFactory;
		private readonly IStratumMessageParser _stratumMessageParser;
		private bool _workIsAvailable;
	}
}