#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using nVeoPool.Common;
using nVeoPool.Common.Rpc;
using nVeoPool.Data;
using nVeoPool.Data.Caching;
using nVeoPool.Data.Models;
using nVeoPool.PoolServer.Banning;
using nVeoPool.PoolServer.Configuration;
using nVeoPool.PoolServer.Models;
using nVeoPool.PoolServer.Notifications;
using nVeoPool.PoolServer.PoW;
using nVeoPool.PoolServer.WorkProtocol.Stratum.Messaging;
using nVeoPool.TcpServer;

namespace nVeoPool.PoolServer.WorkProtocol.Stratum {
	internal class StratumClient : TcpClientBase {
		public StratumClient(IAddressParser addressParser, IBanManager banManager, IDistributedCacheAdapter cache, IConnectionFactory connectionFactory, ulong id, ILogger<StratumClient> logger, IPoolServerConfiguration poolServerConfiguration, IPoWAlgorithm powAlgorithm, IRpcClientAdapterFactory rpcClientAdapterFactory, Socket socket, IStratumMessageParser stratumMessageParser) : base(id, socket) {
			_addressParser = addressParser;
			_banManager = banManager;
			_cache = cache;
			_connectionFactory = connectionFactory;
			FormattedLogin = String.Empty;
			_logger = logger;
			_poolServerConfiguration = poolServerConfiguration;
			_powAlgorithm = powAlgorithm;
			_rpcClientAdapterFactory = rpcClientAdapterFactory;
			_stratumMessageParser = stratumMessageParser;
		}

		public String FormattedLogin { get; private set; }

		public long LastSubmittedOn { get; private set; }

		public async Task<IEnumerable<StratumServerMessage>> ProcessMessageAsync(byte[] messageBytes) {
			var returnMessages = new List<StratumServerMessage>();

			try {
				var clientMessages = _stratumMessageParser.ParseMessage(messageBytes);
				foreach (var clientMessage in clientMessages) {
					if (clientMessage.Parameters == null) {
						returnMessages.Add(new StratumServerMessage {
							Error = new StratumError { ErrorCode = StratumErrorType.Generic, Message = MALFORMED_CLIENT_MESSAGE_ERROR_MESSAGE }
						});

						continue;
					}

					switch (clientMessage.Method) {
						case StratumMethodType.Submit:
							returnMessages.Add(await HandleSubmitAsync(clientMessage));
						break;

						case StratumMethodType.Subscribe:
							returnMessages.Add(await HandleSubscribeAsync(clientMessage));
						break;

						default:
							_logger.LogError("Error while attempting to parse client method in {Type}", nameof(StratumClient));

							returnMessages.Add(new StratumServerMessage {
								Error = new StratumError { ErrorCode = StratumErrorType.Generic, Message = MALFORMED_CLIENT_MESSAGE_ERROR_MESSAGE }
							});
						break;
					}
				}
			} catch (Exception e) {
				var byteString = messageBytes != null ? Encoding.UTF8.GetString(messageBytes) : "[NULL]";
				_logger.LogError(e, "Malformed message from client in {Type}: {ByteString}", nameof(StratumClient), byteString);
			}

			return returnMessages;
		}

		public StratumServerMessageResult UpdateCurrentDifficulty(int newDifficulty) {
			if (newDifficulty != _currentJob.JobDifficulty) {
				_currentJob = new StratumServerMessageResult {
					BlockHash = _currentJob.BlockHash,
					JobDifficulty = newDifficulty,
				};

				return _currentJob;
			}

			return null;
		}

		public StratumServerMessageResult UpdateCurrentJob(Work newWork) {
			if (!newWork.BlockHash.Equals(_currentJob.BlockHash)) {
				_currentJob = new StratumServerMessageResult {
					BlockHash = newWork.BlockHash,
					JobDifficulty = _currentJob.JobDifficulty,
				};

				return _currentJob;
			}

			return null;
		}

		private async Task<StratumServerMessage> HandleSubmitAsync(StratumClientMessage clientMessage) {
			if (!_addressParser.ValidateMinerAddress(clientMessage.Parameters.Id)) {
				return new StratumServerMessage {
					Id = clientMessage.Id,
					Error = new StratumError { ErrorCode = StratumErrorType.InvalidAddress }
				};
			}

			var formattedMinerAddress = _addressParser.FormatMinerAddressForCacheKey(clientMessage.Parameters.Id);
			var formattedMinerPublicKey = _addressParser.ParseMinerAddress(formattedMinerAddress).PublicKey;
			
			if (!await _banManager.MinerIsAllowedAsync(formattedMinerPublicKey)) {
				return new StratumServerMessage {
					Id = clientMessage.Id,
					Error = new StratumError { ErrorCode = StratumErrorType.Banned }
				};
			}

			var miningDataTask = _cache.GetNodeMiningDataAsync();
			var currentJobDifficultyTask = _cache.GetWorkerCurrentJobDifficultyAsync(formattedMinerAddress);
			var submittedOn = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
			_cache.UpdateWorkerHeartbeatImmediate(formattedMinerAddress);

			if (await _cache.MinerShareIsDuplicateAsync(formattedMinerPublicKey, clientMessage.Parameters.Nonce)) {
				_logger.LogWarning("Duplicate share {Nonce} from {MinerAddress}", clientMessage.Parameters.Nonce, clientMessage.Parameters.Id);

				_connectionFactory.GetSubscriber().PublishImmediate(_poolServerConfiguration.ChannelConfiguration.InvalidShareFound(_poolServerConfiguration.ClusteringConfiguration.ServerId), new Share {
					IsDuplicate = true,
					MinerAddress = clientMessage.Parameters.Id,
					SubmittedOn = submittedOn
				});

				return new StratumServerMessage {
					Id = clientMessage.Id,
					Error = new StratumError { ErrorCode = StratumErrorType.DuplicateShare }
				};
			}

			var miningData = await miningDataTask;
			if (String.IsNullOrWhiteSpace(miningData.BlockHash)) {
				_logger.LogWarning("Cache returned null mining data in {MethodName}", nameof(HandleSubmitAsync));

				return new StratumServerMessage {
					Id = clientMessage.Id,
					Error = new StratumError { ErrorCode = StratumErrorType.NoWorkAvailable }
				};
			}

			var maybeCurrentJobDifficulty = await currentJobDifficultyTask;
			if (!maybeCurrentJobDifficulty.HasValue && _poolServerConfiguration.WorkProtocolConfiguration.RequireSubscribeBeforeSubmit) {
				_logger.LogWarning("Cache returned null difficulty for {MinerAddress}", clientMessage.Parameters.Id);

				return new StratumServerMessage {
					Id = clientMessage.Id,
					Error = new StratumError { ErrorCode = StratumErrorType.Generic }
				};
			}

			var currentJobDifficulty = maybeCurrentJobDifficulty ?? _poolServerConfiguration.VarDiffConfiguration.InitialDifficulty;
			var powValidationResult = _powAlgorithm.ValidatePoW(clientMessage.Parameters.Nonce, miningData.BlockHash, miningData.BlockDifficulty, currentJobDifficulty);

			if (!powValidationResult.IsValid) {
				_logger.LogWarning("Invalid share {Nonce} from {MinerAddress}", clientMessage.Parameters.Nonce, clientMessage.Parameters.Id);

				_connectionFactory.GetSubscriber().PublishImmediate(_poolServerConfiguration.ChannelConfiguration.InvalidShareFound(_poolServerConfiguration.ClusteringConfiguration.ServerId), new Share {
					IsDuplicate = false,
					MinerAddress = clientMessage.Parameters.Id,
					SubmittedOn = submittedOn
				});

				return new StratumServerMessage {
					Id = clientMessage.Id,
					Error = new StratumError { ErrorCode = StratumErrorType.LowDifficultyShare }
				};
			}

			var isBlockCandidate = powValidationResult.Difficulty > miningData.BlockDifficulty;
			var isRejectedBlockCandidate = false;

			if (isBlockCandidate) {
				if (await _rpcClientAdapterFactory.GetClientAdapter(miningData.NodeClientId).SubmitWorkAsync(clientMessage.Parameters.Nonce)) {
					_logger.LogInformation(GlobalEventList.Blocks.BlockCandidateAcceptedByNode, "Block candidate {Nonce} for {BlockNumber} with difficulty {ShareDifficulty} from {MinerAddress}", clientMessage.Parameters.Nonce, miningData.CurrentHeight + 1, powValidationResult.Difficulty, clientMessage.Parameters.Id);
				} else {
					_logger.LogWarning("Node rejected block candidate {Nonce} with difficulty {ShareDifficulty} from {MinerAddress}, retrying submit", clientMessage.Parameters.Nonce, powValidationResult.Difficulty, clientMessage.Parameters.Id);

					if (await _rpcClientAdapterFactory.GetClientAdapter(miningData.NodeClientId).SubmitWorkAsync(clientMessage.Parameters.Nonce)) {
						_logger.LogInformation(GlobalEventList.Blocks.BlockCandidateAcceptedByNode, "Block candidate {Nonce} for {BlockNumber} with difficulty {ShareDifficulty} from {MinerAddress}", clientMessage.Parameters.Nonce, miningData.CurrentHeight + 1, powValidationResult.Difficulty, clientMessage.Parameters.Id);
					} else {
						_logger.LogWarning(GlobalEventList.Blocks.BlockCandidateRejectedByNode, "Node rejected block candidate {Nonce} with difficulty {ShareDifficulty} from {MinerAddress}", clientMessage.Parameters.Nonce, powValidationResult.Difficulty, clientMessage.Parameters.Id);

						isRejectedBlockCandidate = true;
					}
				}
			} else {
				_logger.LogInformation("Valid share {Nonce} from {MinerAddress}", clientMessage.Parameters.Nonce, clientMessage.Parameters.Id);
			}

			var updateWorkerRecentSharesTask = _cache.UpdateWorkerRecentSharesAsync(formattedMinerAddress, formattedMinerPublicKey, clientMessage.Parameters.Nonce, submittedOn);
			_connectionFactory.GetSubscriber().PublishImmediate(_poolServerConfiguration.ChannelConfiguration.ShareFound(_poolServerConfiguration.ClusteringConfiguration.ServerId), new Share {
				BlockDifficulty = miningData.BlockDifficulty,
				BlockHash = miningData.BlockHash,
				BlockHeight = miningData.CurrentHeight,
				IsBlockCandidate = isBlockCandidate,
				IsRejectedBlockCandidate = isRejectedBlockCandidate,
				JobDifficulty = currentJobDifficulty,
				MinerAddress = clientMessage.Parameters.Id,
				Nonce = clientMessage.Parameters.Nonce,
				SubmittedOn = submittedOn
			});

			await updateWorkerRecentSharesTask;

			LastSubmittedOn = submittedOn;

			return new StratumServerMessage {
				Id = clientMessage.Id,
				Result = new StratumServerMessageResult { Accepted = 1 }
			};
		}

		private async Task<StratumServerMessage> HandleSubscribeAsync(StratumClientMessage clientMessage) {
			if (!_addressParser.ValidateMinerAddress(clientMessage.Parameters.Id)) {
				return new StratumServerMessage {
					Error = new StratumError { ErrorCode = StratumErrorType.InvalidAddress }
				};
			}

			var miningDataTask = _cache.GetNodeMiningDataAsync();
			var formattedMinerAddress = _addressParser.FormatMinerAddressForCacheKey(clientMessage.Parameters.Id);

			if (!await _banManager.MinerIsAllowedAsync(_addressParser.ParseMinerAddress(formattedMinerAddress).PublicKey)) {
				return new StratumServerMessage {
					Id = clientMessage.Id,
					Error = new StratumError { ErrorCode = StratumErrorType.Banned }
				};
			}

			var currentJobDifficulty = await _cache.GetWorkerCurrentJobDifficultyAsync(formattedMinerAddress);
			if (!currentJobDifficulty.HasValue) {
				currentJobDifficulty = _poolServerConfiguration.VarDiffConfiguration.InitialDifficulty;
				_cache.UpdateWorkerCurrentJobDifficultyImmediate(formattedMinerAddress, _poolServerConfiguration.VarDiffConfiguration.InitialDifficulty);
			}

			_currentJob = new StratumServerMessageResult { JobDifficulty = currentJobDifficulty };

			FormattedLogin = formattedMinerAddress;

			_logger.LogInformation("Subscribed miner {MinerAddress} in {Type} with id {ClientId}", clientMessage.Parameters.Id, nameof(StratumClient), Id);
			
			var miningData = await miningDataTask;
			if (String.IsNullOrWhiteSpace(miningData.BlockHash)) {
				_logger.LogWarning("Cache returned null mining data in {MethodName}", nameof(HandleSubscribeAsync));

				return new StratumServerMessage {
					Id = clientMessage.Id,
					Error = new StratumError { ErrorCode = StratumErrorType.NoWorkAvailable }
				};
			}

			_currentJob.BlockHash = miningData.BlockHash;

			_cache.UpdateWorkerHeartbeatImmediate(formattedMinerAddress);
			
			return new StratumServerMessage {
				Id = clientMessage.Id,
				Result = _currentJob
			};
		}

		private const String MALFORMED_CLIENT_MESSAGE_ERROR_MESSAGE = "Malformed client message received";
		private readonly IAddressParser _addressParser;
		private readonly IBanManager _banManager;
		private readonly IDistributedCacheAdapter _cache;
		private readonly IConnectionFactory _connectionFactory;
		private StratumServerMessageResult _currentJob;
		private readonly ILogger<StratumClient> _logger;
		private readonly IPoolServerConfiguration _poolServerConfiguration;
		private readonly IPoWAlgorithm _powAlgorithm;
		private readonly IRpcClientAdapterFactory _rpcClientAdapterFactory;
		private readonly IStratumMessageParser _stratumMessageParser;
	}
}