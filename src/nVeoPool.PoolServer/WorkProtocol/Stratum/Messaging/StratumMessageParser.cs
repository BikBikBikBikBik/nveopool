#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace nVeoPool.PoolServer.WorkProtocol.Stratum.Messaging {
	internal class StratumMessageParser : IStratumMessageParser {
		public StratumMessageParser(ILogger<StratumMessageParser> logger) {
			_logger = logger;
		}

		#region IStratumMessageParser
		public IEnumerable<StratumClientMessage> ParseMessage(byte[] messageBytes) {
			try {
				return ParseMessageBytes(messageBytes).ToList();
			} catch (JsonReaderException) {
				var byteString = messageBytes != null ? Encoding.UTF8.GetString(messageBytes) : null;

				if (TryParseMessageString(byteString, out var clientMessage)) {
					return new List<StratumClientMessage> { clientMessage };
				}

				throw;
			}
		}
		#endregion

		private IEnumerable<StratumClientMessage> ParseMessageBytes(byte[] messageBytes) {
			using (var stream = new MemoryStream(messageBytes)) {
				using (var streamReader = new StreamReader(stream, Encoding.UTF8)) {
					using (var jsonReader = new JsonTextReader(streamReader) { SupportMultipleContent = true }) {
						var serializer = new JsonSerializer();

						while (jsonReader.Read()) {
							yield return serializer.Deserialize<StratumClientMessage>(jsonReader);
						}
					}
				}
			}
		}

		private bool TryParseMessageString(String byteString, out StratumClientMessage clientMessage) {
			clientMessage = null;
			if (String.IsNullOrWhiteSpace(byteString)) {
				return false;
			}

			var closingBracesIndex = byteString.IndexOf("}}");
			if (closingBracesIndex + 2 < byteString.Length) {
				var jsonBeforeClosingBraces = byteString.Substring(0, closingBracesIndex + 2);

				_logger.LogWarning("Attempting to parse {JsonString} in {Type} from original input {ByteString}", jsonBeforeClosingBraces, nameof(StratumMessageParser), byteString);

				try {
					clientMessage = JsonConvert.DeserializeObject<StratumClientMessage>(jsonBeforeClosingBraces);

					return true;
				} catch (Exception e) {
					_logger.LogError(e, "{Type} failed to parse {JsonString}", nameof(StratumMessageParser), jsonBeforeClosingBraces);
				}
			}

			return false;
		}

		private readonly ILogger<StratumMessageParser> _logger;
	}
}