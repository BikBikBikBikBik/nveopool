using System;
using System.Security.Cryptography;
using Microsoft.Extensions.Logging;

namespace nVeoPool.PoolServer.PoW {
	internal class AmoveoPoWAlgorithm : IPoWAlgorithm {
		public AmoveoPoWAlgorithm(ILogger<AmoveoPoWAlgorithm> logger) {
			_logger = logger;
		}

		#region IPoWAlgorithm
		public double GetHashCountForDifficulty(int difficulty) {
			return GetHashCountForDifficulty((uint)difficulty);
		}

		public double GetHashCountForDifficulty(uint difficulty) {
			var a = difficulty / 256;
			var b = difficulty % 256;

			return Math.Pow(2, a) * (256 + b) / 256;
		}

		public (uint Difficulty, bool IsValid) ValidatePoW(String nonce, String blockHash, int blockDifficulty, int jobDifficulty) {
			try {
				var nonceBytes = Convert.FromBase64String(nonce);
				var blockHashBytes = Convert.FromBase64String(blockHash);
				var text = new byte[55];

				for (var i = 0; i < 32; i++) {
					text[i] = blockHashBytes[i];
				}
				for (var i = 0; i < 23; i++) {
					text[i + 32] = nonceBytes[i];
				}

				using(var hasher = SHA256.Create()) {
					var hash = hasher.ComputeHash(text);
					var difficulty = HashToInteger(hash);

					return (difficulty, difficulty >= jobDifficulty);
				}
			} catch (Exception e) {
				_logger.LogError(e, "Exception during {MethodName} execution", nameof(ValidatePoW));

				return (0, false);
			}
		}
		#endregion

		private uint HashToInteger(byte[] h) {
			uint x = 0;
			var y = new uint[2];

			for (int i = 0; i < 31; i++) {
				if (h[i] == 0) {
					x += 8;
					y[1] = h[i+1];

					continue;
				} else if (h[i] < 2) {
					x += 7;
					y[1] = (h[i] * 128u) + (h[i+1] / 2u);
				} else if (h[i] < 4) {
					x += 6;
					y[1] = (h[i] * 64u) + (h[i+1] / 4u);
				} else if (h[i] < 8) {
					x += 5;
					y[1] = (h[i] * 32u) + (h[i+1] / 8u);
				} else if (h[i] < 16) {
					x += 4;
					y[1] = (h[i] * 16u) + (h[i+1] / 16u);
				} else if (h[i] < 32) {
					x += 3;
					y[1] = (h[i] * 8u) + (h[i+1] / 32u);
				} else if (h[i] < 64) {
					x += 2;
					y[1] = (h[i] * 4u) + (h[i+1] / 64u);
				} else if (h[i] < 128) {
					x += 1;
					y[1] = (h[i] * 2u) + (h[i+1] / 128u);
				} else {
					y[1] = h[i];
				}

				break;
			}

			y[0] = x;

			return(PairToSci(y));
		}

		private uint PairToSci(uint[] pair) {
			return (256 * pair[0]) + pair[1];
		}

		private readonly ILogger<AmoveoPoWAlgorithm> _logger;
	}
}