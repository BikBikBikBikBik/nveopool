#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;

namespace nVeoPool.PoolServer.Configuration.Payment {
	internal class PaymentConfiguration : IPaymentConfiguration {
		#region IPaymentConfiguration
		[JsonProperty(PropertyName = "BatchSendDelay")]
		public uint BatchSendDelaySeconds { get; set; }

		public uint BatchSendSize { get; set; }

		public int BlockMaturityDepth { get; set; }

		[JsonProperty(PropertyName = "Fees")]
		public IDictionary<String, decimal> FeeMap { get; set; }

		[DefaultValue(1*1000)]
		[JsonProperty(PropertyName = "IndividualSendDelay")]
		public uint IndividualSendDelayMilliseconds { get; set; }

		public decimal MinimumPayoutThreshold { get; set; }

		public decimal PoolFee { get; set; }

		public bool PoolPaysTransactionFee { get; set; }

		[JsonProperty(PropertyName = "RoundLength")]
		public int RoundLengthBlocks { get; set; }

		public decimal TransactionFee { get; set; }
		#endregion
	}
}