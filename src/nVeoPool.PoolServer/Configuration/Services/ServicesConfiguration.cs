#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System.ComponentModel;
using Newtonsoft.Json;

namespace nVeoPool.PoolServer.Configuration.Services {
	internal class ServicesConfiguration : IServicesConfiguration {
		#region IServicesConfiguration
		[DefaultValue(60)]
		[JsonProperty(PropertyName = "BanManagement")]
		public uint BanManagementServiceUpdateIntervalSeconds { get; set; }

		[DefaultValue(5*60)]
		[JsonProperty(PropertyName = "BlockCandidateProcessor")]
		public uint BlockCandidateProcessorServiceUpdateIntervalSeconds { get; set; }

		[DefaultValue(15*60)]
		[JsonProperty(PropertyName = "CacheCleanup")]
		public uint CacheCleanupServiceUpdateIntervalSeconds { get; set; }

		[DefaultValue(30)]
		[JsonProperty(PropertyName = "DifficultyCalculator")]
		public uint DifficultyCalculatorServiceUpdateIntervalSeconds { get; set; }

		[DefaultValue(7*24*60*60)]
		[JsonProperty(PropertyName = "InvalidShareCleanup")]
		public uint InvalidShareCleanupServiceUpdateIntervalSeconds { get; set; }

		[DefaultValue(30)]
		[JsonProperty(PropertyName = "InvalidShareStorage")]
		public uint InvalidShareStorageServiceUpdateIntervalSeconds { get; set; }

		[DefaultValue(60)]
		[JsonProperty(PropertyName = "NodeFailover")]
		public uint NodeFailoverServiceUpdateIntervalSeconds { get; set; }

		[DefaultValue(2)]
		[JsonProperty(PropertyName = "NodeSync")]
		public uint NodeSyncServiceUpdateIntervalSeconds { get; set; }

		[DefaultValue(30*60)]
		[JsonProperty(PropertyName = "PaymentSender")]
		public uint PaymentSenderServiceUpdateIntervalSeconds { get; set; }

		[DefaultValue(15)]
		[JsonProperty(PropertyName = "Heartbeat")]
		public uint PoolStatusServiceUpdateIntervalSeconds { get; set; }

		[DefaultValue(7)]
		[JsonProperty(PropertyName = "ArchivedShareLifetime")]
		public uint ShareArchiverServiceArchivedShareLifetimeDays { get; set; }

		[DefaultValue(1*24*60*60)]
		[JsonProperty(PropertyName = "ShareArchiver")]
		public uint ShareArchiverServiceUpdateIntervalSeconds { get; set; }

		[DefaultValue(3)]
		[JsonProperty(PropertyName = "ShareStorage")]
		public uint ShareStorageServiceUpdateIntervalSeconds { get; set; }

		[DefaultValue(190)]
		[JsonProperty(PropertyName = "WorkRetriever")]
		public uint WorkRetrieverServiceUpdateIntervalMilliseconds { get; set; }
		#endregion
	}
}