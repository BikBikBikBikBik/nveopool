﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using Newtonsoft.Json;
using nVeoPool.Common.Configuration;
using nVeoPool.Common.Configuration.Caching;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Common.Configuration.Daemon;
using nVeoPool.Common.Configuration.Database;
using nVeoPool.Common.Configuration.Http;
using nVeoPool.Common.Configuration.Logging;
using nVeoPool.PoolServer.Configuration.Banning;
using nVeoPool.PoolServer.Configuration.Notifications;
using nVeoPool.PoolServer.Configuration.Payment;
using nVeoPool.PoolServer.Configuration.Services;
using nVeoPool.PoolServer.Configuration.VarDiff;
using nVeoPool.PoolServer.Configuration.WorkProtocol;

namespace nVeoPool.PoolServer.Configuration {
	internal class PoolServerConfiguration : ServerConfigurationBase, IPoolServerConfiguration {
		static PoolServerConfiguration() {
			Instance = ReadConfiguration<PoolServerConfiguration>(Environment.GetEnvironmentVariable("NVEOPOOL_POOL_SETTINGS") ?? "pool-settings.json");
		}

		public PoolServerConfiguration(BanningConfiguration banningConfiguration, RedisCacheConfiguration cacheConfiguration, ClusteringConfiguration clusteringConfiguration, DaemonClientConfiguration daemonClientConfiguration, DatabaseConfiguration databaseConfiguration, HttpConfiguration httpConfiguration, LoggingConfiguration loggingConfiguration, NotificationsConfiguration notificationsConfiguration, PaymentConfiguration paymentConfiguration, ServicesConfiguration servicesConfiguration, VarDiffConfiguration varDiffConfiguration, WorkProtocolConfiguration workProtocolConfiguration) : base(cacheConfiguration, clusteringConfiguration, daemonClientConfiguration, databaseConfiguration, httpConfiguration, loggingConfiguration) {
			BanningConfiguration = banningConfiguration;
			NotificationsConfiguration = notificationsConfiguration;
			PaymentConfiguration = paymentConfiguration;
			ServicesConfiguration = servicesConfiguration;
			VarDiffConfiguration = varDiffConfiguration;
			WorkProtocolConfiguration = workProtocolConfiguration;
		}

		public static PoolServerConfiguration Instance { get; }

		#region IPoolServerConfiguration
		[JsonProperty(PropertyName = "Banning")]
		public IBanningConfiguration BanningConfiguration { get; }

		[JsonProperty(PropertyName = "Notifications")]
		public INotificationsConfiguration NotificationsConfiguration { get; }

		[JsonProperty(PropertyName = "Payment")]
		public IPaymentConfiguration PaymentConfiguration { get; }

		[JsonProperty(PropertyName = "ServiceTimers")]
		public IServicesConfiguration ServicesConfiguration { get; }

		[JsonProperty(PropertyName = "VarDiff")]
		public IVarDiffConfiguration VarDiffConfiguration { get; }

		[JsonProperty(PropertyName = "WorkProtocol")]
		public IWorkProtocolConfiguration WorkProtocolConfiguration { get; }
		#endregion
	}
}
