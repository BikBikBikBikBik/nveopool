#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System.ComponentModel;
using Newtonsoft.Json;

namespace nVeoPool.PoolServer.Configuration.WorkProtocol {
	internal class ServerLimitsConfiguration {
		[DefaultValue(120)]
		[JsonProperty(PropertyName = "KeepAliveTimeout")]
		public uint KeepAliveTimeoutSeconds { get; set; }

		[DefaultValue(15000)]
		public long MaximumConcurrentConnections { get; set; }

		[DefaultValue(384)]
		[JsonProperty(PropertyName = "MaximumRequestBodySize")]
		public long MaximumRequestBodySizeBytes { get; set; }

		[DefaultValue(1048576)]
		[JsonProperty(PropertyName = "MaximumRequestBufferSize")]
		public long MaximumRequestBufferSizeBytes { get; set; }

		[DefaultValue(10)]
		[JsonProperty(PropertyName = "MaximumRequestHeaders")]
		public int MaximumRequestHeaderCount { get; set; }

		[DefaultValue(256)]
		[JsonProperty(PropertyName = "MaximumRequestHeadersSize")]
		public int MaximumRequestHeadersTotalSizeBytes { get; set; }

		[DefaultValue(128)]
		[JsonProperty(PropertyName = "MaximumRequestLineSize")]
		public int MaximumRequestLineSizeBytes { get; set; }

		[DefaultValue(65536)]
		[JsonProperty(PropertyName = "MaximumResponseBufferSize")]
		public long MaximumResponseBufferSizeBytes { get; set; }

		[DefaultValue(5)]
		[JsonProperty(PropertyName = "RequestHeadersTimeout")]
		public uint RequestHeadersTimeoutSeconds { get; set; }
	}
}