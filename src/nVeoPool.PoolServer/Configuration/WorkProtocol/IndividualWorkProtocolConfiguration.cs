#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Net;

namespace nVeoPool.PoolServer.Configuration.WorkProtocol {
	internal abstract class IndividualWorkProtocolConfiguration : ServerLimitsConfiguration {
		public IPAddress ListenAddress {
			get {
				if (_listenAddress == null) {
					_listenAddress = IPAddress.Parse(ListenIpAddress);
				}

				return _listenAddress;
			}
		}

		public String ListenIpAddress { get; set; }

		public int ListenPort { get; set; }

		protected IPAddress _listenAddress;
	}
}