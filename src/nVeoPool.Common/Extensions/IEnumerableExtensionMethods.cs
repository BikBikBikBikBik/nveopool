#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;

namespace nVeoPool.Common.Extensions {
	public static class IEnumerableExtensionMethods {
		public static IEnumerable<List<T>> Split<T>(this IEnumerable<T> enumerable, uint listSize) {
			return listSize > 0 ? Split(enumerable.ToList(), (int)listSize) : new List<List<T>> { enumerable.ToList() };
		}

		private static IEnumerable<List<T>> Split<T>(List<T> list, int listSize) {
			for (var i = 0; i < list.Count; i += listSize) {
				yield return list.GetRange(i, Math.Min(listSize, list.Count - i));
			}
		}
	}
}