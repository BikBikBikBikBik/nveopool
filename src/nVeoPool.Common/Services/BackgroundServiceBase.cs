#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using nVeoPool.Common.Configuration.Clustering;

namespace nVeoPool.Common.Services {
	public abstract class BackgroundServiceBase : IHostedService, IDisposable {
		protected BackgroundServiceBase(IClusteringConfiguration clusteringConfiguration, ILogger logger, uint runInterval, ClusterServerType runOnServerType, String serviceName) : this(clusteringConfiguration, logger, runInterval, new [] { runOnServerType }, serviceName) {
		}

		protected BackgroundServiceBase(IClusteringConfiguration clusteringConfiguration, ILogger logger, uint runInterval, IEnumerable<ClusterServerType> runOnServerTypes, String serviceName) {
			_clusteringConfiguration = clusteringConfiguration;
			_logger = logger;
			_runInterval = runInterval;
			_serviceName = serviceName;
			_enabledByLocalConfiguration = _runInterval > 0;
			_enabledByServerType = runOnServerTypes.Contains(_clusteringConfiguration.ServerType);
			_enabled = _enabledByLocalConfiguration && _enabledByServerType;
		}

		#region IHostedService
		public virtual Task StartAsync(CancellationToken cancellationToken) {
			if (_enabled) {
				_executingTask = ExecuteAsync(_cancellationTokenSource.Token);

				if (_executingTask.IsCompleted) {
					return _executingTask;
				}
			} else if (!_enabledByServerType) {
				OnServiceDisabledByServerType();
			} else if (!_enabledByLocalConfiguration) {
				OnServiceDisabledByLocalConfiguration();
			}

			return Task.CompletedTask;
		}

		public virtual async Task StopAsync(CancellationToken cancellationToken) {
			if (_executingTask != null) {
				try {
					_cancellationTokenSource.Cancel();
				} finally {
					await Task.WhenAny(_executingTask, Task.Delay(Timeout.Infinite, cancellationToken));
				}
			}
		}
		#endregion

		#region IDisposable
		public virtual void Dispose() {
			_cancellationTokenSource.Cancel();
		}
		#endregion

		protected void CancelStartup() {
			_cancellationTokenSource.Cancel();
		}

		protected abstract Task ExecuteAsync(CancellationToken cancellationToken);

		protected virtual void LogExceptionMessage(Exception e) {
			_logger.LogError(e, "Exception during {BackgroundService} execution", _serviceName);
		}

		protected virtual void LogExecutingMessage() {
			_logger.LogInformation("Executing {BackgroundService} main loop", _serviceName);
		}

		protected virtual void LogExecutingMessage(EventId eventId) {
			_logger.LogInformation(eventId, "Executing {BackgroundService} main loop", _serviceName);
		}

		protected virtual void LogNotStartingFromLocalConfigurationMessage() {
			_logger.LogInformation("Not starting {BackgroundService} on {ServerId}, disabled in local configuration", _serviceName, _clusteringConfiguration.ServerId);
		}

		protected virtual void LogNotStartingFromServerTypeMessage() {
			_logger.LogInformation("Not starting {BackgroundService} on {ServerId}, service does not run on {ServerType} servers", _serviceName, _clusteringConfiguration.ServerId, _clusteringConfiguration.ServerType);
		}

		protected virtual void LogSleepingMessage() {
			_logger.LogInformation("Sleeping {BackgroundService}", _serviceName);
		}

		protected virtual void LogStoppingMessage() {
			_logger.LogInformation("Stopping {BackgroundService}", _serviceName);
		}

		protected virtual void OnServiceDisabledByLocalConfiguration() {
			LogNotStartingFromLocalConfigurationMessage();
		}

		protected virtual void OnServiceDisabledByServerType() {
			LogNotStartingFromServerTypeMessage();
		}

		private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
		protected readonly IClusteringConfiguration _clusteringConfiguration;
		protected readonly bool _enabled;
		private readonly bool _enabledByLocalConfiguration;
		private readonly bool _enabledByServerType;
		private Task _executingTask;
		protected readonly ILogger _logger;
		protected readonly uint _runInterval;
		protected readonly String _serviceName;
	}
}
