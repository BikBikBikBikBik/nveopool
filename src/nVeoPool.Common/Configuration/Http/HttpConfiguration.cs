#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.ComponentModel;
using System.Net;
using Newtonsoft.Json;

namespace nVeoPool.Common.Configuration.Http {
	public class HttpConfiguration : IHttpConfiguration {
		#region IHttpConfiguration
		public bool EnableRequestLogging { get; set; }
		
		public IPAddress ListenAddress {
			get {
				if (_listenAddress == null) {
					_listenAddress = IPAddress.Parse(ListenIpAddress);
				}

				return _listenAddress;
			}
		}

		public int ListenPort { get; set; }

		[DefaultValue(30)]
		[JsonProperty(PropertyName = "MaximumWaitOnClose")]
		public uint MaximumGracefulCloseWaitSeconds { get; set; }
		#endregion

		public String ListenIpAddress { get; set; }

		private IPAddress _listenAddress;
	}
}