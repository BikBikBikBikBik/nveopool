#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using nVeoPool.Common.Configuration.Caching;
using nVeoPool.Common.Configuration.Clustering;
using nVeoPool.Common.Configuration.Daemon;
using nVeoPool.Common.Configuration.Database;
using nVeoPool.Common.Configuration.Http;
using nVeoPool.Common.Configuration.Logging;
using nVeoPool.Common.Configuration.PubSub;

namespace nVeoPool.Common.Configuration {
	public abstract class ServerConfigurationBase : IServerConfiguration {
		protected ServerConfigurationBase(RedisCacheConfiguration cacheConfiguration, ClusteringConfiguration clusteringConfiguration, DaemonClientConfiguration daemonClientConfiguration, DatabaseConfiguration databaseConfiguration, HttpConfiguration httpConfiguration, LoggingConfiguration loggingConfiguration) {
			CacheConfiguration = cacheConfiguration;
			ClusteringConfiguration = clusteringConfiguration ?? new ClusteringConfiguration(Clustering.ClusteringConfiguration.CreateServerId(), Clustering.ClusteringConfiguration.DefaultServerType);
			DaemonClientConfiguration = daemonClientConfiguration;
			DatabaseConfiguration = databaseConfiguration;
			HttpConfiguration = httpConfiguration;
			LoggingConfiguration = loggingConfiguration;
		}

		#region IServerConfiguration
		[JsonProperty(PropertyName = "Cache")]
		public virtual ICacheConfiguration CacheConfiguration { get; }

		public virtual IChannelConfiguration ChannelConfiguration => new ChannelConfiguration();
		
		[JsonProperty(PropertyName = "Clustering")]
		public virtual IClusteringConfiguration ClusteringConfiguration { get; }

		[JsonProperty(PropertyName = "NodeRpc")]
		public virtual IDaemonClientConfiguration DaemonClientConfiguration { get; }

		[JsonProperty(PropertyName = "Database")]
		public virtual IDatabaseConfiguration DatabaseConfiguration { get; }

		[JsonProperty(PropertyName = "Http")]
		public virtual IHttpConfiguration HttpConfiguration { get; }

		[JsonProperty(PropertyName = "Logging")]
		public virtual ILoggingConfiguration LoggingConfiguration { get; }
		#endregion

		protected static T ReadConfiguration<T>(String fullFileName) {
			if (!File.Exists(fullFileName)) {
				throw new FileNotFoundException("Configuration file does not exist.", fullFileName);
			}

			var settingsFileContents = File.ReadAllText(fullFileName, SETTINGS_FILE_ENCODING);
			if (String.IsNullOrWhiteSpace(settingsFileContents)) {
				throw new Exception("Configuration file is empty.");
			}
			
			return JsonConvert.DeserializeObject<T>(settingsFileContents, _jsonSerializerSettings);
		}

		private static readonly Encoding SETTINGS_FILE_ENCODING = Encoding.ASCII;
		private static readonly JsonSerializerSettings _jsonSerializerSettings = new JsonSerializerSettings {
			ContractResolver = new CamelCasePropertyNamesContractResolver(),
			Converters = new List<JsonConverter> { new StringEnumConverter() },
			DefaultValueHandling = DefaultValueHandling.Populate,
			TypeNameHandling = TypeNameHandling.Auto
		};
	}
}