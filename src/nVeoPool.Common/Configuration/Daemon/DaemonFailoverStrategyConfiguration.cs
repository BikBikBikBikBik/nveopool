#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System.ComponentModel;
using Newtonsoft.Json;

namespace nVeoPool.Common.Configuration.Daemon {
	public class DaemonFailoverStrategyConfiguration {
		[DefaultValue(DefaultBreakDurationSeconds)]
		[JsonProperty(PropertyName = "BreakDuration")]
		public uint BreakDurationSeconds { get; set; }

		[DefaultValue(DefaultFailureThresholdPercent)]
		[JsonProperty(PropertyName = "FailureThreshold")]
		public double FailureThresholdPercent { get; set; }

		[DefaultValue(DefaultMinimumRequestCount)]
		[JsonProperty(PropertyName = "MinimumRequests")]
		public uint MinimumRequestCount { get; set; }

		[DefaultValue(DefaultSamplingDurationSeconds)]
		[JsonProperty(PropertyName = "SampleDuration")]
		public uint SamplingDurationSeconds { get; set; }

		public static DaemonFailoverStrategyConfiguration Default => DefaultDaemonFailoverStrategyConfiguration;

		private const uint DefaultBreakDurationSeconds = 90;
		private const double DefaultFailureThresholdPercent = 80;
		private const uint DefaultMinimumRequestCount = 20;
		private const uint DefaultSamplingDurationSeconds = 90;
		private static readonly DaemonFailoverStrategyConfiguration DefaultDaemonFailoverStrategyConfiguration = new DaemonFailoverStrategyConfiguration {
			BreakDurationSeconds = DefaultBreakDurationSeconds,
			FailureThresholdPercent = DefaultFailureThresholdPercent,
			MinimumRequestCount = DefaultMinimumRequestCount,
			SamplingDurationSeconds = DefaultSamplingDurationSeconds
		};
	}
}
