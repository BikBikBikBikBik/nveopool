#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace nVeoPool.Common.Configuration.Daemon {
	public class DaemonClientConfiguration : IDaemonClientConfiguration {
		#region IDaemonClientConfiguration
		[JsonProperty(PropertyName = "Backup")]
		public BackupDaemonConfiguration BackupDaemonConfiguration { get; set; }

		[JsonProperty(PropertyName = "Local")]
		public LocalDaemonConfiguration LocalDaemonConfiguration {
			get {
				if (_localDaemonConfiguration == null) {
					_localDaemonConfiguration = new LocalDaemonConfiguration();
				}

				return _localDaemonConfiguration;
			}
			set => _localDaemonConfiguration = value;
		}

		public List<Uri> PreferredPeerAddresses {
			get {
				if (_preferredPeers == null) {
					_preferredPeers = PreferredPeers?.Select(p => new Uri(p.StartsWith("http://") ? p : $"http://{p}")).ToList() ?? new List<Uri>();
				}

				return _preferredPeers;
			}
		}
		#endregion

		public List<String> PreferredPeers { get; set; }

		private LocalDaemonConfiguration _localDaemonConfiguration;
		private List<Uri> _preferredPeers;
	}
}
