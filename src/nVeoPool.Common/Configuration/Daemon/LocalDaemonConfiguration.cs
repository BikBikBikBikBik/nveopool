#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace nVeoPool.Common.Configuration.Daemon {
	public class LocalDaemonConfiguration {
		public LocalDaemonConfiguration() {
			ExternalApiPort = DEFAULT_EXTERNAL_API_PORT;
			InternalApiPort = DEFAULT_INTERNAL_API_PORT;
			Ip = DEFAULT_IP_ADDRESS;
		}

		public Uri ExternalApiAddress {
			get {
				if (_externalApiAddress == null) {
					_externalApiAddress = new Uri($"{(Ip.StartsWith("http://") ? String.Empty : "http://")}{Ip}:{ExternalApiPort}");
				}

				return _externalApiAddress;
			}
		}

		[DefaultValue(DEFAULT_EXTERNAL_API_PORT)]
		public int ExternalApiPort { get; set; }

		[JsonProperty(PropertyName = "Strategy")]
		public DaemonFailoverStrategyConfiguration FailoverConfiguration { get; set; }

		public Uri InternalApiAddress {
			get {
				if (_internalApiAddress == null) {
					_internalApiAddress = new Uri($"{(Ip.StartsWith("http://") ? String.Empty : "http://")}{Ip}:{InternalApiPort}");
				}

				return _internalApiAddress;
			}
		}

		[DefaultValue(DEFAULT_INTERNAL_API_PORT)]
		public int InternalApiPort { get; set; }

		[DefaultValue(DEFAULT_IP_ADDRESS)]
		public String Ip { get; set; }

		public bool IsPrimaryNode { get; set; }

		private const int DEFAULT_EXTERNAL_API_PORT = 8080;
		private const int DEFAULT_INTERNAL_API_PORT = 8081;
		private const String DEFAULT_IP_ADDRESS = "127.0.0.1";
		private Uri _externalApiAddress;
		private Uri _internalApiAddress;
	}
}
