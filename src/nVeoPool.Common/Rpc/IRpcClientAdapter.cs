#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using nVeoPool.Common.Models;

namespace nVeoPool.Common.Rpc {
	public interface IRpcClientAdapter {
		String HostName { get; }

		ulong Id { get; }

		bool IsPrimaryNode { get; }

		Task<Block> GetBlockAsync(int blockHeight, CancellationToken cancellationToken = new CancellationToken());

		Task<int> GetCurrentHeightAsync(CancellationToken cancellationToken = new CancellationToken());

		Task<bool> GetKeysStatusAsync(CancellationToken cancellationToken = new CancellationToken());

		Task<IEnumerable<Uri>> GetPeersAsync(bool includeSpecial, CancellationToken cancellationToken = new CancellationToken());

		Task<String> GetPubKeyAsync(CancellationToken cancellationToken = new CancellationToken());

		Task<Block> GetTopAsync(CancellationToken cancellationToken = new CancellationToken());

		Task<(int BlockDifficulty, String BlockHash, int FieldZero)> GetWorkAsync(CancellationToken cancellationToken = new CancellationToken());

		Task<String> SetNormalSyncModeAsync(CancellationToken cancellationToken = new CancellationToken());

		Task<String> SpendAsync(String publicKey, decimal amount, decimal fee, bool amountRequiresMultiplication);

		Task<bool> SubmitWorkAsync(String nonce);

		Task<String> SyncHeadersAsync(Uri nodeAddress);
	}
}
