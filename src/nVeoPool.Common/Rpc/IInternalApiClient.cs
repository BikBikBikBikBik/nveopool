#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Threading;
using System.Threading.Tasks;

namespace nVeoPool.Common.Rpc {
	public interface IInternalApiClient {
		String HostName { get; }

		Task<String> CreateAccountAsync(String publicKey, decimal amount, decimal fee, bool amountRequiresMultiplication);
		
		Task<String> GetKeysStatusAsync(CancellationToken cancellationToken = new CancellationToken());
		
		Task<String> GetPubKeyAsync(CancellationToken cancellationToken = new CancellationToken());
		
		Task<String> GetWorkAsync(CancellationToken cancellationToken = new CancellationToken());

		Task<String> SetNormalSyncModeAsync(CancellationToken cancellationToken = new CancellationToken());

		Task<String> SpendAsync(String publicKey, decimal amount, bool amountRequiresMultiplication);

		Task<String> SpendAsync(String publicKey, decimal amount, decimal fee, bool amountRequiresMultiplication);

		Task<String> SubmitWorkAsync(String nonce);

		Task<String> SyncHeadersAsync(String ipAddress, int port);
	}
}
