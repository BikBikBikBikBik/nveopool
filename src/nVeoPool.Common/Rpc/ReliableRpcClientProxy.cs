#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using nVeoPool.Common.Models;
using Polly;

namespace nVeoPool.Common.Rpc {
	public class ReliableRpcClientProxy : IRpcClientAdapter {
		public ReliableRpcClientProxy(IRpcClientAdapter innerClient, Policy policy) {
			_innerClient = innerClient;
			_policy = policy;
		}

		#region IRpcClientAdapter
		public String HostName => _innerClient.HostName;

		public ulong Id => _innerClient.Id;

		public bool IsPrimaryNode => _innerClient.IsPrimaryNode;

		public Task<Block> GetBlockAsync(int blockHeight, CancellationToken cancellationToken = new CancellationToken()) {
			return TryExecuteAndCaptureAsync(() => _innerClient.GetBlockAsync(blockHeight, cancellationToken), nameof(GetBlockAsync));
		}

		public Task<int> GetCurrentHeightAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return TryExecuteAndCaptureAsync(() => _innerClient.GetCurrentHeightAsync(cancellationToken), nameof(GetCurrentHeightAsync));
		}

		public Task<bool> GetKeysStatusAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return TryExecuteAndCaptureAsync(() => _innerClient.GetKeysStatusAsync(cancellationToken), nameof(GetKeysStatusAsync));
		}

		public Task<IEnumerable<Uri>> GetPeersAsync(bool includeSpecial, CancellationToken cancellationToken = new CancellationToken()) {
			return TryExecuteAndCaptureAsync(() => _innerClient.GetPeersAsync(includeSpecial, cancellationToken), nameof(GetPeersAsync));
		}

		public Task<String> GetPubKeyAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return TryExecuteAndCaptureAsync(() => _innerClient.GetPubKeyAsync(cancellationToken), nameof(GetPubKeyAsync));
		}

		public Task<Block> GetTopAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return TryExecuteAndCaptureAsync(() => _innerClient.GetTopAsync(cancellationToken), nameof(GetTopAsync));
		}

		public Task<(int BlockDifficulty, String BlockHash, int FieldZero)> GetWorkAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return TryExecuteAndCaptureAsync(() => _innerClient.GetWorkAsync(cancellationToken), nameof(GetWorkAsync));
		}

		public Task<String> SetNormalSyncModeAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return TryExecuteAndCaptureAsync(() => _innerClient.SetNormalSyncModeAsync(cancellationToken), nameof(SetNormalSyncModeAsync));
		}

		public Task<String> SpendAsync(String publicKey, decimal amount, decimal fee, bool amountRequiresMultiplication) {
			return TryExecuteAndCaptureAsync(() => _innerClient.SpendAsync(publicKey, amount, fee, amountRequiresMultiplication), nameof(SpendAsync));
		}

		public Task<bool> SubmitWorkAsync(String nonce) {
			return TryExecuteAndCaptureAsync(() => _innerClient.SubmitWorkAsync(nonce), nameof(SubmitWorkAsync));
		}

		public Task<String> SyncHeadersAsync(Uri nodeAddress) {
			return TryExecuteAndCaptureAsync(() => _innerClient.SyncHeadersAsync(nodeAddress), nameof(SyncHeadersAsync));
		}
		#endregion

		private async Task<T> TryExecuteAndCaptureAsync<T>(Func<Task<T>> action, String methodName) {
			var policyResult = await _policy.ExecuteAndCaptureAsync(action);
			if (policyResult.Outcome == OutcomeType.Successful) {
				return policyResult.Result;
			}
			
			throw new Exception($"Error executing action {methodName}", policyResult.FinalException);
		}

		private readonly IRpcClientAdapter _innerClient;
		private readonly Policy _policy;
	}
}
