#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using nVeoPool.Common.Extensions;
using nVeoPool.Common.Models;

namespace nVeoPool.Common.Rpc {
	public class RpcClientAdapter : IRpcClientAdapter {
		public RpcClientAdapter(IExternalApiClient externalApiClient, IInternalApiClient internalApiClient, bool isPrimaryNode) : this(externalApiClient, internalApiClient, 0, isPrimaryNode) {
		}

		public RpcClientAdapter(IExternalApiClient externalApiClient, IInternalApiClient internalApiClient, ulong id, bool isPrimaryNode) {
			_externalApiClient = externalApiClient;
			Id = id;
			_internalApiClient = internalApiClient;
			IsPrimaryNode = isPrimaryNode;
		}

		#region IRpcClientAdapter
		public String HostName => _externalApiClient.HostName;

		public ulong Id { get; }

		public bool IsPrimaryNode { get; }

		public async Task<Block> GetBlockAsync(int blockHeight, CancellationToken cancellationToken = new CancellationToken()) {
			var fullBlockArray = (await _externalApiClient.GetBlockAsync(blockHeight, cancellationToken)).ToJArray();

			return new Block {
				CoinbaseAddress = fullBlockArray[1][10][1][1].Value<String>()
			};
		}

		public async Task<int> GetCurrentHeightAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return (await _externalApiClient.GetCurrentHeightAsync(cancellationToken)).ToJArray()[1].Value<int>();
		}

		public async Task<bool> GetKeysStatusAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return UNLOCKED_KEYS_STATUS_RESPONSE.Equals(await _internalApiClient.GetKeysStatusAsync(cancellationToken));
		}

		public async Task<IEnumerable<Uri>> GetPeersAsync(bool includeSpecial, CancellationToken cancellationToken = new CancellationToken()) {
			var peers = await _externalApiClient.GetPeersAsync(cancellationToken);

			if (!includeSpecial) {
				peers = peers.Where(p => !"0.0.0.0".Equals(p.IpAddress) && !"127.0.0.1".Equals(p.IpAddress));
			}

			return peers.Select(p => new Uri($"http://{p.IpAddress}:{p.Port}"));
		}

		public async Task<String> GetPubKeyAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return (await _internalApiClient.GetPubKeyAsync(cancellationToken)).ToJArray()[1].Value<String>().FromBase64();
		}

		public async Task<Block> GetTopAsync(CancellationToken cancellationToken = new CancellationToken()) {
			var fullBlockArray = (await _externalApiClient.GetTopAsync(cancellationToken)).ToJArray();
			
			return new Block {
				Hash = fullBlockArray[1][2].Value<String>(),
				Height = fullBlockArray[1][1].Value<uint>()
			};
		}

		public async Task<(int BlockDifficulty, String BlockHash, int FieldZero)> GetWorkAsync(CancellationToken cancellationToken = new CancellationToken()) {
			var miningDataArray = (await _internalApiClient.GetWorkAsync(cancellationToken)).ToJArray()[1].Values().ToList();
		
			return (miningDataArray[3].Value<int>(), miningDataArray[1].Value<String>(), miningDataArray[0].Value<int>());
		}

		public Task<String> SetNormalSyncModeAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return _internalApiClient.SetNormalSyncModeAsync(cancellationToken);
		}

		public async Task<String> SpendAsync(String publicKey, decimal amount, decimal fee, bool amountRequiresMultiplication) {
			String transactionResponse;

			if (fee > 0) {
				var accountInfo = (await _externalApiClient.GetAccountAsync(publicKey)).ToJArray();

				if (accountInfo[1].Type == JTokenType.String) {
					transactionResponse = await _internalApiClient.CreateAccountAsync(publicKey, amount, fee, amountRequiresMultiplication);
				} else {
					transactionResponse = await _internalApiClient.SpendAsync(publicKey, amount, fee, amountRequiresMultiplication);
				}
			} else {
				transactionResponse = await _internalApiClient.SpendAsync(publicKey, amount, amountRequiresMultiplication);
			}

			return transactionResponse.ToJArray()[1].Value<String>();
		}

		public async Task<bool> SubmitWorkAsync(String nonce) {
			try {
				await _internalApiClient.SubmitWorkAsync(nonce);

				return true;
			} catch (HttpRequestException e) {
				if (e.Message.Contains("500 (Internal Server Error)")) {
					return false;
				}

				throw;
			}
		}

		public Task<String> SyncHeadersAsync(Uri nodeAddress) {
			return _internalApiClient.SyncHeadersAsync(nodeAddress.Host, nodeAddress.Port);
		}
		#endregion

		private const String UNLOCKED_KEYS_STATUS_RESPONSE = "[\"ok\",\"unlocked\"]";
		private readonly IExternalApiClient _externalApiClient;
		private readonly IInternalApiClient _internalApiClient;
	}
}
