#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.CircuitBreaker;
using nVeoPool.Common.Configuration;
using nVeoPool.Common.Configuration.Daemon;

namespace nVeoPool.Common.Rpc {
	public class RpcClientAdapterFactory : IRpcClientAdapterFactory {
		public RpcClientAdapterFactory(ILogger<RpcClientAdapterFactory> logger, IServerConfiguration serverConfiguration) {
			_logger = logger;
			_nextClientId = 1;
			_primaryNodePolicy = CreateFailoverPolicy(serverConfiguration.DaemonClientConfiguration.LocalDaemonConfiguration.ExternalApiAddress.Host, serverConfiguration.DaemonClientConfiguration.BackupDaemonConfiguration?.FailoverConfiguration ?? DaemonFailoverStrategyConfiguration.Default);
			var rpcClientAdapter = new RpcClientAdapter(new ExternalApiClient(serverConfiguration.DaemonClientConfiguration.LocalDaemonConfiguration.ExternalApiAddress), new InternalApiClient(serverConfiguration.DaemonClientConfiguration.LocalDaemonConfiguration.InternalApiAddress), _nextClientId++, true);
			_backupNodePolicyMap = serverConfiguration.DaemonClientConfiguration.BackupDaemonConfiguration?.Nodes?.ToDictionary(n => (IRpcClientAdapter)new RpcClientAdapter(new ExternalApiClient(n.ExternalApiAddress), new InternalApiClient(n.InternalApiAddress), _nextClientId++, n.IsPrimaryNode), n => CreateFailoverPolicy(n.ExternalApiAddress.Host, n.FailoverConfiguration ?? (serverConfiguration.DaemonClientConfiguration.BackupDaemonConfiguration.FailoverConfiguration ?? DaemonFailoverStrategyConfiguration.Default))) ?? new Dictionary<IRpcClientAdapter, CircuitBreakerPolicy>();
			_backupNodesEnabled = _backupNodePolicyMap.Any();
			_primaryClient = _backupNodesEnabled ? (IRpcClientAdapter)new ReliableRpcClientProxy(rpcClientAdapter, _primaryNodePolicy) : rpcClientAdapter;
			_backupNodePolicyMap[_primaryClient] = _primaryNodePolicy;
		}

		#region IRpcClientAdapterFactory
		public IEnumerable<IRpcClientAdapter> GetBackupClientAdapters() {
			return _backupNodePolicyMap.Where(kv => !kv.Key.IsPrimaryNode).Select(kv => kv.Key);
		}

		public IRpcClientAdapter GetClientAdapter() {
			return GetClientAdapter(true);
		}

		public IRpcClientAdapter GetClientAdapter(ulong preferredClientId) {
			var (client, policy) = _backupNodePolicyMap.FirstOrDefault(kv => kv.Key.Id == preferredClientId && (kv.Value.CircuitState == CircuitState.Closed || kv.Value.CircuitState == CircuitState.HalfOpen));
			if (client != null) {
				_logger.LogDebug("Node client with id {NodeClientId} available", preferredClientId);

				return new ReliableRpcClientProxy(client, policy);
			}

			_logger.LogWarning("Requested node client with id {NodeClientId} unavailable, attempting to retrieve backup node client", preferredClientId);

			return GetClientAdapter(false);
		}

		public IRpcClientAdapter GetClientAdapter(bool requirePrimaryNode) {
			if (!_backupNodesEnabled) {
				_logger.LogDebug("Backup strategy disabled, returning primary node client {NodeAddress}", _primaryClient.HostName);

				return _primaryClient;
			}
			if (_primaryNodePolicy.CircuitState == CircuitState.Closed || _primaryNodePolicy.CircuitState == CircuitState.HalfOpen) {
				_logger.LogDebug("Backup strategy enabled, returning primary node client {NodeAddress}", _primaryClient.HostName);

				return _primaryClient;
			}

			var (client, policy) = _backupNodePolicyMap.FirstOrDefault(kv => (kv.Value.CircuitState == CircuitState.Closed || kv.Value.CircuitState == CircuitState.HalfOpen) && (!requirePrimaryNode || kv.Key.IsPrimaryNode));
			if (client != null) {
				_logger.LogInformation("Backup strategy enabled, returning {NodeType} node client for {NodeAddress}", client.IsPrimaryNode ? "Primary" : "Backup", client.HostName);

				return new ReliableRpcClientProxy(client, policy);
			}

			_logger.LogError("Node backup strategy enabled but no backup node clients are available");

			throw new Exception("No suitable node client found");
		}

		public IRpcClientAdapter GetClientAdapter(Uri nodeAddress, bool isPrimaryNode = false) {
			var rpcClientAdapter = new RpcClientAdapter(new ExternalApiClient(nodeAddress), new InternalApiClient(nodeAddress), isPrimaryNode);

			return _backupNodesEnabled ? (IRpcClientAdapter)new ReliableRpcClientProxy(rpcClientAdapter, CreateFailoverPolicy(nodeAddress.Host, DaemonFailoverStrategyConfiguration.Default)) : rpcClientAdapter;
		}
		#endregion

		private CircuitBreakerPolicy CreateFailoverPolicy(String nodeHostName, DaemonFailoverStrategyConfiguration configuration) {
			return Policy.Handle<Exception>().AdvancedCircuitBreakerAsync(configuration.FailureThresholdPercent / 100d, TimeSpan.FromSeconds(configuration.SamplingDurationSeconds), (int)configuration.MinimumRequestCount, TimeSpan.FromSeconds(configuration.BreakDurationSeconds), (e, ts) => {
				_logger.LogWarning("Node client for {NodeAddress} transitioning to disabled state", nodeHostName);
			}, () => {
				_logger.LogInformation("Node client for {NodeAddress} transitioning to ready state", nodeHostName);
			}, () => {
				_logger.LogInformation("Node client for {NodeAddress} transitioning to half-ready state", nodeHostName);
			});
		}

		private readonly IDictionary<IRpcClientAdapter, CircuitBreakerPolicy> _backupNodePolicyMap;
		private readonly bool _backupNodesEnabled;
		private readonly ILogger<RpcClientAdapterFactory> _logger;
		private ulong _nextClientId;
		private readonly IRpcClientAdapter _primaryClient;
		private readonly CircuitBreakerPolicy _primaryNodePolicy;
	}
}
