﻿#region License
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of nVeoPool.

nVeoPool is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

nVeoPool is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with nVeoPool.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Threading;
using System.Threading.Tasks;
using nVeoPool.Common.Extensions;

namespace nVeoPool.Common.Rpc {
	public class InternalApiClient : ApiClientBase, IInternalApiClient {
		public InternalApiClient(Uri nodeAddress) : base(nodeAddress) {
		}

		#region IInternalApiClient
		public String HostName => _nodeAddress.Host;

		public Task<String> CreateAccountAsync(String publicKey, decimal amount, decimal fee, bool amountRequiresMultiplication) {
			var integralAmount = (long)(amountRequiresMultiplication ? amount.ToVeoBaseUnits() : amount);
			var integralFee = (long)(amountRequiresMultiplication ? fee.ToVeoBaseUnits() : fee);
			
			return GetNodeResponseAsync($"[\"create_account\",\"{publicKey}\",{integralAmount},{integralFee}]");
		}

		public Task<String> GetKeysStatusAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return GetSimpleCommandResponseAsync("keys_status", cancellationToken);
		}

		public Task<String> GetPubKeyAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return GetSimpleCommandResponseAsync("pubkey", cancellationToken);
		}
		
		public Task<String> GetWorkAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return GetSimpleCommandResponseAsync("mining_data", cancellationToken);
		}

		public Task<String> SetNormalSyncModeAsync(CancellationToken cancellationToken = new CancellationToken()) {
			return GetSimpleCommandResponseAsync("sync_normal", cancellationToken);
		}

		public Task<String> SpendAsync(String publicKey, decimal amount, bool amountRequiresMultiplication) {
			var integralAmount = (long)(amountRequiresMultiplication ? amount.ToVeoBaseUnits() : amount);
			
			return GetNodeResponseAsync($"[\"spend\",\"{publicKey}\",{integralAmount}]");
		}

		public Task<String> SpendAsync(String publicKey, decimal amount, decimal fee, bool amountRequiresMultiplication) {
			var integralAmount = (long)(amountRequiresMultiplication ? amount.ToVeoBaseUnits() : amount);
			var integralFee = (long)(amountRequiresMultiplication ? fee.ToVeoBaseUnits() : fee);
			
			return GetNodeResponseAsync($"[\"spend\",\"{publicKey}\",{integralAmount},{integralFee}]");
		}

		public Task<String> SubmitWorkAsync(String nonce) {
			return GetNodeResponseAsync($"[\"work\",\"{nonce}\",0]");
		}

		public Task<String> SyncHeadersAsync(String ipAddress, int port) {
			return GetNodeResponseAsync($"[\"sync\",2,[-7,{ipAddress.Replace('.', ',')}],{port}]");
		}
		#endregion
	}
}
